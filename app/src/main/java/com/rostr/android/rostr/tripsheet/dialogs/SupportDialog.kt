package com.rostr.android.rostr.tripsheet.dialogs

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.utils.showLog
import com.rostr.android.rostr.tripsheet.utils.showLongToast
import kotlinx.android.synthetic.main.dialog_support.*


class SupportDialog : DialogFragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.dialog_support,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_contact_us_close.setOnClickListener(this)
        tv_contact_us_whatsappText.setOnClickListener(this)
        tv_contact_us_callText.setOnClickListener(this)
        tv_contact_us_emailText.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            /*dialog.window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )*/
            val wlp = dialog.window?.attributes
            wlp?.width = WindowManager.LayoutParams.MATCH_PARENT
            wlp?.height = WindowManager.LayoutParams.WRAP_CONTENT
            wlp?.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
            dialog.window?.attributes = wlp
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_contact_us_close -> dismiss()

            R.id.tv_contact_us_whatsappText -> {
                try {
                    val whatsappIntent = Intent(Intent.ACTION_VIEW)
                    val url =
                        "https://api.whatsapp.com/send?phone=" + context!!.getString(R.string.contact)
                    whatsappIntent.setPackage("com.whatsapp")
                    whatsappIntent.data = Uri.parse(url)
                    if (whatsappIntent.resolveActivity(activity!!.packageManager) != null) {
                        activity!!.startActivity(whatsappIntent)
                    } else {
                        showLongToast(
                            context!!,
                            getString(R.string.no_whatsapp)
                        )
                    }
                } catch (e: Exception) {
                    showLog(e)
                    showLongToast(
                        context!!,
                        getString(R.string.request_failure)
                    )
                }
            }

            R.id.tv_contact_us_callText -> {
                val intent = Intent(
                    Intent.ACTION_DIAL,
                    Uri.parse("tel:" + activity!!.getString(R.string.contact))
                )
                activity!!.startActivity(intent)
            }

            R.id.tv_contact_us_emailText -> {
                val emailIntent = Intent(Intent.ACTION_SEND)
                emailIntent.type = "message/rfc822"
                emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(tv_contact_us_emailText.text))
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, activity?.getString(R.string.contact_us))
                try {
                    activity!!.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
                } catch (ex: ActivityNotFoundException) {
                    showLongToast(context!!, "There are no email clients installed.")
                }
            }
        }
    }
}
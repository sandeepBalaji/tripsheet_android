package com.rostr.android.rostr.tripsheet.fragments.tripDetailFragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.adapter.NoteAdapter
import com.rostr.android.rostr.tripsheet.dialogs.AddNoteDialog
import com.rostr.android.rostr.tripsheet.model.TripNoteResponse
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.utils.DATA_KEY
import kotlinx.android.synthetic.main.fragment_trip_note.*
import kotlinx.android.synthetic.main.recycler_layout.*

class TripNotesFragment : Fragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var tripData: TripResponse
    private lateinit var noteList: ArrayList<TripNoteResponse>
    private var noteAdapter: NoteAdapter? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trip_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null && arguments!!.containsKey(DATA_KEY)) {
            tripData = arguments?.getParcelable(DATA_KEY)!!
            noteList = tripData.tripNotes!!
            initRecycler()
            bt_recycler_actionButton.setOnClickListener(this)
            fab_trip_note_addNote.setOnClickListener(this)
        } else
            showError("No data received from the trip", false)
    }

    private fun initRecycler(){
        if (noteList.isEmpty())
            showError(getString(R.string.empty_notes), true)
        else {
            tv_recyclerMessage.visibility = GONE
            bt_recycler_actionButton.visibility = GONE
            rv_recycler.apply {
                layoutManager = LinearLayoutManager(mContext)
                setHasFixedSize(true)
                isNestedScrollingEnabled = false
                noteAdapter = NoteAdapter(mContext, noteList) { _, _ ->

                }
                adapter = noteAdapter
            }
            fab_trip_note_addNote.visibility = VISIBLE
        }
    }

    override fun onClick(v: View?) {
        showAddNoteDialog()
    }

    private fun showAddNoteDialog() {
        if (childFragmentManager.findFragmentByTag(getString(R.string.add_note)) != null)
            (childFragmentManager.findFragmentByTag(getString(R.string.add_note)) as BottomSheetDialogFragment).dismiss()
        val addNoteDialog = AddNoteDialog {
            tripData.tripNotes!!.clear()
            tripData.tripNotes!!.addAll(it as ArrayList<TripNoteResponse>)
            noteList.clear()
            noteList.addAll(it)
            if(noteAdapter != null)
                noteAdapter!!.notifyDataSetChanged()
            else
                initRecycler()

        }
        val bundle = Bundle()
        bundle.putParcelable(DATA_KEY, tripData)
        addNoteDialog.arguments = bundle
        addNoteDialog.show(childFragmentManager, getString(R.string.add_note))
    }

    private fun showError(message: String, isEmptyNotes: Boolean) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
        if (isEmptyNotes) {
            bt_recycler_actionButton.text = getString(R.string.add_note)
            bt_recycler_actionButton.visibility = VISIBLE
        }
    }
}
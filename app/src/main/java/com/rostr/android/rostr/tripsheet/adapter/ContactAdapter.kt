package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.Contacts
import kotlinx.android.synthetic.main.item_driver.view.*

class ContactAdapter(
    private val context: Context,
    private val mainList: ArrayList<Contacts>,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {

    class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.iv_driver_item_image
        val name: TextView = itemView.tv_driver_item_name
        val phone: TextView = itemView.tv_driver_item_vehicleType
        val amount: TextView = itemView.tv_driver_item_amount
        val vehicleNo: TextView = itemView.tv_driver_item_vehicleNo
        val callIcon: ImageView = itemView.iv_driver_item_callIcon
        val whatsapp: ImageView = itemView.iv_driver_item_whatsappIcon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_driver,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.image.visibility = GONE
        holder.amount.visibility = GONE
        holder.callIcon.visibility = GONE
        holder.whatsapp.visibility = GONE
        holder.vehicleNo.visibility = GONE
        val currentItem = mainList[position]
        holder.name.text = currentItem.contactName
        holder.phone.text = currentItem.contactNumber
        holder.itemView.setOnClickListener {
            onItemClick(it, position)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
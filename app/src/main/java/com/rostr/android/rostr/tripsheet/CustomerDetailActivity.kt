package com.rostr.android.rostr.tripsheet

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ListPopupWindow
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.adapter.OptionsMenuAdapter
import com.rostr.android.rostr.tripsheet.adapter.TripAdapter
import com.rostr.android.rostr.tripsheet.dialogs.ProgressDialog
import com.rostr.android.rostr.tripsheet.model.Customer
import com.rostr.android.rostr.tripsheet.model.DeleteRequest
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.deletePutOperation
import com.rostr.android.rostr.tripsheet.network.getCustomersTripsOperation
import com.rostr.android.rostr.tripsheet.network.getInvoiceOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_driver_detail.*
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import java.io.File


class CustomerDetailActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var customerProfile: Customer
    private lateinit var tripList: ArrayList<TripResponse>
    private lateinit var tripAdapter: TripAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_detail)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            tv_driver_detail_pay.visibility = GONE
            initRecycler()
            if (intent.hasExtra(DATA_KEY)) {
                customerProfile = intent.getParcelableExtra(DATA_KEY)
                Glide.with(this).load(R.drawable.ic_avatar_male).into(iv_driver_detail_driverImage)
                tv_driver_detail_driverName.text = customerProfile.name
                tv_toolbar_title.text = getString(R.string.app_name)
                bt_recycler_actionButton.setOnClickListener(this)
                fab_driver_detail_addTransaction.setOnClickListener(this)
                iv_driver_detail_optionsIcon.setOnClickListener(this)
            } else {
                showAlertDialog(this, getString(R.string.oops), "Did not receive driver details") {
                    finish()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(this))
            fetchTripData()
        else
            showError(getString(R.string.no_internet))
    }

    private fun initRecycler() {
        tripList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@CustomerDetailActivity)
            tripAdapter =
                TripAdapter(this@CustomerDetailActivity, tripList, false) { view, position ->
                    when (view.id) {
                        R.id.tv_trip_item_invoice -> {
                            checkStoragePermission(tripList[position]._id!!)
                        }
                        else -> {
                            try {
                                val intent =
                                    Intent(
                                        this@CustomerDetailActivity,
                                        TripDetailActivity::class.java
                                    )
                                intent.putExtra(DATA_KEY, tripList[position])
                                startActivity(intent)
                            } catch (ex: Exception) {
                                showLog(ex)
                            }
                        }
                    }
                }
            adapter = tripAdapter
        }
    }


    private fun checkStoragePermission(tripId: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        getInvoiceUrl(tripId)
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        showShortToast(
                            this@CustomerDetailActivity,
                            getString(R.string.grant_image_permission)
                        )
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest,
                        token: PermissionToken
                    ) {
                        showAlertDialog(this@CustomerDetailActivity,
                            getString(R.string.permission_required_title),
                            getString(R.string.grant_image_permission),
                            "Yes",
                            "No", {
                                token.continuePermissionRequest()
                            }, {
                                token.cancelPermissionRequest()
                            })
                    }
                }).check()
        } else {
            getInvoiceUrl(tripId)
        }
    }

    private fun getInvoiceUrl(tripId: String) {
        val progress = ProgressDialog.progressDialog(
            this,
            "Fetching Invoice..."
        )
        progress.show()
        progress.setCanceledOnTouchOutside(false)
        getInvoiceOperation(ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            tripId, {
                progress.dismiss()
                fileSetup(it.message)
            }, {
                progress.dismiss()
                if (it.first == 401) {
                    showAlertDialog(
                        this,
                        getString(R.string.unauthorized_title),
                        getString(R.string.logged_out)
                    ) {
                        logoutUser(this)
                    }
                } else
                    showAlertDialog(this, getString(R.string.oops), it.second)
            })
    }

    private fun fileSetup(fileFromUrl: String?) {
        val fileName = "${fileFromUrl}.pdf"
        val parentPath =
            "${Environment.getExternalStorageDirectory()}" +
                    "${File.separator}${getString(R.string.app_name)}"
        val parentFile = File(parentPath)
        val docFilePath = "$parentPath${File.separator}$fileName"
        if (!parentFile.exists()) {
            showLog("parent file does not exist")
            if (parentFile.mkdirs()) {
                showLog("parent file created")
                downLoadFileFromUrl(docFilePath, "${BuildConfig.SERVER_URL_IMAGE}/pdf/$fileName")
            } else {
                showAlertDialog(
                    this,
                    getString(R.string.oops),
                    "Could not create the directory"
                )
            }
        } else {
            showLog("parent file exists")
            downLoadFileFromUrl(docFilePath, "${BuildConfig.SERVER_URL_IMAGE}/pdf/$fileName")
        }
    }

    private fun downLoadFileFromUrl(outputFilePath: String, downloadUrl: String) {
        val progress = ProgressDialog.progressDialog(
            this,
            "Downloading file..."
        )
        progress.show()
        progress.setCanceledOnTouchOutside(false)
        downloadFile(
            this, outputFilePath,
            downloadUrl, {
                progress.dismiss()
                openInvoiceFile(this, outputFilePath)
            }, {
                progress.dismiss()
                showAlertDialog(
                    this,
                    getString(R.string.oops),
                    it
                )
            }
        )
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab_driver_detail_addTransaction -> {
                startActivity(Intent(this, AddCustomerActivity::class.java))
            }

            R.id.iv_driver_detail_optionsIcon -> {
                showOptions()
            }
        }
    }

    private fun showOptions() {
        val popAdapter = OptionsMenuAdapter(
            this,
            resources.getStringArray(R.array.customer_options)
        )
        val mPopupWindow = ListPopupWindow(this)
        mPopupWindow.setAdapter(popAdapter)
        mPopupWindow.anchorView = iv_driver_detail_optionsIcon
        mPopupWindow.width = 400
        mPopupWindow.horizontalOffset = -350 //<--this provides the margin you need
        mPopupWindow.show()
        mPopupWindow.setOnItemClickListener { _, _, position, _ ->
            when (position) {
                0 -> { //edit
                    val intent = Intent(this, AddCustomerActivity::class.java)
                    intent.putExtra(DATA_KEY, customerProfile)
                    startActivity(intent)
                }

                1 -> { // delete
                    showAlertDialog(this, null,
                        "Are you sure you want to delete customer: ${customerProfile.name}?",
                        "Yes", "No", {
                            if (isConnectedToInternet(this))
                                deletePutOperation(
                                    ApiService.create(),
                                    PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                    "${BuildConfig.SERVER_URL}transporters/remove-customer",
                                    DeleteRequest(null, customerProfile._id),
                                    {
                                        showAlertDialog(
                                            this,
                                            null,
                                            "Successfully deleted customer!"
                                        ) {
                                            finish()
                                        }
                                    }, {
                                        showAlertDialog(this, getString(R.string.oops), it.second)
                                    }
                                )
                            else
                                showAlertDialog(
                                    this,
                                    getString(R.string.no_internet_label),
                                    getString(R.string.no_internet)
                                )
                        }, {
                            // do nothing cancel clicked
                        })
                }
            }
            mPopupWindow.dismiss()
        }
    }

    private fun fetchTripData() {
        if (isConnectedToInternet(this)) {
            showLog(customerProfile.toString())
            getCustomersTripsOperation(ApiService.create(),
                customerProfile._id,
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!, {
                    pb_progress.visibility = GONE
                    tripList.clear()
                    tripList.addAll(it)
                    tripAdapter.notifyDataSetChanged()
                    if (tripList.isEmpty())
                        showError(getString(R.string.empty_trips))
                }, {
                    pb_progress.visibility = GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            this, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            logoutUser(this)
                        }
                    } else
                        showError(it.second)
                })
        } else
            showError(getString(R.string.no_internet))
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
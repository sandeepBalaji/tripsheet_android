package com.rostr.android.rostr.tripsheet.dialogs

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.developers.imagezipper.ImageZipper
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.addTripNoteOperation
import com.rostr.android.rostr.tripsheet.network.uploadImageOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.dialog_add_note.*
import kotlinx.android.synthetic.main.image_picker.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

private const val GALLERY = 400
private const val CAMERA_ACTION = 200

class AddNoteDialog(
    val onSuccess: (tripNotes: ArrayList<TripNoteResponse>?) -> Unit
) : BottomSheetDialogFragment(),
    View.OnClickListener {

    private lateinit var mContext: Context
    private var tripData: TripResponse? = null
    private lateinit var parentActivity: FragmentActivity
    private var imagePath: String? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_add_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tripData = arguments?.getParcelable(DATA_KEY)
        bt_add_note_save.setOnClickListener(this)
        iv_add_note_image.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_add_note_save -> {
                if (isEmpty(et_add_note_text)) {
                    showShortToast(mContext, "Please enter a note")
                } else {
                    if (isConnectedToInternet(mContext)) {
                        val request = TripNoteRequest(
                            NoteRequest(
                                notes = et_add_note_text.text.toString(),
                                date = Calendar.getInstance().timeInMillis,
                                by = UserSingleton.instance!!._id,
                                images = imagePath
                            )
                        )
                        addTripNoteOperation(
                            ApiService.create(),
                            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                            tripData?._id!!,
                            request, {
                                showAlertDialog(
                                    mContext,
                                    null,
                                    "Note has been successfully added"
                                ) {
                                    onSuccess(it.tripNotes)
                                    dismiss()
                                }
                            }, {
                                if (it.first == 401) {
                                    showAlertDialog(
                                        mContext, getString(R.string.unauthorized_title),
                                        getString(R.string.logged_out)
                                    ) {
                                        logoutUser(parentActivity)
                                    }
                                } else
                                    showAlertDialog(mContext, getString(R.string.oops), it.second)
                            })
                    } else
                        showAlertDialog(
                            mContext,
                            getString(R.string.no_internet_label),
                            getString(R.string.no_internet)
                        )
                }
            }

            R.id.iv_add_note_image -> {
                checkStoragePermission()
            }
        }
    }

    private fun checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(parentActivity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        showImageDialog()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        showShortToast(
                            mContext,
                            getString(R.string.grant_image_permission)
                        )
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest,
                        token: PermissionToken
                    ) {
                        showAlertDialog(mContext,
                            getString(R.string.permission_required_title),
                            getString(R.string.grant_image_permission),
                            "Yes",
                            "No", {
                                token.continuePermissionRequest()
                            }, {
                                token.cancelPermissionRequest()
                            })
                    }
                }).check()
        } else {
            showImageDialog()
        }
    }

    private fun showImageDialog() {
        val dialog = BottomSheetDialog(mContext)
        val dialogView = layoutInflater.inflate(R.layout.image_picker, null)
        dialogView.tv_image_picker_camera.setOnClickListener {
            showLog("Camera clicked")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Dexter.withActivity(parentActivity)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(object : PermissionListener {
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            showLog("Permission granted")
                            initCamera()
                            dialog.dismiss()
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permission: PermissionRequest?,
                            token: PermissionToken?
                        ) {
                            showAlertDialog(mContext, getString(R.string.permission_required_title),
                                getString(R.string.grant_camera_permission),
                                "Ok", "Cancel", {
                                    token?.continuePermissionRequest()
                                }, {
                                    token?.cancelPermissionRequest()
                                }
                            )
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            showShortToast(mContext, getString(R.string.grant_camera_permission))
                        }
                    }).check()
            } else {
                initCamera()
                dialog.dismiss()
            }
        }

        dialogView.tv_image_picker_gallery.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(intent, GALLERY)
            dialog.dismiss()
        }
        dialogView.tv_image_picker_cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun initCamera() {
        showLog("Camera init")
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val f = File(Environment.getExternalStorageDirectory(), "temp.jpg")
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getFileUri(f))
        startActivityForResult(intent, CAMERA_ACTION)
    }

    private fun getFileUri(mediaFile: File): Uri {
        return FileProvider.getUriForFile(
            mContext,
            BuildConfig.APPLICATION_ID + ".provider",
            mediaFile
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK) {
            when (requestCode) {
                CAMERA_ACTION -> {
                    var f = File(Environment.getExternalStorageDirectory().toString())
                    for (temp: File in f.listFiles()) {
                        if (temp.name == "temp.jpg") {
                            f = temp
                            break
                        }
                    }
                    try {
                        val bitmapOptions: BitmapFactory.Options = BitmapFactory.Options()
                        val bitmap = BitmapFactory.decodeFile(
                            f.absolutePath,
                            bitmapOptions
                        )
                        Glide.with(mContext).load(bitmap).apply(
                            RequestOptions()
                                .centerCrop()
                                .error(R.drawable.ic_add_photo)
                        ).into(iv_add_note_image)

                        val parentPath =
                            "${Environment.getExternalStorageDirectory()}" +
                                    "${File.separator}${getString(R.string.app_name)}"
                        if (f.delete())
                            showLog("temp file deleted")
                        val parentFile = File(parentPath)
                        if (!parentFile.exists()) {
                            if (parentFile.mkdirs())
                                writeToNewFile(parentPath, bitmap)
                            else
                                showAlertDialog(
                                    mContext,
                                    getString(R.string.oops),
                                    "Could not create the directory"
                                )
                        } else
                            writeToNewFile(parentPath, bitmap)
                    } catch (e: Exception) {
                        showLog(e)
                    }
                }

                GALLERY -> {
                    if (data != null) {
                        val selectedImage: Uri? = data.data
                        val filePath = arrayOf(MediaStore.Images.Media.DATA)
                        if (parentActivity.contentResolver != null && selectedImage != null) {
                            val c = parentActivity.contentResolver.query(
                                selectedImage,
                                filePath,
                                null,
                                null,
                                null
                            )
                            if (c != null) {
                                c.moveToFirst()
                                val columnIndex = c.getColumnIndex(filePath[0])
                                val imagePath = c.getString(columnIndex)
                                c.close()
                                val thumbnail = BitmapFactory.decodeFile(imagePath)
                                Glide.with(mContext).load(thumbnail).apply(
                                    RequestOptions()
                                        .centerCrop()
                                        .error(R.drawable.ic_add_photo)
                                ).into(iv_add_note_image)
                                showLog("path of image from gallery......******************.........$imagePath")
                                val compressedFile =
                                    ImageZipper(mContext).compressToFile(File(imagePath))
                                uploadImage(compressedFile)
                                iv_add_note_image.setImageBitmap(thumbnail)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun writeToNewFile(parentPath: String, bitmap: Bitmap) {
        val file = File(parentPath, "${System.currentTimeMillis()}.jpg")
        try {
            val outFile = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile)
            outFile.flush()
            outFile.close()
            val imagePath = file.absolutePath
            showLog("Image path $imagePath")
            val compressedFile = ImageZipper(mContext).compressToFile(file)
            uploadImage(compressedFile)
        } catch (e: FileNotFoundException) {
            showLog(e)
        } catch (e: IOException) {
            showLog(e)
        } catch (e: Exception) {
            showLog(e)
        }
    }

    private fun uploadImage(file: File) {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("introImage", file.name, requestFile)
        val progress = ProgressDialog.progressDialog(mContext, "please wait...")
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        uploadImageOperation(ApiService.create(),
            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
            body, {
                showLog("success")
                progress.dismiss()
                imagePath = it.imageUrl
            }, {
                showLog("error uploading image: ${it.second}")
                progress.dismiss()
                if (it.first == 401) {
                    showAlertDialog(
                        mContext, getString(R.string.unauthorized_title),
                        getString(R.string.logged_out)
                    ) {
                        logoutUser(parentActivity)
                    }
                } else
                    showAlertDialog(mContext, getString(R.string.oops), it.second)
            })
    }
}
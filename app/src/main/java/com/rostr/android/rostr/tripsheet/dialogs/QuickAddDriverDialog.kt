package com.rostr.android.rostr.tripsheet.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.network.*
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.quickadd_user_dialog.*


class QuickAddDriverDialog(val title: String, private val contactDetail: Contacts?) :
    BottomSheetDialogFragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var parentActivity: FragmentActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity!!
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog: BottomSheetDialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog: BottomSheetDialog = it as BottomSheetDialog
            val bottomSheet: FrameLayout =
                dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from(bottomSheet).isHideable = true
        }
        return bottomSheetDialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.quickadd_user_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bt_quick_addButton.setOnClickListener(this)
        tv_quick_add_title.text = title
        if (arguments != null && arguments!!.containsKey(DATA_KEY)) {
            val existingData: UserDriver = arguments!!.getParcelable(DATA_KEY)!!
            et_quick_add_name.setEditable(false)
            et_quick_add_mobileNo.setEditable(false)
            et_quick_add_name.value = existingData.driver?.name
            et_quick_add_mobileNo.value = existingData.driver?.mobileNo
            et_quick_add_upiId.value = existingData.upi
            bt_quick_addButton.text = getString(R.string.update)
        } else {
            if (contactDetail != null) {
                et_quick_add_name.value = contactDetail.contactName
                et_quick_add_mobileNo.value = contactDetail.contactNumber
            }
        }
    }

    override fun onClick(v: View?) {
        if (isConnectedToInternet(mContext)) {
            if (bt_quick_addButton.text == getString(R.string.update)) {
                updateDriverOperation(ApiService.create(),
                    PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                    UpdateDriverUpiRequest(et_quick_add_upiId.value), {

                    }, {

                    })
            } else {
                when {
                    et_quick_add_name.isEmpty ->
                        showShortToast(
                            mContext,
                            "Please enter the driver name"
                        )
                    !et_quick_add_mobileNo.isPhoneValid ->
                        showShortToast(
                            mContext,
                            "Please enter the driver's mobile number"
                        )
                    et_quick_add_vehicleNo.isEmpty -> {
                        showShortToast(
                            mContext,
                            "Please enter the vehicle number"
                        )
                    }
                    et_quick_add_vehicleType.isEmpty -> {
                        showShortToast(
                            mContext,
                            "Please enter the vehicle type"
                        )
                    }
                    else -> {
                        checkIfDriverExists(et_quick_add_mobileNo.value, {
                            if (it == null)
                                addDriverOperation(
                                    ApiService.create(),
                                    PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                                    AddDriverRequest(
                                        name = et_quick_add_name.value,
                                        mobileNo = et_quick_add_mobileNo.value,
                                        upi = et_quick_add_upiId.value,
                                        vehicles = DriverVehicle(
                                            regNumber = et_quick_add_vehicleNo.value,
                                            vehicleType = et_quick_add_vehicleType.value
                                        )
                                    ), {
                                        showSuccess(dialog)

                                    }, { error ->
                                        if (error.first == 401) {
                                            showAlertDialog(
                                                mContext, getString(R.string.unauthorized_title),
                                                getString(R.string.logged_out)
                                            ) {
                                                logoutUser(parentActivity)
                                            }
                                        } else
                                            showAlertDialog(
                                                mContext,
                                                getString(R.string.oops),
                                                error.second
                                            )
                                    })
                            else
                                addDriverToTransporterOperation(
                                    ApiService.create(),
                                    PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                                    LinkExistingDriverRequest(it._id), {
                                        showSuccess(dialog)
                                    }, { error ->
                                        if (error.first == 401) {
                                            showAlertDialog(
                                                mContext, getString(R.string.unauthorized_title),
                                                getString(R.string.logged_out)
                                            ) {
                                                logoutUser(parentActivity)
                                            }
                                        } else
                                            showAlertDialog(
                                                mContext,
                                                getString(R.string.oops),
                                                error.second
                                            )
                                    }
                                )
                        }, {
                            if (it.first == 401) {
                                showAlertDialog(
                                    mContext, getString(R.string.unauthorized_title),
                                    getString(R.string.logged_out)
                                ) {
                                    logoutUser(parentActivity)
                                }
                            } else
                                showAlertDialog(mContext, getString(R.string.oops), it.second)
                        })
                    }
                }
            }
        } else
            showAlertDialog(
                mContext,
                getString(R.string.no_internet_label),
                getString(R.string.no_internet)
            )
    }

    private fun checkIfDriverExists(
        phoneNo: String,
        onSuccess: (response: UserDriver?) -> Unit,
        onFailure: (error: Pair<Int, String>) -> Unit
    ) {
        checkIfTransporterHasDriver(ApiService.create(),
            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
            phoneNo, {
                onSuccess(it)
            }, {
                if (it.first == 400)
                    onSuccess(null)
                else {
                    onFailure(it)
                }
            })
    }

    private fun showSuccess(dialog: Dialog?) {
        dialog?.dismiss()
        showAlertDialog(mContext, null, "Driver added successfully!") {
            parentActivity.finish()
        }
    }
}
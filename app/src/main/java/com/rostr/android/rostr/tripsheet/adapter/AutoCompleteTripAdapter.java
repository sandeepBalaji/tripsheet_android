package com.rostr.android.rostr.tripsheet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rostr.android.rostr.tripsheet.R;
import com.rostr.android.rostr.tripsheet.model.TripResponse;
import com.rostr.android.rostr.tripsheet.model.UserCustomer;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteTripAdapter extends ArrayAdapter<TripResponse> {

    private Context mContext;
    private int itemLayout;

    private List<TripResponse> items, tempItems, suggestions;

    public AutoCompleteTripAdapter(@NonNull Context context, int resource, List<TripResponse> dataList) {
        super(context, resource, dataList);
        mContext = context;
        this.items = dataList;
        tempItems = new ArrayList<>(items); // this makes the difference.
        itemLayout = resource;
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(itemLayout, parent, false);
        }
        TripResponse trip = items.get(position);
        TextView strName = convertView.findViewById(R.id.tv_option_item_text);
        strName.setText(trip.getTripName());
        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((UserCustomer) resultValue).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (TripResponse trip : tempItems) {
                    if (trip.getTripName().toLowerCase()
                            .contains(constraint.toString().toLowerCase()) ||
                            trip.getCustomer().getName().toLowerCase()
                                    .contains(constraint.toString().toLowerCase()) ||
                            (trip.getDriver() != null &&
                                    !trip.getDriver().getName().toLowerCase()
                                            .contains(constraint.toString().toLowerCase()))) {
                        suggestions.add(trip);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<TripResponse> filterList = (ArrayList<TripResponse>) results.values;
            if (results.count > 0) {
                clear();
                for (TripResponse trip : filterList) {
                    add(trip);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
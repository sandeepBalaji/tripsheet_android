package com.rostr.android.rostr.tripsheet.fragments.roleFragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.rostr.android.rostr.tripsheet.CreateProfileActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.RoleParentActivity
import com.rostr.android.rostr.tripsheet.utils.ROLE_KEY
import kotlinx.android.synthetic.main.fragment_select_role.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

class SelectRoleFragment : Fragment(), View.OnClickListener {
    private lateinit var mContext: Context
    private var parentActivity: FragmentActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_select_role, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tv_toolbar_title.text = getString(R.string.select_role)
        tv_select_role_business.setOnClickListener(this)
        tv_select_role_driver.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_select_role_business -> {
                val fragment = SelectBusinessTypeFragment()
                val args = Bundle()
                args.putString(
                    ROLE_KEY,
                    "transporter"
                )
                fragment.arguments = args
                (parentActivity as RoleParentActivity).replaceFragment(fragment)
            }


            R.id.tv_select_role_driver -> {
                val intent = Intent(mContext, CreateProfileActivity::class.java)
                intent.putExtra(
                    ROLE_KEY,
                    tv_select_role_driver.text.toString().toLowerCase(Locale.ENGLISH)
                )
                (parentActivity as RoleParentActivity).startActivityFromFragment(intent)
            }
        }
    }
}
package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.rostr.android.rostr.tripsheet.utils.DATA_KEY
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        formatTerms()
        bt_start.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        tv_start_terms.setOnClickListener {
            val intent = Intent(this, WebActivity::class.java)
            intent.putExtra(DATA_KEY, "http://www.tripsheet.in/terms")
            startActivity(intent)
        }
    }

    private fun formatTerms() {
        val word = SpannableString(getString(R.string.terms))
        word.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorButtonBlue)),
            30,
            word.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        word.setSpan(UnderlineSpan(), 30, word.length, 0)
        tv_start_terms.text = word
    }
}
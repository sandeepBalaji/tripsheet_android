package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity.RESULT_ERROR
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.rostr.android.rostr.tripsheet.adapter.PassengerAdapter
import com.rostr.android.rostr.tripsheet.adapter.SpinnerAdapter
import com.rostr.android.rostr.tripsheet.customviews.LabelledEditText
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.createTripOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_trip.*
import kotlinx.android.synthetic.main.quickadd_user_dialog.view.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*
import kotlin.collections.ArrayList


const val START_DATE_REQUEST = 100
const val END_DATE_REQUEST = 200
const val CUSTOMER_SEARCH_REQUEST = 300
const val DRIVER_SEARCH_REQUEST = 400
const val SOURCE_SEARCH = 500
const val DESTINATION_SEARCH = 600

class AddTripActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var passengerList: ArrayList<PassengerModel>
    private lateinit var passengerAdapter: PassengerAdapter
    private lateinit var startCalendar: Calendar
    private var endCalendar: Calendar? = null
    private var driver: UserDriver? = null
    private var customer: Customer? = null
    //    private var paymentDateCalendar: Calendar? = null
    private var srcAdd: Address? = null
    private var tripCost: Double = 0.0
    private var toll: Double = 0.0
    private var bata: Double = 0.0
    private var otherCost: Double = 0.0
    private var destAdd: Address? = null
    private var gstData: GstModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip)
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            tv_toolbar_title.text = getString(R.string.add_trip)
            startCalendar = Calendar.getInstance()
            tv_trip_startDateTime.setText(getFormattedDateTime(startCalendar))
            bt_trip_actionButton.text = getString(R.string.add_trip)
            bt_trip_addPassenger.paintFlags =
                bt_trip_addPassenger.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            setClickListeners()
            startCalendar = Calendar.getInstance()
            initRecycler()
        }
    }

    private fun setClickListeners() {
        bt_trip_addPassenger.setOnClickListener(this)
        tv_trip_startDateTime.setOnClickListener(this)
        tv_trip_endDateTime.setOnClickListener(this)
        bt_trip_actionButton.setOnClickListener(this)
        tv_trip_assignDriver.setOnClickListener(this)
        tv_trip_assignCustomer.setOnClickListener(this)
        tv_trip_tripAmount.setOnClickListener(this)
//        iv_trip_removeGst.setOnClickListener(this)
        et_trip_location.setSourceClickListener {
            startLocationSearch(SOURCE_SEARCH)
        }
        et_trip_location.setDestClickListener {
            startLocationSearch(DESTINATION_SEARCH)
        }
    }

    private fun startLocationSearch(requestCode: Int) {
        /*
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(this, getString(R.string.maps_api_key))
        }
        // Set the fields to specify which types of place data to return.
        val fields = Arrays.asList(Place.Field.ADDRESS, Place.Field.LAT_LNG)
//        val rectangularBounds = RectangularBounds.newInstance(
//            LatLng(12.864162, 77.438610),
//            LatLng(13.139807, 77.711895)
//        )
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        )
            .setCountry("IN")
//            .setLocationRestriction(rectangularBounds)
            .build(this)
        startActivityForResult(intent, requestCode)
    }

    private fun initRecycler() {
        passengerList = ArrayList()
        rv_trip_customerRecycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@AddTripActivity)
            passengerAdapter =
                PassengerAdapter(this@AddTripActivity, passengerList) { _, position ->
                    passengerList.removeAt(position)
                    passengerAdapter.notifyDataSetChanged()
                }
            adapter = passengerAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_trip_startDateTime -> {
                val dateIntent = Intent(this, DateTimePickerActivity::class.java)
                dateIntent.putExtra(DATE_KEY, startCalendar.timeInMillis)
                startActivityForResult(dateIntent, START_DATE_REQUEST)
            }
            R.id.bt_trip_addPassenger -> {
                showAddPassengerDialog()
            }
            R.id.tv_trip_assignDriver -> {
                val intent = Intent(this, UserSearchActivity::class.java)
                intent.putExtra(DATA_KEY, DRIVER_SEARCH_REQUEST)
                startActivityForResult(intent, DRIVER_SEARCH_REQUEST)
            }
            R.id.tv_trip_assignCustomer -> {
                val intent = Intent(this, UserSearchActivity::class.java)
                intent.putExtra(DATA_KEY, CUSTOMER_SEARCH_REQUEST)
                startActivityForResult(intent, CUSTOMER_SEARCH_REQUEST)
            }
            R.id.tv_trip_tripAmount -> {
                showAmountDialog()
            }

            R.id.tv_trip_endDateTime -> {
                val dateIntent = Intent(this, DateTimePickerActivity::class.java)
                dateIntent.putExtra(DATE_KEY, startCalendar.timeInMillis)
                startActivityForResult(dateIntent, END_DATE_REQUEST)
            }

            else -> {
                val tripTypeRadio = findViewById<Chip>(rg_trip_tripType.checkedChipId)
                if (tripTypeRadio == null) {
                    showShortToast(this, "Please select trip type")
                    return
                }
                if (et_trip_tripName.isEmpty) {
                    showShortToast(this, "Please enter a trip name")
                    return
                }
                if (customer == null) {
                    showShortToast(this, "Please select a customer")
                    return
                }
                if (srcAdd == null) {
                    showShortToast(this, "Please enter a source location")
                    return
                }
                if (destAdd == null) {
                    showShortToast(this, "Please enter a destination")
                    return
                }
                if (endCalendar != null && !isEndAfterStart(endCalendar!!, startCalendar)) {
                    showShortToast(this, "Your end time should be after the start time")
                    return
                }
                showSMSDialog()
            }
        }
    }

    private fun showSMSDialog() {
        val alertLayout = layoutInflater.inflate(R.layout.dialog_sms_layout, null)
        val alert = AlertDialog.Builder(this)
        alert.setView(alertLayout)
        alert.setCancelable(false)
        val customerCb: CheckBox = alertLayout.findViewById(R.id.cb_sms_dialog_customers)
        val passengerCb: CheckBox = alertLayout.findViewById(R.id.cb_sms_dialog_passengers)
        val dialog = alert.create()
        if (passengerList.isEmpty())
            passengerCb.visibility = GONE
        alertLayout.findViewById<Button>(R.id.bt_sms_dialog_doneButton).setOnClickListener {
            doCreate(
                smsToPassenger = passengerCb.isChecked,
                smsToCustomer = customerCb.isChecked
            )
            dialog.dismiss()
        }
        alertLayout.findViewById<Button>(R.id.bt_sms_dialog_cancelButton).setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    /*private fun checkSMSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(this)
                .withPermission(Manifest.permission.SEND_SMS)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        doCreate(true)
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        showAlertDialog(this@AddTripActivity,
                            getString(R.string.permission_required_title),
                            getString(R.string.send_sms_permission),
                            "Ok",
                            "Cancel",
                            {
                                token?.continuePermissionRequest()
                            },
                            {
                                token?.cancelPermissionRequest()
                            }
                        )
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        showShortToast(
                            this@AddTripActivity,
                            getString(R.string.send_sms_permission)
                        )
                    }
                }).check()
        } else {
            doCreate(true)
        }
    }*/

    private fun doCreate(smsToPassenger: Boolean, smsToCustomer: Boolean) {
        createTripOperation(
            ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            getRequestData(), {
                showAlertDialog(
                    this, null,
                    "Trip has been successfully created!"
                ) {
                    finish()
                }
                /*if (sendSms) {
                    if (it.passengers?.isNotEmpty()!!) {
                        it.passengers.forEach {passenger->
                            showLog(passenger.mobileNo)
                            showSMSDialog(passenger.mobileNo)
                        }
                    }
                }*/
            }, {
                showApiCallError(it)
            })
    }

/*private fun sendSMS(mobileNo: String) {
    try {
        val smsManager = SmsManager.getDefault()
        smsManager.sendTextMessage(
            mobileNo,
            null,
            "Trip created.",
            null,
            null
        )
        Toast.makeText(
            this, "SMS Sent!",
            Toast.LENGTH_LONG
        ).show()
    } catch (e: Exception) {
        Toast.makeText(
            this,
            "SMS faild, please try again later!",
            Toast.LENGTH_LONG
        ).show()
        e.printStackTrace()
    }
}*/


/*fun getSIMInfo(context: Context): List<SimInfo> {
    val simInfoList = arrayListOf<SimInfo>()
    val URI_TELEPHONY = Uri.parse("content://telephony/siminfo/")
    val c = this.contentResolver.query(URI_TELEPHONY, null, null, null, null)
    if (c != null && c.moveToFirst()) {
        do {
            val id = c.getInt(c.getColumnIndex("_id"))
            val slot = c.getInt(c.getColumnIndex("slot"))
            val display_name = c.getString(c.getColumnIndex("display_name"))
            val icc_id = c.getString(c.getColumnIndex("icc_id"))
            val simInfo = SimInfo(id, display_name, icc_id, slot)
            showLog(simInfo.toString())
            simInfoList.add(simInfo)
        } while (c.moveToNext())
    }
    c.close()

    return simInfoList
}*/


    private fun getRequestData(): TripCreateRequest {
        val tripTypeRadio = findViewById<Chip>(rg_trip_tripType.checkedChipId)
        val acType = rg_trip_acType.findViewById<RadioButton>(rg_trip_acType.checkedRadioButtonId)
        return TripCreateRequest(
            driver = driver!!.driver?._id!!,
            tripName = et_trip_tripName.value,
            acType = acType.tag.toString(),
            source = srcAdd!!,
            destination = destAdd!!,
            dateOfRide = getOnlyDateInMillis(startCalendar.timeInMillis),
            endDate = if (endCalendar != null) getOnlyDateInMillis(endCalendar!!.timeInMillis) else null,
            startTime = getOnlyTimeInMillis(startCalendar.timeInMillis),
            endTime = if (endCalendar != null) getOnlyTimeInMillis(endCalendar!!.timeInMillis) else null,
            vehicle = driver!!.driver?.vehicle?.id,
            notes = et_trip_note.text.toString(),
            passengers = if (passengerList.isNotEmpty()) passengerList else null,
            customer = customer?._id!!,
            gstInfo = gstData,
            amount = tripCost,
            toll = toll,
            driverBata = bata,
            otherCost = otherCost,
            tripType = tripTypeRadio.text.toString()
        )
    }

    private fun showAmountDialog() {
        val dialog = BottomSheetDialog(this)
        dialog.setOnShowListener {
            val bottomDialog: BottomSheetDialog = it as BottomSheetDialog
            val bottomSheet: FrameLayout =
                bottomDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from(bottomSheet).isHideable = true
        }
        val dialogView = layoutInflater.inflate(R.layout.dialog_payment, null)
        val gstSpinner: Spinner = dialogView.findViewById(R.id.sp_payment_gstType)
        val tripAmount: LabelledEditText = dialogView.findViewById(R.id.et_payment_tripAmount)
        val tollAmount: LabelledEditText = dialogView.findViewById(R.id.et_payment_tollAmount)
        val bataAmount: LabelledEditText = dialogView.findViewById(R.id.et_payment_driverBata)
        val otherAmount: LabelledEditText = dialogView.findViewById(R.id.et_payment_other)
        val gstPercent: LabelledEditText = dialogView.findViewById(R.id.et_payment_gstPercent)
        gstSpinner.apply {
            adapter = SpinnerAdapter(
                this@AddTripActivity,
                resources.getStringArray(R.array.gst_options)
            )
        }
        tripAmount.value = tripCost.toString()
        tollAmount.value = toll.toString()
        bataAmount.value = bata.toString()
        otherAmount.value = otherCost.toString()
        if (gstData != null) {
            if (gstData?.IGST != null && gstData?.IGST!! > 0.0) {
                gstPercent.value = gstData?.IGST.toString()
                gstSpinner.setSelection(2)
            } else if (gstData?.CGST != null && gstData?.CGST!! > 0.0) {
                gstPercent.value =
                    (gstData?.CGST!! + gstData?.SGST!!).toString()
                gstSpinner.setSelection(1)
            }
        }
        dialogView.findViewById<Button>(R.id.bt_payment_done).setOnClickListener {
            tripCost = tripAmount.double
            bata = bataAmount.double
            toll = tollAmount.double
            otherCost = otherAmount.double
            gstData = if (tripAmount.double == 0.0 || gstSpinner.selectedItemPosition == 0)
                null
            else if (gstSpinner.selectedItemPosition == 1) {
                val sGst = gstPercent.double / 2
                val cGst = gstPercent.double / 2
                GstModel(sGst, cGst, null)
            } else {
                GstModel(null, null, gstPercent.double)
            }
            val totalAmount = tripCost + bata + toll + otherCost + tripCost * (gstPercent.double/100)
            tv_trip_tripAmount.setText(getString(R.string.rs_value, totalAmount))
            dialog.dismiss()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun showApiCallError(
        error: Pair<Int
                , String>
    ) {
        if (error.first == 401) {
            showAlertDialog(
                this, getString(R.string.unauthorized_title),
                getString(R.string.logged_out)
            ) {
                PreferenceUtils(this).clearData()
                val intent = Intent(this, SplashActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        } else
            showAlertDialog(this, getString(R.string.oops), error.second)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == START_DATE_REQUEST) {
                if (data?.extras != null && data.hasExtra(DATE_KEY)) {
                    val calendar = Calendar.getInstance()
                    calendar.timeInMillis = data.getLongExtra(DATE_KEY, 0)
                    if (endCalendar != null && !isEndAfterStart(endCalendar!!, startCalendar)) {
                        showShortToast(this, "Your end time should be after the start time")
                        return
                    } else {
                        startCalendar = calendar
                        tv_trip_startDateTime.setText(getFormattedDateTime(startCalendar))
                    }
                }
            } else if (requestCode == END_DATE_REQUEST) {
                if (data?.extras != null && data.hasExtra(DATE_KEY)) {
                    val calendar = Calendar.getInstance()
                    calendar.timeInMillis = data.getLongExtra(DATE_KEY, 0)
                    if (endCalendar != null && !isEndAfterStart(endCalendar!!, startCalendar)) {
                        showShortToast(this, "Your end time should be after the start time")
                        return
                    } else {
                        endCalendar = calendar
                        tv_trip_endDateTime.setText(getFormattedDateTime(endCalendar!!))
                    }
                }
            } else if (requestCode == DRIVER_SEARCH_REQUEST) {
                try {
                    driver = data?.extras!!.getParcelable(DATA_KEY)!!
                    showLog("Selected driver: $driver")
                    tv_trip_assignDriver.setText(driver?.driver?.name)
                } catch (ex: Exception) {
                    showLog(ex)
                    showAlertDialog(this, getString(R.string.oops), ex.localizedMessage)
                }
            } else if (requestCode == CUSTOMER_SEARCH_REQUEST) {
                try {
                    customer = data?.extras!!.getParcelable(DATA_KEY)!!
                    tv_trip_assignCustomer.setText(customer?.name)
                } catch (ex: Exception) {
                    showLog(ex)
                    showAlertDialog(this, getString(R.string.oops), ex.localizedMessage)
                }
            } else if (requestCode == SOURCE_SEARCH || requestCode == DESTINATION_SEARCH) {
                if (data != null) {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    if (requestCode == SOURCE_SEARCH) {
                        try {
                            srcAdd = Address(
                                place.address,
                                Location(place.latLng?.latitude, place.latLng?.longitude)
                            )
                            showLog("Home: $srcAdd")
                            et_trip_location.sourceText = srcAdd?.addressLine
                        } catch (ex: Exception) {
                            showLog(ex)
                            showAlertDialog(
                                this,
                                getString(R.string.oops),
                                getString(R.string.address_error)
                            )
                        }
                    } else if (requestCode == DESTINATION_SEARCH) {
                        try {
                            destAdd = Address(
                                place.address,
                                Location(place.latLng?.latitude, place.latLng?.longitude)
                            )
                            showLog("office: $destAdd")
                            et_trip_location.destText = destAdd?.addressLine
                        } catch (ex: Exception) {
                            showLog(ex)
                            showAlertDialog(
                                this,
                                getString(R.string.oops),
                                getString(R.string.address_error)
                            )
                        }
                    }
                }
            }
        } else if (resultCode == RESULT_ERROR) {
            if (data != null) {
                val status = Autocomplete.getStatusFromIntent(data)
                showLog("$status ${status.statusMessage}")
                showAlertDialog(this, getString(R.string.oops), status.statusMessage!!)
            }
        }
    }

    private fun showAddPassengerDialog() {
        val dialog = BottomSheetDialog(this)
        val dialogView = layoutInflater.inflate(R.layout.quickadd_user_dialog, null)
        val name = dialogView.et_quick_add_name
        val mobile = dialogView.et_quick_add_mobileNo
        dialogView.et_quick_add_upiId.visibility = GONE
        dialogView.et_quick_add_vehicleNo.visibility = GONE
        dialogView.et_quick_add_vehicleType.visibility = GONE
        dialogView.tv_quick_add_title.text = getString(R.string.add_passenger_details)
        dialogView.findViewById<Button>(R.id.bt_quick_addButton).setOnClickListener {
            when {
                name.isEmpty ->
                    showShortToast(
                        this,
                        "Please enter the customer's name"
                    )
                mobile.isEmpty ->
                    showShortToast(
                        this,
                        "Please enter the customer's mobile number"
                    )
                else -> {
                    passengerList.add(
                        PassengerModel(
                            name.value,
                            mobile.value
                        )
                    )
                    showLog("Size ${passengerList.size}")
                    passengerAdapter.notifyDataSetChanged()
                    dialog.dismiss()
                }
            }
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
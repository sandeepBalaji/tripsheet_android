package com.rostr.android.rostr.tripsheet.fragments.tripDetailFragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.SplashActivity
import com.rostr.android.rostr.tripsheet.adapter.TripDetailTransactionAdapter
import com.rostr.android.rostr.tripsheet.model.Data
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getTransactionForTripOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.recycler_layout.*

class TripTransactionFragment : Fragment() {

    private lateinit var mContext: Context
    private lateinit var tripId: String
    private lateinit var transactionList: ArrayList<Data>
    private lateinit var transactionAdapter: TripDetailTransactionAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recycler_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        if (arguments != null && arguments!!.containsKey(DATA_KEY)) {
            tripId = arguments!!.getString(DATA_KEY)!!
            getTransactionsForTrips()
        } else
            showError("No data received from the trip")
    }

    private fun initRecycler() {
        transactionList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
            transactionAdapter =
                TripDetailTransactionAdapter(mContext, transactionList) { _, _ ->

                }
            adapter = transactionAdapter
        }
    }

    private fun getTransactionsForTrips() {
        pb_progress.visibility = VISIBLE
        tv_recyclerMessage.visibility = GONE
        if (isConnectedToInternet(mContext)) {
            getTransactionForTripOperation(ApiService.create(),
                PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                tripId, {
                    pb_progress.visibility = GONE
                    transactionList.clear()
                    transactionList.addAll(it)
                    transactionAdapter.notifyDataSetChanged()
                    if (transactionList.isEmpty())
                        showError(getString(R.string.empty_trip_transactions))
                }, {
                    pb_progress.visibility = GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            mContext, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            PreferenceUtils(mContext).clearData()
                            val intent = Intent(mContext, SplashActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        }
                    } else
                        showError(it.second)
                })
        } else
            showError(getString(R.string.no_internet))
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
    }
}
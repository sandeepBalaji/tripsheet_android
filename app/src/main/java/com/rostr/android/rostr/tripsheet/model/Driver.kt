package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDriver(
    val due: String? = null,
    val _id: String,
    val driver: Driver?,
    val upi: String? = null
) : Parcelable

@Parcelize
data class TransporterDriver(
    val due: String? = null,
    val _id: String,
    val driver: String,
    val upi: String? = null
) : Parcelable

@Parcelize
data class Driver(
    val _id: String,
    val isActive: Boolean,
    val govtIdProof: ArrayList<DocumentModel>,
    val mobileNo: String,
    val name: String,
    val license: LicenceInfo? = null,
    val transporters: ArrayList<String>,
    val verificationDate: Long,
    val verificationStatus: DriverVerificationStatus,
    val vehicle: DriverVehicle?
) : Parcelable

@Parcelize
data class DriverProfile(
    val _id: String,
    val isActive: Boolean,
    val govtIdProof: ArrayList<DocumentModel>,
    val mobileNo: String,
    val name: String,
    val license: LicenceInfo? = null,
    val transporters: ArrayList<DriversTransporter>,
    val verificationDate: Long,
    val verificationStatus: DriverVerificationStatus
) : Parcelable

@Parcelize
data class DriverVerificationStatus(
    val status: Boolean
) : Parcelable

data class AddDriverRequest(
    val name: String,
    val mobileNo: String,
    val upi: String,
    val vehicles: DriverVehicle
)

data class UpdateDriverUpiRequest(
    val upi: String
)

@Parcelize
data class DriverVehicle(
    val id: String? = null,
    val regNumber: String,
    val vehicleType: String
) : Parcelable

data class LinkExistingDriverRequest(
    val driver: String
)
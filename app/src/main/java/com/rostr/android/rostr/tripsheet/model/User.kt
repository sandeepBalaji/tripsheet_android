package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class User(
    val __v: Int,
    val _id: String,
    val id: String?,
    val createdAt: String,
    val gender: String,
    val keywords: List<String>,
    val mobileNo: String,
    val email: String? = null,
    val name: String,
    val driver: Driver?,
    var transporter: UserTransPorter?,
    val updatedAt: String
)

object UserSingleton {
    var instance: User? = null
}

data class CreateProfileRequest(
    val type: String,
    val name: String,
    val mobileNo: String,
    val email: String? = null,
    val gender: String,
    val companyName: String? = null,
    val profileImage: String? = null,
    val license: LicenceInfo? = null,
    val isLogistic: Boolean? = null
)

data class CreateProfileResponse(
    val token: String,
    val user: User
)

@Parcelize
data class LicenceInfo(
    val licenseNo: String,
    val expiryDate: Long
) : Parcelable

data class SwitchProfileRequest(
    val driver: String? = null,
    val transporter: String? = null
)
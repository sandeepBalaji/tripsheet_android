package com.rostr.android.rostr.tripsheet.model

data class SimInfo(
    val id: Int,
    val display_name: String,
    val icc_id: String,
    val slot: Int
)
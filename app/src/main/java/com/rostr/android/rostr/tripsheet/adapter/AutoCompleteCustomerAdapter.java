package com.rostr.android.rostr.tripsheet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rostr.android.rostr.tripsheet.R;
import com.rostr.android.rostr.tripsheet.model.Customer;
import com.rostr.android.rostr.tripsheet.model.UserCustomer;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteCustomerAdapter extends ArrayAdapter<Customer> {

    private Context mContext;
    private int itemLayout;

    private List<Customer> items, tempItems, suggestions;

    public AutoCompleteCustomerAdapter(@NonNull Context context, int resource, List<Customer> dataList) {
        super(context, resource, dataList);
        mContext = context;
        this.items = dataList;
        tempItems = new ArrayList<>(items); // this makes the difference.
        itemLayout = resource;
        suggestions = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(itemLayout, parent, false);
        }
        Customer customer = items.get(position);
        TextView strName = convertView.findViewById(R.id.tv_option_item_text);
        strName.setText(customer.getName());
        return convertView;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    private Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((UserCustomer) resultValue).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Customer customer : tempItems) {
                    if (customer.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Customer> filterList = (ArrayList<Customer>) results.values;
            if (results.count > 0) {
                clear();
                for (Customer customer : filterList) {
                    add(customer);
                    notifyDataSetChanged();
                }
            }
        }
    };
}
package com.rostr.android.rostr.tripsheet

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.RadioButton
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.createCustomerOperation
import com.rostr.android.rostr.tripsheet.network.updateCustomerOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_add_customer.*
import kotlinx.android.synthetic.main.toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AddCustomerActivity : AppCompatActivity(), View.OnClickListener {
    private var addressCall: Call<GoogleAddress>? = null
    private var customerId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_customer)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            tv_toolbar_title.text = getString(R.string.add_customer)
            setListeners()
            if (intent.extras != null && intent.extras!!.containsKey(DATA_KEY)) {
                setData(intent.getParcelableExtra(DATA_KEY))
            }
        }
    }

    private fun setData(customer: Customer) {
        customerId = customer._id
        et_add_customer_name.value = customer.name
        et_add_customer_mobileNo.value = customer.mobileNo
        et_add_customer_email.value = customer.email
        rg_add_customer_gender.check(
            when (customer.gender.toLowerCase(Locale.ENGLISH)) {
                "male" -> R.id.rbMale

                "female" -> R.id.rbFemale

                else -> R.id.rbOthers
            }
        )
        et_add_customer_businessName.value = customer.companyName
        rg_add_customer_type.check(
            when (customer.type.toLowerCase(Locale.ENGLISH)) {
                "corporate" -> R.id.rb_business
                else -> R.id.rb_individual
            }
        )
        et_add_customer_pincode.value = customer.address?.pincode
        et_add_customer_gstNumber.value = customer.gstInfo?.gstIN
        bt_add_customer_addButton.text = getString(R.string.update)
    }

    private fun setListeners() {
        bt_add_customer_addButton.setOnClickListener(this)
        et_add_customer_pincode.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (addressCall != null)
                    addressCall!!.cancel()

                if (s?.length == 6) {
                    getAddressFromPinCode(s.toString())
                    ll_add_customer_locationLayout.visibility = VISIBLE
                    tv_add_customer_state.setText("No State")
                    tv_add_customer_city.setText("No City")
                } else
                    ll_add_customer_locationLayout.visibility = GONE
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        rg_add_customer_type.setOnCheckedChangeListener { radioGroup, checkedId ->
            val radioButton = radioGroup.findViewById<RadioButton>(checkedId)
            if (radioButton.text == getString(R.string.individual))
                et_add_customer_businessName.visibility = GONE
            else
                et_add_customer_businessName.visibility = VISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_add_customer_addButton -> {
                if (isConnectedToInternet(this)) {
                    val gender =
                        rg_add_customer_gender.findViewById<RadioButton>(rg_add_customer_gender.checkedRadioButtonId)
                            .text.toString()
                    val addressLine =
                        if (et_add_customer_address.isEmpty) null else et_add_customer_address.value
                    val state =
                        if (tv_add_customer_state.isEmpty) null else tv_add_customer_state.stringText
                    val city =
                        if (tv_add_customer_city.isEmpty) null else tv_add_customer_city.stringText
                    val pinCode =
                        if (et_add_customer_pincode.isEmpty) null else et_add_customer_pincode.value
                    val customerType =
                        rg_add_customer_type.findViewById<RadioButton>(
                            rg_add_customer_type.checkedRadioButtonId
                        ).tag.toString().toLowerCase(Locale.ENGLISH)
                    val businessName =
                        if (rg_add_customer_type
                                .findViewById<RadioButton>(
                                    rg_add_customer_type
                                        .checkedRadioButtonId
                                ).text == getString(R.string.business)
                        )
                            et_add_customer_businessName.value
                        else
                            null

                    val request = CustomerCreateRequest(
                        name = et_add_customer_name.value,
                        mobileNo = et_add_customer_mobileNo.value,
                        gender = gender,
                        address = Address(addressLine, null, state, city, pinCode),
                        gstInfo = if (et_add_customer_gstNumber.isEmpty) null else CustomerGst(
                            et_add_customer_gstNumber.value
                        ),
                        email = if (et_add_customer_email.isEmailValid) null else et_add_customer_email.value,
                        type = customerType,
                        companyName = businessName
                    )
                    if (customerId != null) {
                        updateCustomerOperation(
                            ApiService.create(),
                            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                            customerId!!,
                            request, {
                                showAlertDialog(
                                    this,
                                    null,
                                    "The customer details has been updated successfully!"
                                ) {
                                    finish()
                                }
                            }, {
                                showAPIError(it)
                            })
                    } else {
                        createCustomerOperation(ApiService.create(),
                            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                            request, {
                                showAlertDialog(this, null, "Created customer successfully!") {
                                    finish()
                                }
                            }, {
                                showAPIError(it)
                            })
                    }
                } else
                    showAlertDialog(
                        this,
                        getString(R.string.no_internet_label),
                        getString(R.string.no_internet)
                    )
            }
        }
    }

    private fun showAPIError(error: Pair<Int, String>) {
        if (error.first == 401) {
            showAlertDialog(
                this, getString(R.string.unauthorized_title),
                getString(R.string.logged_out)
            ) {
                logoutUser(this)
            }
        } else
            showAlertDialog(this, getString(R.string.oops), error.second)
    }

    private fun getAddressFromPinCode(pin: String) {
        val url =
            "https://maps.googleapis.com/maps/api/geocode/json?address=$pin&sensor=true&key=${getString(
                R.string.maps_api_key
            )}"
        addressCall = ApiService.create()!!.getAddressFromPinCode(url)
        addressCall!!.enqueue(object : Callback<GoogleAddress> {
            override fun onFailure(call: Call<GoogleAddress>, t: Throwable) {
                showAlertDialog(
                    this@AddCustomerActivity,
                    getString(R.string.oops),
                    t.localizedMessage
                )
            }

            override fun onResponse(call: Call<GoogleAddress>, response: Response<GoogleAddress>) {
                if (response.isSuccessful) {
                    showLog("response is success")
                    val body = response.body()
                    if (body?.status == "OK") {
                        showLog("response status is OK")
                        et_add_customer_address.value = body.results[0].formatted_address
                        for (components in body.results[0].address_components) {
                            if (components.types.contains("locality")) {
                                showLog("found city ${components.types}")
                                tv_add_customer_city.setText(components.long_name)
                                break
                            }
                        }

                        for (components in body.results[0].address_components) {
                            if (components.types.contains("administrative_area_level_1")) {
                                showLog("found state ${components.types}")
                                tv_add_customer_state.setText(components.long_name)
                                break
                            }
                        }
                    } else {
                        showAlertDialog(
                            this@AddCustomerActivity,
                            getString(R.string.oops),
                            body?.error_message!!
                        )
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
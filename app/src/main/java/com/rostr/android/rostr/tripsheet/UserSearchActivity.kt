package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.View.VISIBLE
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rostr.android.rostr.tripsheet.adapter.CustomersAdapter
import com.rostr.android.rostr.tripsheet.adapter.DriverAdapter
import com.rostr.android.rostr.tripsheet.model.Customer
import com.rostr.android.rostr.tripsheet.model.UserDriver
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getTransportersDriversOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_user_search.*
import kotlinx.android.synthetic.main.recycler_layout.*

class UserSearchActivity : AppCompatActivity(), View.OnClickListener {

    private var requestCode: Int = 0
    private var customerAdapter: CustomersAdapter? = null
    private var driverAdapter: DriverAdapter? = null

    private var customerList: ArrayList<Customer>? = null
    private var driverList: ArrayList<UserDriver>? = null

    private var filteredCustomerList: ArrayList<Customer>? = null
    private var filteredDriverList: ArrayList<UserDriver>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_search)

        requestCode = intent.getIntExtra(DATA_KEY, 0)
        when (requestCode) {
            CUSTOMER_SEARCH_REQUEST ->
                et_user_search.hint = "Search customer by name"
            DRIVER_SEARCH_REQUEST ->
                et_user_search.hint = "Search driver by name"
        }
        iv_user_search_backIcon.setOnClickListener(this)
        iv_user_search_addUser.setOnClickListener(this)
        initRecycler()
        listenForSearch()
    }

    private fun listenForSearch() {
        // search action for keyboard
        et_user_search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    showLog("performing search")
                    performSearch()
                    return true
                }
                return false
            }
        })
    }

    private fun initRecycler() {
        customerList = ArrayList()
        driverList = ArrayList()
        filteredCustomerList = ArrayList()
        filteredDriverList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@UserSearchActivity)
            when (requestCode) {
                CUSTOMER_SEARCH_REQUEST -> {
                    customerAdapter = CustomersAdapter(
                        this@UserSearchActivity,
                        filteredCustomerList!!
                    ) { _, position ->
                        sendUserResultBack(filteredCustomerList!![position])
                    }
                    adapter = customerAdapter
                }
                DRIVER_SEARCH_REQUEST -> {
                    driverAdapter = DriverAdapter(
                        this@UserSearchActivity,
                        filteredDriverList!!, true
                    ) { _, position ->
                        sendUserResultBack(filteredDriverList!![position])
                    }
                    adapter = driverAdapter
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_user_search_addUser -> {
                when (requestCode) {
                    CUSTOMER_SEARCH_REQUEST ->
                        startActivity(Intent(this, AddCustomerActivity::class.java))

                    DRIVER_SEARCH_REQUEST ->
                        startActivity(Intent(this, ContactListActivity::class.java))
                }
            }

            R.id.iv_user_search_backIcon ->
                finish()
        }
    }

    private fun performSearch() {
        try {
            filteredCustomerList?.clear()
            filteredDriverList?.clear()
            val searchText = et_user_search.text.toString()
            showLog("search for $searchText")
            when (requestCode) {
                DRIVER_SEARCH_REQUEST -> {
                    val filtered = driverList?.filter { it.driver?.name!!.contains(searchText) }
                    showLog("result size ${filtered?.size}")
                    filteredDriverList?.addAll(filtered!!)
                    driverAdapter?.notifyDataSetChanged()
                }

                CUSTOMER_SEARCH_REQUEST -> {
                    val filtered = customerList?.filter { it.name.contains(searchText) }
                    showLog("result size ${filtered?.size}")
                    filteredCustomerList?.addAll(filtered!!)
                    customerAdapter?.notifyDataSetChanged()
                }
            }
        } catch (ex: Exception) {
            showLog(ex)
            showAlertDialog(this, getString(R.string.oops), ex.localizedMessage)
        }
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(this)) {
            getTransportersDriversOperation(ApiService.create(),
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!, {
                    customerList?.clear()
                    driverList?.clear()
                    filteredCustomerList?.clear()
                    filteredDriverList?.clear()

                    customerList?.addAll(it.customers)
                    filteredCustomerList?.addAll(it.customers)
                    driverList?.addAll(it.drivers)
                    filteredDriverList?.addAll(it.drivers)

                    customerAdapter?.notifyDataSetChanged()
                    driverAdapter?.notifyDataSetChanged()
                    when (requestCode) {
                        CUSTOMER_SEARCH_REQUEST -> {
                            if (filteredCustomerList!!.isEmpty())
                                showError(getString(R.string.empty_customers))
                        }

                        DRIVER_SEARCH_REQUEST -> {
                            if (filteredDriverList!!.isEmpty())
                                showError(getString(R.string.empty_drivers))
                        }
                    }
                }, {
                    if (it.first == 401) {
                        showAlertDialog(
                            this, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            PreferenceUtils(this).clearData()
                            val intent = Intent(this, SplashActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        }
                    } else
                        showAlertDialog(this, getString(R.string.oops), it.second)
                })
        } else {
            showAlertDialog(
                this, getString(R.string.no_internet_label),
                getString(R.string.no_internet)
            ) {
                finish()
            }
        }
    }

    private fun sendUserResultBack(user: Customer) {
        val intent = Intent()
        intent.putExtra(DATA_KEY, user)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun sendUserResultBack(user: UserDriver) {
        val intent = Intent()
        intent.putExtra(DATA_KEY, user)
        setResult(RESULT_OK, intent)
        finish()
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
    }
}
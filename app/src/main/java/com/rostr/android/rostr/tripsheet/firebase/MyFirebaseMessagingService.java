package com.rostr.android.rostr.tripsheet.firebase;

import android.content.Context;
import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rostr.android.rostr.tripsheet.utils.PreferenceUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.rostr.android.rostr.tripsheet.utils.ConstKeysKt.FCM_TOKEN_KEY;
import static com.rostr.android.rostr.tripsheet.utils.MessageUtilsKt.showLog;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "Messaging Service";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        PreferenceUtils pu = new PreferenceUtils(getApplicationContext());
        showLog("NEW_TOKEN " + s);
        pu.setData(FCM_TOKEN_KEY, s);
//        sendTokenToServer(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        showLog("From: " + remoteMessage.getFrom());

        String title = "", body = "";
        // Check if message contains a notification payload.
        try {
            if (remoteMessage.getNotification() != null) {
                handleNotification(remoteMessage.getNotification().getBody());
                title = remoteMessage.getNotification().getTitle();
                body = remoteMessage.getNotification().getBody();
                showLog("Notification Body: " + title + " " + body);
            }

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                showLog("Data Payload: " + remoteMessage.getData().toString());
                try {
                    Map<String, String> params = new HashMap<>(remoteMessage.getData());
                    JSONObject json = new JSONObject(params);
                    handleDataMessage(json);
                } catch (Exception e) {
                    showLog("Exception: " + e.getMessage());
                }
            } else {
                showLog("No data payload found");
            }
        } catch (Exception jex) {
            showLog(jex);
//            Crashlytics.logException(jex);
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            //DO NOTHING WHEN NOTIFICATION TAG IS RECEIVED
            // app is in foreground, broadcast the push message
            /*Intent pushNotification = new Intent(Globals.PUSH_NOTIFICATION);
            pushNotification.putExtra(JsonKeys.MESSAGE_KEY, message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);*/

            // play notification sound
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
        } /*else {
            // If the app is in background, firebase itself handles the notification
        }*/
    }

    private void handleDataMessage(JSONObject json) {
        /*try {
            String title = json.getString(JsonKeys.TITLE_KEY);
            String body = json.getString(JsonKeys.BODY_KEY);
            String action = json.getString(JsonKeys.ACTION_KEY);
            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                MessageUtils.showLog("push json: " + json.toString());
                // app is in foreground, broadcast the push message
                if (!action.equalsIgnoreCase("REFUND") && json.has(JsonKeys.RIDE_ID_KEY)) {
                    Intent pushNotification = new Intent(Globals.PUSH_NOTIFICATION);
                    pushNotification.putExtra(JsonKeys.MESSAGE_KEY, json.toString());
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                }
                // app is in foreground, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent.putExtra(JsonKeys.MESSAGE_KEY, body);
                resultIntent.putExtra(JsonKeys.TITLE_KEY, getString(R.string.app_name));
                showNotificationMessage(getApplicationContext(), title, body, *//*timestamp,*//* resultIntent);
                storeNotification(json.toString());
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent.putExtra(JsonKeys.MESSAGE_KEY, body);
                resultIntent.putExtra(JsonKeys.TITLE_KEY, title);
                showNotificationMessage(getApplicationContext(), title, body, *//*timestamp,*//* resultIntent);

                MessageUtils.showLog("notification message background: " + json.toString());
                String tripId = json.getString(JsonKeys.RIDE_ID_KEY);
                String userStatus = json.getString(JsonKeys.STATUS_KEY);

                if (userStatus.equalsIgnoreCase("dropped")) {
                    PreferenceUtils pu = new PreferenceUtils(getApplicationContext());
                    pu.setData(JsonKeys.STATUS_KEY, "notrated");
                    pu.setData(JsonKeys.RIDE_KEY, tripId);
                }
            }
        } catch (JSONException e) {
            MessageUtils.showLog("Json Exception: " + e.getMessage());
        } catch (Exception e) {
            MessageUtils.showLog("Exception: " + e.getMessage());
        }*/
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, /*String timeStamp,*/ Intent intent) {
        NotificationUtils notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(/*title*/title, message, /*timeStamp*/"", intent);
    }

    /**
     * Showing notification with text and image
     */
    /*private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }*/
    private void storeNotification(String jsonMessage) {
        /*try {
            PreferenceUtils pu = new PreferenceUtils(getApplicationContext());
            JSONObject notification = new JSONObject(jsonMessage);
            notification.put(JsonKeys.TIMESTAMP_KEY, Calendar.getInstance().getTimeInMillis());
            JSONArray putDataArray;
            if (pu.getStringData(Globals.PUSH_NOTIFICATION) != null) {
                putDataArray = new JSONArray(pu.getStringData(Globals.PUSH_NOTIFICATION));
            } else {
                putDataArray = new JSONArray();
            }
            putDataArray.put(notification);
            pu.setData(Globals.PUSH_NOTIFICATION, putDataArray.toString());
        } catch (JSONException jex) {
            MessageUtils.showLog(jex);
            Crashlytics.logException(jex);
        }*/
    }

    public static void deleteOldNotifications(Context context) {
        /*try {
            PreferenceUtils pu = new PreferenceUtils(context);
            if (pu.getStringData(Globals.PUSH_NOTIFICATION) != null) {
                JSONArray existingNotificationArray = new JSONArray(pu.getStringData(Globals.PUSH_NOTIFICATION));
                for (int i = 0; i < existingNotificationArray.length(); i++) {
                    JSONObject notificationObject = existingNotificationArray.getJSONObject(i);
                    if (DateTimeUtils.isNotificationToBeDeleted(notificationObject.getLong(JsonKeys.TIMESTAMP_KEY)))
                        existingNotificationArray.remove(i);
                }
                pu.setData(Globals.PUSH_NOTIFICATION, existingNotificationArray.toString());
            }
        } catch (Exception ex) {
            MessageUtils.showLog(ex);
        }*/
    }

    public static void deleteAllNotifications(Context context) {
        /*try {
            PreferenceUtils pu = new PreferenceUtils(context);
            if (pu.getStringData(Globals.PUSH_NOTIFICATION) != null) {
                pu.clearNotifications();
            }
        } catch (Exception ex) {
            MessageUtils.showLog(ex);
        }*/
    }
}
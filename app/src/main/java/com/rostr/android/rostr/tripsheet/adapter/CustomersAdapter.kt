package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.Customer
import kotlinx.android.synthetic.main.item_customer.view.*

class CustomersAdapter(
    private val context: Context,
    private val mainList: ArrayList<Customer>,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<CustomersAdapter.CustomerViewHolder>() {

    class CustomerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_customer_item_name
        val phone: TextView = itemView.tv_customer_item_phone
        val type: TextView = itemView.tv_customer_item_customerType
        val businessName: TextView = itemView.tv_customer_item_businessName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        return CustomerViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_customer,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        val currentItem = mainList[position]
        holder.name.text = currentItem.name
        holder.phone.text = currentItem.mobileNo
        if (currentItem.type == "individual") {
            holder.type.visibility = GONE
            holder.businessName.visibility = GONE
        }else{
            holder.type.visibility = VISIBLE
            holder.businessName.visibility = VISIBLE
            holder.type.text = "BIZ"
            holder.businessName.text = currentItem.companyName
        }

        holder.itemView.setOnClickListener {
            onItemClick(it, position)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
package com.rostr.android.rostr.tripsheet.services

import android.app.Service
import android.content.Intent
import android.os.CountDownTimer
import android.os.IBinder
import com.rostr.android.rostr.tripsheet.utils.showLog


class CountDownService : Service() {
    internal var bi = Intent(COUNTDOWN_BR)

    private var cdt: CountDownTimer? = null

    override fun onCreate() {
        super.onCreate()
        showLog("Starting timer...")
        cdt = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                bi.putExtra("countdown", millisUntilFinished)
                sendBroadcast(bi)
            }

            override fun onFinish() {
                showLog("Timer finished")
            }
        }

        cdt!!.start()
    }

    override fun onDestroy() {

        cdt!!.cancel()
        showLog("Timer cancelled")
        super.onDestroy()
    }

    override fun onBind(arg0: Intent): IBinder? {
        return null
    }

    companion object {

        private val TAG = CountDownService::class.java.simpleName

        const val COUNTDOWN_BR = "com.rostr.android.driverapp.rostr.rostrdriver"
    }
}
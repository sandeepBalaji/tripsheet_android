package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class TransactionResponse(
    val _id: String? = null,
    val count: Int,
    val `data`: List<Data>,
    val total: Int
)

@Parcelize
data class DriverX(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val govtIdProof: List<DocumentModel>,
    val isActive: Boolean,
    val mobileNo: Long,
    val name: String,
    val transporters: List<String>,
    val updatedAt: String,
    val verificationDate: Long,
    val verificationStatus: VerificationStatus
) : Parcelable

@Parcelize
data class VerificationStatus(
    val status: Boolean
) : Parcelable

@Parcelize
data class Data(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val date: Long,
    val driver: DriverX,
    val images: List<String>,
    val paymentStatus: String,
    val purpose: String,
    val remarks: String,
    val total: String? = null,
    val transporter: TransporterX,
    val updatedAt: String
) : Parcelable

@Parcelize
data class TransporterX(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val customers: List<String>,
    val drivers: List<DriverXX>,
    val gender: String,
    val isActive: Boolean,
    val mobileNo: String,
    val name: String,
    val updatedAt: String
    /*val vehicles: List<Any>*/
) : Parcelable

@Parcelize
data class DriverXX(
    val _id: String,
    val driver: String,
    val due: Int
) : Parcelable
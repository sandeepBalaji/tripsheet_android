package com.rostr.android.rostr.tripsheet

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ListPopupWindow
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rostr.android.rostr.tripsheet.adapter.OptionsMenuAdapter
import com.rostr.android.rostr.tripsheet.adapter.TransactionAdapter
import com.rostr.android.rostr.tripsheet.dialogs.PayNowDialog
import com.rostr.android.rostr.tripsheet.dialogs.QuickAddDriverDialog
import com.rostr.android.rostr.tripsheet.dialogs.TransactionDialog
import com.rostr.android.rostr.tripsheet.model.Data
import com.rostr.android.rostr.tripsheet.model.DeleteRequest
import com.rostr.android.rostr.tripsheet.model.UserDriver
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.deletePutOperation
import com.rostr.android.rostr.tripsheet.network.getTransactionForDriverOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_driver_detail.*
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.*

class DriverDetailActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var driverProfile: UserDriver
    private lateinit var transactionList: ArrayList<Data>
    private lateinit var transactionAdapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_detail)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            initRecycler()
            if (intent.hasExtra(DATA_KEY)) {
                driverProfile = intent.getParcelableExtra(DATA_KEY)
                tv_toolbar_title.text = getString(R.string.app_name)
                Glide.with(this).load(R.drawable.ic_avatar_male).into(iv_driver_detail_driverImage)
                tv_driver_detail_driverName.text = driverProfile.driver?.name
                bt_recycler_actionButton.setOnClickListener(this)
                fab_driver_detail_addTransaction.setOnClickListener(this)
                iv_driver_detail_optionsIcon.setOnClickListener(this)
                tv_driver_detail_pay.setOnClickListener(this)
            } else {
                showAlertDialog(this, getString(R.string.oops), "Did not receive driver details") {
                    finish()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchTransactionData()
    }

    private fun initRecycler() {
        transactionList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@DriverDetailActivity)
            isNestedScrollingEnabled = false
            transactionAdapter =
                TransactionAdapter(this@DriverDetailActivity, transactionList) { _, position ->
                    val bundle = Bundle()
                    bundle.putParcelable(USER_DATA_KEY, driverProfile)
                    bundle.putParcelable(DATA_KEY, transactionList[position])
                    bundle.putInt(POSITION_KEY, position)
                    startTransactionDialog(bundle)
                }
            adapter = transactionAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_driver_detail_optionsIcon -> {
                showOptions()
            }

            R.id.tv_driver_detail_pay -> {
                if (supportFragmentManager.findFragmentByTag(getString(R.string.add_driver)) != null)
                    (supportFragmentManager.findFragmentByTag(getString(R.string.add_driver)) as BottomSheetDialogFragment).dismiss()
                val payDialog = PayNowDialog(driverProfile)
                payDialog.show(supportFragmentManager, getString(R.string.add_driver))
            }

            R.id.fab_driver_detail_addTransaction -> {
                startTransactionAdd()
            }

            R.id.bt_recycler_actionButton -> {
                startTransactionAdd()
            }
        }
    }

    private fun showOptions() {
        val popAdapter = OptionsMenuAdapter(
            this,
            resources.getStringArray(R.array.driver_options)
        )
        val mPopupWindow = ListPopupWindow(this)
        mPopupWindow.setAdapter(popAdapter)
        mPopupWindow.anchorView = iv_driver_detail_optionsIcon
        mPopupWindow.width = 350
        mPopupWindow.horizontalOffset = -300 //<--this provides the margin you need
        mPopupWindow.show()
        mPopupWindow.setOnItemClickListener { _, _, position, _ ->
            when (position) {
                0 -> { // edit
                    if (supportFragmentManager.findFragmentByTag(getString(R.string.update_driver_upi)) != null)
                        (supportFragmentManager.findFragmentByTag(getString(R.string.update_driver_upi)) as BottomSheetDialogFragment).dismiss()
                    val updateDriverDialog =
                        QuickAddDriverDialog(getString(R.string.update_driver_upi), null)
                    val bundle = Bundle()
                    bundle.putParcelable(DATA_KEY, driverProfile)
                    updateDriverDialog.arguments = bundle
                    updateDriverDialog.show(supportFragmentManager, getString(R.string.update_driver_upi))
                }

                1 -> { // delete
                    showAlertDialog(this, null,
                        "Are you sure you want to delete driver: ${driverProfile.driver?.name}?",
                        "Yes", "No", {
                            if (isConnectedToInternet(this))
                                deletePutOperation(
                                    ApiService.create(),
                                    PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                    "${BuildConfig.SERVER_URL}transporters/remove-driver",
                                    DeleteRequest(null, driverProfile.driver?._id),
                                    {
                                        showAlertDialog(
                                            this,
                                            null,
                                            "Successfully deleted driver!"
                                        ) {
                                            finish()
                                        }
                                    }, {
                                        showAlertDialog(this, getString(R.string.oops), it.second)
                                    }
                                )
                            else
                                showAlertDialog(
                                    this,
                                    getString(R.string.no_internet_label),
                                    getString(R.string.no_internet)
                                )
                        }, {

                        })
                }
            }
            mPopupWindow.dismiss()
        }
    }

    private fun startTransactionAdd() {
        val bundle = Bundle()
        bundle.putParcelable(USER_DATA_KEY, driverProfile)
        startTransactionDialog(bundle)
    }

    private fun startTransactionDialog(bundle: Bundle) {
        if (supportFragmentManager.findFragmentByTag(getString(R.string.add_transaction)) != null)
            (supportFragmentManager.findFragmentByTag(getString(R.string.add_transaction)) as BottomSheetDialogFragment).dismiss()
        val addTransactionDialog = TransactionDialog {
            if (bundle.containsKey(POSITION_KEY))
                transactionList.add(bundle.getInt(POSITION_KEY), it)
            else
                transactionList.add(it)
            transactionAdapter.notifyDataSetChanged()
        }
        addTransactionDialog.arguments = bundle
        addTransactionDialog.show(supportFragmentManager, getString(R.string.add_transaction))
    }

    private fun fetchTransactionData() {
        if (isConnectedToInternet(this)) {
            pb_progress.visibility = VISIBLE
            tv_recyclerMessage.visibility = GONE
            bt_recycler_actionButton.visibility = GONE
            getTransactionForDriverOperation(ApiService.create(),
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                driverProfile.driver?._id!!, {
                    pb_progress.visibility = GONE
                    transactionList.clear()
                    transactionList.addAll(it.data)
                    if (transactionList.isEmpty()) {
                        showError(getString(R.string.empty_driver_transactions))
                        fab_driver_detail_addTransaction.visibility = GONE
                    } else
                        fab_driver_detail_addTransaction.visibility = VISIBLE
                    transactionAdapter.notifyDataSetChanged()
                }, {
                    pb_progress.visibility = GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            this, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            logoutUser(this)
                        }
                    } else
                        showError(it.second)
                })
        } else
            showError(getString(R.string.no_internet))
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
        bt_recycler_actionButton.text = getString(R.string.add_transaction)
        bt_recycler_actionButton.visibility = VISIBLE
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_toolbar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
            R.id.nav_notifications -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
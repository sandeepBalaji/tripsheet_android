package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.ReportResponse
import com.rostr.android.rostr.tripsheet.utils.getFormattedDateTime
import kotlinx.android.synthetic.main.item_report.view.*
import java.util.*

class ReportsAdapter(
    private val context: Context,
    private val mainList: ArrayList<ReportResponse>,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<ReportsAdapter.ReportViewHolder>() {

    class ReportViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val number: TextView = itemView.tv_report_item_count
        val date: TextView = itemView.tv_report_item_date
        val name: TextView = itemView.tv_report_item_reportName
        val downloadIcon: ImageView = itemView.iv_report_item_downloadIcon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder {
        return ReportViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_report,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ReportViewHolder, position: Int) {
        val currentItem = mainList[position]
        holder.number.text = "${position + 1}."
        val fileName = currentItem.url?.split("/")
        holder.name.text = fileName!![fileName.lastIndex]
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = currentItem.date
        holder.date.text = getFormattedDateTime(calendar)
        if (currentItem.status == "COMPLETED") {
            holder.downloadIcon.setOnClickListener {
                onItemClick(it, position)
            }
        } else {
            holder.downloadIcon.visibility = GONE
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
    }
}
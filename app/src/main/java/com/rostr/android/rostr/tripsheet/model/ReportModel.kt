package com.rostr.android.rostr.tripsheet.model

data class ReportResponse(
    val _id: String,
    val status: String,
    val date: Long,
    val url: String?
)
package com.rostr.android.rostr.tripsheet.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Typeface
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.rostr.android.rostr.tripsheet.CreateProfileActivity
import com.rostr.android.rostr.tripsheet.HomeActivity
import com.rostr.android.rostr.tripsheet.LoginActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.verifyOtpOperation
import com.rostr.android.rostr.tripsheet.services.CountDownService
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.fragment_verification.*
import kotlin.properties.Delegates


class VerificationFragment : Fragment() {

    private lateinit var mContext: Context
    private lateinit var parentActivity: LoginActivity
    private var fcm: String? = null
    private var signinRes: SignInRes? = null
    private lateinit var mobileNumber: String

    private var showResend: Boolean by Delegates.observable(false) { _, _, newValue ->
        if (newValue) {
            tv_resend.visibility = View.VISIBLE
            tv_countDown.visibility = View.GONE
            changeContent()
        } else {
            tv_resend.visibility = View.GONE
            tv_countDown.visibility = View.GONE
        }
    }

    private var showCountDown: Boolean by Delegates.observable(false) { _, _, newValue ->
        if (newValue) {
            showLog("starting coundownService")
            mContext.startService(Intent(mContext, CountDownService::class.java))
            showResend = false
        } else {
            showLog("stopping coundownService")
            mContext.stopService(Intent(mContext, CountDownService::class.java))
            tv_countDown.visibility = View.GONE
        }
    }

    private fun changeContent() {
        tv_resend.visibility = View.VISIBLE
        tv_resend.text = getSpannedString()
    }

    private fun getSpannedString(): Spannable {
        val message = "Didn't receive the verification code? Resend code"
        val splitText = message.split("?")
        val compulsoryString = SpannableStringBuilder(splitText[1])
        compulsoryString.setSpan(
            StyleSpan(Typeface.BOLD),
            0, splitText[1].length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        return compulsoryString
    }

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            updateTimerUI(intent)
        }
    }

    fun updateTimerUI(intent: Intent) {
        if (intent.extras != null) {
            val millisUntilFinished = intent.getLongExtra("countdown", 0)
            tv_countDown.text = getString(R.string.seconds_value, millisUntilFinished / 1000)
            when ((millisUntilFinished / 1000).toInt()) {
                0 -> {
                    showCountDown = false
                    showResend = true
                }
                else -> {
                    tv_countDown.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity as LoginActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_verification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            showLog("verify bundle", arguments!!)
            signinRes = arguments!!.getParcelable(DATA_KEY)!!
            mobileNumber = arguments!!.getString(MOBILE_NO)!!
        }
        initClickListeners()
        generateFCMToken({
            fcm = it
        }, {
            showLog("FCM TOKEN GENERATION FAILS :\t$it")
        })
        mContext.startService(Intent(mContext, CountDownService::class.java))
        showResend = false
    }

    private fun generateFCMToken(
        onSuccess: (fcm: String?) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    showLog("fcm received  ${task.result!!.token}")
                    onSuccess(task.result!!.token)
                    return@OnCompleteListener
                } else
                // Get new FCM token
                    onFailure("fcm getting failed  ${task.exception}")
            })
    }

    private fun initClickListeners() {
        bt_verify.setOnClickListener {
            if (otp.isEmpty) {
                showShortToast(mContext, getString(R.string.empty_otp))
            } else {
                if (isConnectedToInternet(mContext))
                    doVerify()
                else
                    showAlertDialog(
                        mContext,
                        getString(R.string.no_internet_label),
                        getString(R.string.no_internet)
                    )
            }
        }

        tv_resend.setOnClickListener {
            if (isConnectedToInternet(mContext))
                parentActivity.doSignIn(PreferenceUtils(mContext).getString("mobileNo")!!)
            else
                showAlertDialog(
                    mContext,
                    getString(R.string.no_internet_label),
                    getString(R.string.no_internet)
                )
        }
    }

    override fun onResume() {
        super.onResume()
        mContext.registerReceiver(broadcastReceiver, IntentFilter(CountDownService.COUNTDOWN_BR))
    }

    override fun onStop() {
        super.onStop()
        try {
            mContext.unregisterReceiver(broadcastReceiver)
        } catch (e: Exception) {
            // Receiver was probably already stopped in onPause()
            showLog(e)
        }
    }

    private fun doVerify() {
        val tempToken = signinRes?.authKey
        if (tempToken != null) {
            if (fcm != null) {
                val uniqueId = Settings.Secure.getString(
                    mContext.contentResolver,
                    Settings.Secure.ANDROID_ID
                )
                val deviceInfo = DeviceInfo("Android", fcm!!, uniqueId)
                val mobileStore =
                    MobileStoreInfo(
                        deviceInfo,
                        signinRes?.mobileStore!!.id,
                        mobileNumber,
                        signinRes?.mobileStore!!.status
                    )
                val verifyRequest = VerifyRequest(
                    tempToken,
                    mobileStore,
                    otp.value
                )
                parentActivity.showProgress = true
                verifyOtpOperation(ApiService.create(), verifyRequest, {
                    parentActivity.showProgress = false
                    if (it.user != null) {
                        PreferenceUtils(mContext).setData(ACCESS_TOKEN, "Bearer ${it.token}")
                        PreferenceUtils(mContext).setData(ID_KEY, it.user._id)
                        UserSingleton.instance = it.user
                        when {
                            it.user.driver != null && it.user.driver.isActive -> {
                                PreferenceUtils(mContext).setData(ROLE_KEY, "driver")
                                navigate(it)
                            }

                            it.user.transporter != null && it.user.transporter!!.isActive -> {
                                PreferenceUtils(mContext).setData(ROLE_KEY, "transporter")
                                navigate(it)
                            }
                            else -> {
                                showAlertDialog(
                                    mContext, getString(R.string.oops),
                                    "None of your profiles are active. Please contact our support team"
                                ) {
                                    parentActivity.finish()
                                }
                            }
                        }
                    } else {
                        navToProfileCreation(it.token)
                    }
                }, {
                    parentActivity.showProgress = false
                    showAlertDialog(mContext, getString(R.string.oops), it.second)
                    showCountDown = false
                    showResend = true
                })
            }
        }
    }

    private fun navigate(response: VerifyResponse) {
        when {
            response.user?.driver == null && response.user?.transporter == null -> {
                navToProfileCreation(response.token)
            }
            else -> {
                val intent = Intent(mContext, HomeActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }
    }

    private fun navToProfileCreation(tempToken: String) {
        PreferenceUtils(mContext).setData(TEMP_TOKEN, "Bearer $tempToken")
//        val intent = Intent(mContext, RoleParentActivity::class.java)
        val intent = Intent(mContext, CreateProfileActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(
            ROLE_KEY,
            "transporter"
        )
        startActivity(intent)
    }

    fun doAfterResendOperation() {
        parentActivity.replaceFragment(VerificationFragment())
    }
}
package com.rostr.android.rostr.tripsheet.network

import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.utils.showLog
import okhttp3.MultipartBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

fun checkVersionOperation(
    apiService: ApiService?,
    versionRequest: VersionRequest,
    onSuccess: (response: VersionResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.checkVersion(versionRequest)!!
        .enqueue(object : Callback<VersionResponse> {
            override fun onFailure(call: Call<VersionResponse>, t: Throwable) {
                showLog("fail to get version check  ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<VersionResponse>,
                response: Response<VersionResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }
        })
}

fun updateTransporterProfileOperation(
    apiService: ApiService?,
    header: String,
    request: UpdateTransporterRequest,
    onSuccess: (response: UserTransPorter) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.updateTransporterProfile(header, request)!!
        .enqueue(object : Callback<UserTransPorter> {
            override fun onFailure(call: Call<UserTransPorter>, t: Throwable) {
                showLog("fail update transporter ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<UserTransPorter>,
                response: Response<UserTransPorter>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }
        })
}

fun signInOperation(
    apiService: ApiService?,
    signInRequest: SignInRequest,
    onSuccess: (signInRes: SignInRes) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.loginUser(signInRequest)!!
        .enqueue(object : Callback<SignInRes> {
            override fun onFailure(call: Call<SignInRes>, t: Throwable) {
                showLog("fail to get data in sign in operation ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(call: Call<SignInRes>, response: Response<SignInRes>) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun verifyOtpOperation(
    apiService: ApiService?,
    verifyRequest: VerifyRequest,
    onSuccess: (verifyRes: VerifyResponse) -> Unit,
    onFailure: (Pair<Int, String>) -> Unit
) {
    apiService?.verifyOtp(verifyRequest)!!
        .enqueue(object : Callback<VerifyResponse> {
            override fun onFailure(call: Call<VerifyResponse>, t: Throwable) {
                showLog("fail to verify ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<VerifyResponse>,
                response: Response<VerifyResponse>
            ) {
                if (response.code() == 201 || response.code() == 200) {
                    val res = response.body()
                    if (res != null) {
                        showLog("otp response is $res")
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun createProfileOperation(
    apiService: ApiService?,
    accessToken: String,
    profileRequest: CreateProfileRequest,
    onSuccess: (signInRes: CreateProfileResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.createProfile(accessToken, profileRequest)!!
        .enqueue(object : Callback<CreateProfileResponse> {
            override fun onFailure(call: Call<CreateProfileResponse>, t: Throwable) {
                showLog("fail to create profile ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<CreateProfileResponse>,
                response: Response<CreateProfileResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getMyProfileOperation(
    apiService: ApiService?,
    accessToken: String,
    url: String,
    onSuccess: (signInRes: User) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getMyProfile(accessToken, url)!!
        .enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                showLog("fail to get profile data ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<User>,
                response: Response<User>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getTransportersDriversOperation(
    apiService: ApiService?,
    accessToken: String,
    onSuccess: (response: Transporter) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTransportersDrivers(accessToken)!!
        .enqueue(object : Callback<Transporter> {
            override fun onFailure(call: Call<Transporter>, t: Throwable) {
                showLog("fail to get my drivers ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<Transporter>,
                response: Response<Transporter>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun checkIfTransporterHasDriver(
    apiService: ApiService?,
    accessToken: String,
    phoneNumber: String,
    onSuccess: (response: UserDriver) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.searchDriverWithNumber(phoneNumber, accessToken)!!
        .enqueue(object : Callback<UserDriver> {
            override fun onFailure(call: Call<UserDriver>, t: Throwable) {
                showLog("fail to get check if driver exist ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<UserDriver>,
                response: Response<UserDriver>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun addDriverOperation(
    apiService: ApiService?,
    accessToken: String,
    request: AddDriverRequest,
    onSuccess: (response: UserDriver) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.addDriver(accessToken, request)!!
        .enqueue(object : Callback<UserDriver> {
            override fun onFailure(call: Call<UserDriver>, t: Throwable) {
                showLog("fail to add driver ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<UserDriver>,
                response: Response<UserDriver>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun updateDriverOperation(
    apiService: ApiService?,
    accessToken: String,
    request: UpdateDriverUpiRequest,
    onSuccess: (response: AnonymousResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.updateDriverUPI(accessToken, request)!!
        .enqueue(object : Callback<AnonymousResponse> {
            override fun onFailure(call: Call<AnonymousResponse>, t: Throwable) {
                showLog("fail to update driver ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<AnonymousResponse>,
                response: Response<AnonymousResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun addDriverToTransporterOperation(
    apiService: ApiService?,
    accessToken: String,
    request: LinkExistingDriverRequest,
    onSuccess: (response: UserDriver) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.addExistingDriverToOperator(accessToken, request)!!
        .enqueue(object : Callback<UserDriver> {
            override fun onFailure(call: Call<UserDriver>, t: Throwable) {
                showLog("fail to add driver to transporter ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<UserDriver>,
                response: Response<UserDriver>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun createTripOperation(
    apiService: ApiService?,
    accessToken: String,
    tripRequest: TripCreateRequest,
    onSuccess: (signInRes: TripResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.createTrip(accessToken, tripRequest)!!
        .enqueue(object : Callback<TripResponse> {
            override fun onFailure(call: Call<TripResponse>, t: Throwable) {
                showLog("fail to create trip ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<TripResponse>,
                response: Response<TripResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun createCustomerOperation(
    apiService: ApiService?,
    accessToken: String,
    request: CustomerCreateRequest,
    onSuccess: (/*response: Customer*/) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.createCustomer(accessToken, request)!!
        .enqueue(object : Callback<Customer> {
            override fun onFailure(call: Call<Customer>, t: Throwable) {
                showLog("failed reading response ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<Customer>,
                response: Response<Customer>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess()
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun updateCustomerOperation(
    apiService: ApiService?,
    accessToken: String,
    customerId: String,
    request: CustomerCreateRequest,
    onSuccess: (/*response: Customer*/) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.updateCustomer(accessToken, customerId, request)!!
        .enqueue(object : Callback<Customer> {
            override fun onFailure(call: Call<Customer>, t: Throwable) {
                showLog("failed reading response ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<Customer>,
                response: Response<Customer>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess()
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun updateTripOperation(
    apiService: ApiService?,
    accessToken: String,
    tripId: String,
    tripRequest: TripCreateRequest,
    onSuccess: (signInRes: TripResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.updateTrip(accessToken, tripId, tripRequest)!!
        .enqueue(object : Callback<TripResponse> {
            override fun onFailure(call: Call<TripResponse>, t: Throwable) {
                showLog("fail to get data in sign in operation")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<TripResponse>,
                response: Response<TripResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun createTransactionOperation(
    apiService: ApiService?,
    accessToken: String,
    request: TransactionRequest,
    onSuccess: (response: Data) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.createTransaction(accessToken, request)!!
        .enqueue(object : Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                showLog("fail to create transaction ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<Data>,
                response: Response<Data>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getTransactionForDriverOperation(
    apiService: ApiService?,
    accessToken: String,
    driverId: String,
    onSuccess: (response: TransactionResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTransactionsForDriver(driverId, accessToken)!!
        .enqueue(object : Callback<TransactionResponse> {
            override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                showLog("fail to transaction ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<TransactionResponse>,
                response: Response<TransactionResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getTransactionForTripOperation(
    apiService: ApiService?,
    accessToken: String,
    tripId: String,
    onSuccess: (response: ArrayList<Data>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTransactionsForTrips(tripId, accessToken)!!
        .enqueue(object : Callback<ArrayList<Data>> {
            override fun onFailure(call: Call<ArrayList<Data>>, t: Throwable) {
                showLog("fail to transaction ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<ArrayList<Data>>,
                response: Response<ArrayList<Data>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getTransactionForTransporterOperation(
    apiService: ApiService?,
    accessToken: String,
    transporterId: String,
    onSuccess: (response: TransactionResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTransactionsForTransporter(transporterId, accessToken)!!
        .enqueue(object : Callback<TransactionResponse> {
            override fun onFailure(call: Call<TransactionResponse>, t: Throwable) {
                showLog("fail to transaction ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<TransactionResponse>,
                response: Response<TransactionResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun updateTransactionOperation(
    apiService: ApiService?,
    accessToken: String,
    request: TransactionRequest,
    transactionId: String,
    onSuccess: (response: Data) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.updateTransaction(accessToken, transactionId, request)!!
        .enqueue(object : Callback<Data> {
            override fun onFailure(call: Call<Data>, t: Throwable) {
                showLog("fail to update transaction ${t.localizedMessage}")
                onFailure(
                    Pair(
                        500,
                        "Something is wrong with the response of our server. Please contact our support team"
                    )
                )
            }

            override fun onResponse(
                call: Call<Data>,
                response: Response<Data>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getMyTripsOperation(
    apiService: ApiService?,
    accessToken: String,
    onSuccess: (response: ArrayList<TripResponse>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getMyTrips(accessToken)!!.enqueue(object : Callback<ArrayList<TripResponse>> {
        override fun onFailure(call: Call<ArrayList<TripResponse>>, t: Throwable) {
            showLog("fail to get trip data ${t.localizedMessage}")
            onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
        }

        override fun onResponse(
            call: Call<ArrayList<TripResponse>>,
            response: Response<ArrayList<TripResponse>>
        ) {
            if (response.code() == 200 || response.code() == 201) {
                val res = response.body()
                if (res != null) {
                    onSuccess(res)
                } else {
                    onFailure(
                        Pair(
                            500,
                            "Something went wrong, we did not get response from our server. Please try again later"
                        )
                    )
                }
            } else {
                onFailure(handleErrorResponse(response))
            }
        }

    })
}

fun getTransportersTripsOperation(
    apiService: ApiService?,
    accessToken: String,
    onSuccess: (response: ArrayList<TripResponse>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTransportersTrips(accessToken)!!
        .enqueue(object : Callback<ArrayList<TripResponse>> {
            override fun onFailure(call: Call<ArrayList<TripResponse>>, t: Throwable) {
                showLog("fail to get trip data ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<ArrayList<TripResponse>>,
                response: Response<ArrayList<TripResponse>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getCustomersTripsOperation(
    apiService: ApiService?,
    customerId: String,
    accessToken: String,
    onSuccess: (tripResponse: ArrayList<TripResponse>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getCustomerTrips(customerId, accessToken)!!
        .enqueue(object : Callback<ArrayList<TripResponse>> {
            override fun onFailure(call: Call<ArrayList<TripResponse>>, t: Throwable) {
                showLog("fail to get trip data ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<ArrayList<TripResponse>>,
                response: Response<ArrayList<TripResponse>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun switchProfileOperation(
    apiService: ApiService?,
    accessToken: String,
    request: SwitchProfileRequest,
    onSuccess: (response: SwitchProfileRequest) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.switchProfile(accessToken, request)!!
        .enqueue(object : Callback<SwitchProfileRequest> {
            override fun onFailure(call: Call<SwitchProfileRequest>, t: Throwable) {
                showLog("fail to switch profile ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<SwitchProfileRequest>,
                response: Response<SwitchProfileRequest>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }
        })
}

fun getDriversTripOperation(
    apiService: ApiService?,
    accessToken: String,
    onSuccess: (tripResponse: ArrayList<TripResponse>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getAssignedTrips(accessToken)!!
        .enqueue(object : Callback<ArrayList<TripResponse>> {
            override fun onFailure(call: Call<ArrayList<TripResponse>>, t: Throwable) {
                showLog("fail to get drivers trips ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<ArrayList<TripResponse>>,
                response: Response<ArrayList<TripResponse>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getDriverTripsOperation(
    apiService: ApiService?,
    accessToken: String,
    driverId: String,
    onSuccess: (tripResponse: ArrayList<TripResponse>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getDriverTrips(driverId, accessToken)!!
        .enqueue(object : Callback<ArrayList<TripResponse>> {
            override fun onFailure(call: Call<ArrayList<TripResponse>>, t: Throwable) {
                showLog("fail to create alternate profile ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<ArrayList<TripResponse>>,
                response: Response<ArrayList<TripResponse>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun deletePutOperation(
    apiService: ApiService?,
    accessToken: String,
    url: String,
    request: DeleteRequest,
    onSuccess: (tripResponse: AnonymousResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.deletePutEntries(accessToken, url, request)!!
        .enqueue(object : Callback<AnonymousResponse> {
            override fun onFailure(call: Call<AnonymousResponse>, t: Throwable) {
                showLog("fail to create alternate profile ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<AnonymousResponse>,
                response: Response<AnonymousResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getDriversProfileOperation(
    apiService: ApiService?,
    accessToken: String,
    onSuccess: (response: DriverProfile) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getDriversProfile(accessToken)!!
        .enqueue(object : Callback<DriverProfile> {
            override fun onFailure(call: Call<DriverProfile>, t: Throwable) {
                showLog("fail to get driver profile ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(call: Call<DriverProfile>, response: Response<DriverProfile>) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }
        })
}

fun deleteOperation(
    apiService: ApiService?,
    accessToken: String,
    url: String,
    onSuccess: (tripResponse: AnonymousResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.deleteEntries(accessToken, url)!!
        .enqueue(object : Callback<AnonymousResponse> {
            override fun onFailure(call: Call<AnonymousResponse>, t: Throwable) {
                showLog("fail to delete ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<AnonymousResponse>,
                response: Response<AnonymousResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun alternateSignInOperation(
    apiService: ApiService?,
    accessToken: String,
    request: CreateProfileRequest,
    userId: String,
    onSuccess: (response: User) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.createAlternateProfile(accessToken, userId, request)!!
        .enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                showLog("fail to create alternate profile ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<User>,
                response: Response<User>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun addTripNoteOperation(
    apiService: ApiService?,
    accessToken: String,
    tripId: String,
    request: TripNoteRequest,
    onSuccess: (userList: TripResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.addTripNote(accessToken, tripId, request)!!
        .enqueue(object : Callback<TripResponse> {
            override fun onFailure(call: Call<TripResponse>, t: Throwable) {
                showLog("fail to post note trip ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<TripResponse>,
                response: Response<TripResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}


fun getInvoiceOperation(
    apiService: ApiService?,
    accessToken: String,
    tripId: String,
    onSuccess: (response: AnonymousResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getInvoice(accessToken, tripId)!!
        .enqueue(object : Callback<AnonymousResponse> {
            override fun onFailure(call: Call<AnonymousResponse>, t: Throwable) {
                showLog("fail to get invoice ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<AnonymousResponse>,
                response: Response<AnonymousResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun generateTransactionReportsOperation(
    apiService: ApiService?,
    accessToken: String,
    driverId: String?,
    startDate: Long?,
    endDate: Long?,
    onSuccess: (response: AnonymousResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTransactionReports(accessToken, driverId, startDate, endDate)!!
        .enqueue(object : Callback<AnonymousResponse> {
            override fun onFailure(call: Call<AnonymousResponse>, t: Throwable) {
                showLog("fail to get transaction reports ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<AnonymousResponse>,
                response: Response<AnonymousResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun generateTripReportsOperation(
    apiService: ApiService?,
    accessToken: String,
    customerId: String?,
    driverId: String?,
    tripType: String?,
    startDate: Long?,
    endDate: Long?,
    onSuccess: (response: AnonymousResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getTripReports(accessToken, customerId, driverId, startDate, endDate, tripType)!!
        .enqueue(object : Callback<AnonymousResponse> {
            override fun onFailure(call: Call<AnonymousResponse>, t: Throwable) {
                showLog("fail to get transaction reports ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<AnonymousResponse>,
                response: Response<AnonymousResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun getReportsOperation(
    apiService: ApiService?,
    accessToken: String,
    onSuccess: (response: ArrayList<ReportResponse>) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.getAllReports(accessToken)!!
        .enqueue(object : Callback<ArrayList<ReportResponse>> {
            override fun onFailure(call: Call<ArrayList<ReportResponse>>, t: Throwable) {
                showLog("fail to get reports ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<ArrayList<ReportResponse>>,
                response: Response<ArrayList<ReportResponse>>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else {
                    onFailure(handleErrorResponse(response))
                }
            }

        })
}

fun uploadImageOperation(
    apiService: ApiService?,
    accessToken: String,
    body: MultipartBody.Part,
    onSuccess: (response: UploadImageResponse) -> Unit,
    onFailure: (error: Pair<Int, String>) -> Unit
) {
    apiService?.uploadImage(accessToken, body)!!
        .enqueue(object : Callback<UploadImageResponse> {
            override fun onFailure(call: Call<UploadImageResponse>, t: Throwable) {
                showLog("fail to upload data ${t.localizedMessage}")
                onFailure(Pair(500, "Something went wrong with our server. Please try again later"))
            }

            override fun onResponse(
                call: Call<UploadImageResponse>,
                response: Response<UploadImageResponse>
            ) {
                if (response.code() == 200) {
                    val res = response.body()
                    if (res != null) {
                        onSuccess(res)
                    } else {
                        onFailure(
                            Pair(
                                500,
                                "Something went wrong, we did not get response from our server. Please try again later"
                            )
                        )
                    }
                } else
                    onFailure(handleErrorResponse(response))
            }
        })
}


/*
* Default function to handle error response of all the network APIs
* */
fun handleErrorResponse(response: Any): Pair<Int, String> {
    try {
        val resp = response as Response<Any>
        showLog("onFailure " + resp.code())
        if (resp.code() == 400) {
            return (Pair(400, "We did not find what you are searching for"))
        } else if (resp.code() == 401) {
            return (Pair(401, "Your login has timed out. Please login again"))
        } else if (resp.code() == 404) {
            return (Pair(404, "The data does not exist for what you are searching"))
        } else if (resp.code() == 500 || resp.code() == 503 || resp.code() == 502) {
            return (
                    Pair(
                        resp.code(),
                        "Something went wrong with our server. Please try again later"
                    )
                    )
        } else {
            return try {
                val jObjError = JSONObject(resp.errorBody()?.string())
                Pair(500, jObjError.getString("errorMessage"))
            } catch (e: Exception) {
                Pair(500, e.localizedMessage)
            }
        }
    } catch (ex: java.lang.Exception) {
        return (
                Pair(
                    500,
                    "Something went wrong with our server. Please try again later"
                )
                )
    }
}
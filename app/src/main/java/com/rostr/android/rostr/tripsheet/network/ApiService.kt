package com.rostr.android.rostr.tripsheet.network

import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.utils.MOBILE_NO
import com.rostr.android.rostr.tripsheet.utils.getSystemTimeStamp
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*


interface ApiService {
    @POST("versions/checkVersion")
    fun checkVersion(
        @Body versionRequest: VersionRequest
    ): Call<VersionResponse>

    @POST("mobileStores/otp")
    fun loginUser(
        @Body signInRequest: SignInRequest
    ): Call<SignInRes>

    @POST("mobileStores/verify")
    fun verifyOtp(
        @Body verifyRequest: VerifyRequest
    ): Call<VerifyResponse>

    @POST("users")
    fun createProfile(
        @Header("Authorization") accessToken: String,
        @Body request: CreateProfileRequest
    ): Call<CreateProfileResponse>

    @POST("users/alternate/sign-up/{id}")
    fun createAlternateProfile(
        @Header("Authorization") accessToken: String,
        @Path("id") userId: String,
        @Body request: CreateProfileRequest
    ): Call<User>

    @GET
    fun getMyProfile(
        @Header("Authorization") accessToken: String,
        @Url url: String
    ): Call<User>

    @POST("trips")
    fun createTrip(
        @Header("Authorization") accessToken: String,
        @Body request: TripCreateRequest
    ): Call<TripResponse>

    @PUT("trips/{id}")
    fun updateTrip(
        @Header("Authorization") accessToken: String,
        @Path("id") id: String,
        @Body request: TripCreateRequest
    ): Call<TripResponse>

    @POST("transporters/create-driver")
    fun addDriver(
        @Header("Authorization") accessToken: String,
        @Body request: AddDriverRequest
    ): Call<UserDriver>

    @PUT("transporters/edit-upi")
    fun updateDriverUPI(
        @Header("Authorization") accessToken: String,
        @Body request: UpdateDriverUpiRequest
    ): Call<AnonymousResponse>

    @PUT("transporters/add-driver")
    fun addExistingDriverToOperator(
        @Header("Authorization") accessToken: String,
        @Body request: LinkExistingDriverRequest
    ): Call<UserDriver>

    @POST("customers")
    fun createCustomer(
        @Header("Authorization") accessToken: String,
        @Body request: CustomerCreateRequest
    ): Call<Customer>

    @POST("customers/{id}")
    fun updateCustomer(
        @Header("Authorization") accessToken: String,
        @Path("id") customerId: String,
        @Body request: CustomerCreateRequest
    ): Call<Customer>

    @GET("transporters")
    fun getTransportersDrivers(
        @Header("Authorization") accessToken: String
    ): Call<Transporter>

    @GET("drivers")
    fun getDriversProfile(
        @Header("Authorization") accessToken: String
    ): Call<DriverProfile>

    @GET("drivers/fetch")
    fun searchDriverWithNumber(
        @Query(MOBILE_NO) mobile: String,
        @Header("Authorization") accessToken: String
    ): Call<UserDriver>

    @GET
    fun getAddressFromPinCode(
        @Url url: String
    ): Call<GoogleAddress>

    @POST("transactions")
    fun createTransaction(
        @Header("Authorization") accessToken: String,
        @Body request: TransactionRequest
    ): Call<Data>

    @PUT("transactions/{id}")
    fun updateTransaction(
        @Header("Authorization") accessToken: String,
        @Path("id") transactionId: String,
        @Body request: TransactionRequest
    ): Call<Data>

    @PUT
    fun deletePutEntries(
        @Header("Authorization") accessToken: String,
        @Url url: String,
        @Body request: DeleteRequest
    ): Call<AnonymousResponse>

    @DELETE
    fun deleteEntries(
        @Header("Authorization") accessToken: String,
        @Url url: String
    ): Call<AnonymousResponse>

    @GET("trips/transporter")
    fun getMyTrips(@Header("Authorization") accessToken: String)
            : Call<ArrayList<TripResponse>>

    @GET("trips/driver")
    fun getTransportersTrips(@Header("Authorization") accessToken: String)
            : Call<ArrayList<TripResponse>>

    @GET("trips/myAssignedTrips/")
    fun getAssignedTrips(@Header("Authorization") accessToken: String)
            : Call<ArrayList<TripResponse>>

    @GET("transactions/transporter")
    fun getTransactionsForDriver(
        @Query("driver") driverId: String,
        @Header("Authorization") accessToken: String
    ): Call<TransactionResponse>

    @GET("transactions/trip/{id}")
    fun getTransactionsForTrips(
        @Path("id") tripId: String,
        @Header("Authorization") accessToken: String
    ): Call<ArrayList<Data>>

    @GET("transactions/driver")
    fun getTransactionsForTransporter(
        @Query("transporter") transporterId: String,
        @Header("Authorization") accessToken: String
    ): Call<TransactionResponse>


    @PUT("trips/")
    fun updateTrips(
        @Path("id") tripId: String,
        @Header("Authorization") accessToken: String,
        @Body request: CustomerCreateRequest
    ): Call<TripResponse>

    @GET("trips/transporter")
    fun getCustomerTrips(
        @Query("customer") customerId: String,
        @Header("Authorization") accessToken: String
    ): Call<ArrayList<TripResponse>>

    @GET("trips/transporter")
    fun getDriverTrips(
        @Query("driver") driverId: String,
        @Header("Authorization") accessToken: String
    ): Call<ArrayList<TripResponse>>

    @PUT("transporters")
    fun updateTransporterProfile(
        @Header("Authorization") accessToken: String,
        @Body request: UpdateTransporterRequest
    ): Call<UserTransPorter>

    @POST("users/switch-profile")
    fun switchProfile(
        @Header("Authorization") accessToken: String,
        @Body request: SwitchProfileRequest
    ): Call<SwitchProfileRequest>

    @GET("trips/get/invoice/{id}")
    fun getInvoice(
        @Header("Authorization") accessToken: String,
        @Path("id") tripId: String
    ): Call<AnonymousResponse>

    @PUT("trips/update/trip-notes/{id}")
    fun addTripNote(
        @Header("Authorization") accessToken: String,
        @Path("id") tripId: String,
        @Body request: TripNoteRequest
    ): Call<TripResponse>

    @Multipart
    @POST("images/uploadImage")
    fun uploadImage(
        @Header("Authorization") accessToken: String,
        @Part file: MultipartBody.Part
    ): Call<UploadImageResponse>

    @POST("transporters/trip-report")
    fun getTripReports(
        @Header("Authorization") accessToken: String,
        @Query("customer") customerId: String?,
        @Query("driver") driverId: String?,
        @Query("startDate") startDate: Long?,
        @Query("endDate") endDate: Long?,
        @Query("tripType") tripType: String?
    ): Call<AnonymousResponse>

    @POST("transporters/transaction-report")
    fun getTransactionReports(
        @Header("Authorization") accessToken: String,
        @Query("driver") driverId: String?,
        @Query("startDate") startDate: Long?,
        @Query("endDate") endDate: Long?
    ): Call<AnonymousResponse>

    @GET("reports")
    fun getAllReports(
        @Header("Authorization") accessToken: String
    ): Call<ArrayList<ReportResponse>>


    companion object {
        @Volatile
        private var retrofit: Retrofit? = null

        private fun getInstance(): Retrofit {
            val logInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG)
                logInterceptor.level = HttpLoggingInterceptor.Level.BODY
            else
                logInterceptor.level = HttpLoggingInterceptor.Level.NONE
            val client = OkHttpClient.Builder()
                .addInterceptor {
                    val original = it.request()
                    val request = original.newBuilder()
                        .header("date", getSystemTimeStamp())
                        .method(original.method(), original.body())
                        .build()
                    it.proceed(request)
                }
                .addInterceptor(logInterceptor)
                .build()

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.SERVER_URL)
                .client(client)
                .build()
        }

        @Synchronized
        fun create(): ApiService? {
            retrofit ?: synchronized(this) {
                retrofit = getInstance()
            }

            return retrofit?.create(ApiService::class.java)
        }
    }
}
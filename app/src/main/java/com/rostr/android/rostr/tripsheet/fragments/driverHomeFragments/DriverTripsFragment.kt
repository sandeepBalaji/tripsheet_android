package com.rostr.android.rostr.tripsheet.fragments.driverHomeFragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.appcompat.widget.ListPopupWindow
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.SplashActivity
import com.rostr.android.rostr.tripsheet.TripDetailActivity
import com.rostr.android.rostr.tripsheet.adapter.AutoCompleteTripAdapter
import com.rostr.android.rostr.tripsheet.adapter.OptionsMenuAdapter
import com.rostr.android.rostr.tripsheet.adapter.TripAdapter
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getTransportersTripsOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.calendar_dialog.view.*
import kotlinx.android.synthetic.main.fragment_trips.*
import kotlinx.android.synthetic.main.recycler_layout.*
import ru.cleverpumpkin.calendar.CalendarDate
import ru.cleverpumpkin.calendar.CalendarView
import java.util.*
import kotlin.collections.ArrayList

class DriverTripsFragment : Fragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var tripList: ArrayList<TripResponse>
    private lateinit var filteredList: ArrayList<TripResponse>
    private lateinit var tripAdapter: TripAdapter
    private lateinit var dateCalendar: Calendar

    private var selectedDates: List<CalendarDate> = ArrayList()
    private var filterPosition: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trips, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dateCalendar = Calendar.getInstance()
        dateCalendar.timeInMillis = getOnlyDateInMillis(dateCalendar.timeInMillis)
        tv_trips_date.text = getFormattedDate(dateCalendar.timeInMillis)
        actv_trips_searchBox.hint = "Trip, Customer or driver name"
        initRecycler()
        tv_trips_date.setOnClickListener(this)
        iv_trips_filterIcon.setOnClickListener(this)
        iv_trips_searchIcon.setOnClickListener(this)
        actv_trips_searchBox.setAdapter(
            AutoCompleteTripAdapter(
                mContext,
                R.layout.item_option,
                tripList
            )
        )
        actv_trips_searchBox.setOnItemClickListener { adapterView, _, position, _ ->
            filteredList.clear()
            filteredList.add(adapterView.getItemAtPosition(position) as TripResponse)
            tripAdapter.notifyDataSetChanged()
        }
    }

    private fun initRecycler() {
        tripList = ArrayList()
        filteredList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
            tripAdapter = TripAdapter(mContext, filteredList, false) { _, position ->
                val intent =
                    Intent(mContext, TripDetailActivity::class.java)
                intent.putExtra(DATA_KEY, filteredList[position])
                startActivity(intent)
            }
            adapter = tripAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_trips_searchIcon -> {
                if (actv_trips_searchBox.visibility == View.VISIBLE) {
                    actv_trips_searchBox.visibility = GONE
                    if (!isEmpty(actv_trips_searchBox)) {
                        actv_trips_searchBox.setText("")
                        showData()
                    }
                } else
                    actv_trips_searchBox.visibility = View.VISIBLE
            }

            R.id.tv_trips_date -> {
                showCalendarDialog()
            }

            R.id.iv_trips_filterIcon -> {
                if (tripList.isNullOrEmpty())
                    showLongSnack(cl_trips_parent, "Cannot apply filter on an empty list")
                else
                    showFilterOptions()
            }
        }
    }

    private fun showFilterOptions() {
        val options = resources.getStringArray(R.array.trip_filter_options)
        val popAdapter = OptionsMenuAdapter(
            mContext,
            options
        )
        val mPopupWindow = ListPopupWindow(mContext)
        mPopupWindow.setAdapter(popAdapter)
        mPopupWindow.anchorView = iv_trips_filterIcon
        mPopupWindow.width = 300
        mPopupWindow.horizontalOffset = -250 //<--this provides the margin you need
        mPopupWindow.setOnItemClickListener { _, _, position, _ ->
            //            tv_search_sortBy.text = options[position]
            tv_recyclerMessage.visibility = GONE
            bt_recycler_actionButton.visibility = GONE
            showFilteredList(position)
            mPopupWindow.dismiss()
        }
        mPopupWindow.show()
    }

    private fun showFilteredList(filter: Int) {
        filteredList.clear()
        when (filter) {
            0 -> { // all
                filteredList.addAll(tripList)
            }

            1 -> { // local
                filteredList.addAll(ArrayList(tripList.filter { it.tripType == getString(R.string.local) }))
            }

            2 -> { // airport
                filteredList.addAll(ArrayList(tripList.filter { it.tripType == getString(R.string.airport) }))
            }

            3 -> { // outstation
                filteredList.addAll(ArrayList(tripList.filter { it.tripType == getString(R.string.outstation) }))
            }

            4 -> { // delivery
                filteredList.addAll(ArrayList(tripList.filter { it.tripType == getString(R.string.delivery) }))
            }
        }
        tripAdapter.notifyDataSetChanged()
        filterPosition = filter
        if (filteredList.isEmpty())
            showError(getString(R.string.empty_trips))
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(mContext)) {
            pb_progress.visibility = View.VISIBLE
            tv_recyclerMessage.visibility = GONE
            getTransportersTripsOperation(
                ApiService.create(),
                PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                {
                    if (isAdded) {
                        pb_progress.visibility = GONE
                        tripList.clear()
                        tripList.addAll(it)
                        showData()
                    }
                }, {
                    if (isAdded) {
                        pb_progress.visibility = GONE
                        if (it.first == 401) {
                            showAlertDialog(
                                mContext, getString(R.string.unauthorized_title),
                                getString(R.string.logged_out)
                            ) {
                                PreferenceUtils(mContext).clearData()
                                val intent = Intent(mContext, SplashActivity::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(intent)
                            }
                        } else
                            showError(it.second)
                    }
                })
        } else
            showError(getString(R.string.no_internet))
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = View.VISIBLE
    }

    private fun showCalendarDialog() {
        val dialog = BottomSheetDialog(mContext)
        val dialogView = layoutInflater.inflate(R.layout.calendar_dialog, null)

        val calendar = Calendar.getInstance()
// Initial date
        val initialDate = CalendarDate(calendar.time)
// Minimum available date
        val minDate = CalendarDate(calendar.time)
// Maximum available date
        val maxDate = CalendarDate(calendar.time)
        // The first day of week
        val firstDayOfWeek = Calendar.MONDAY

        // Set up calendar with all available parameters
        dialogView.cv_calendar.setupCalendar(
            initialDate = initialDate,
            minDate = null,
            maxDate = maxDate,
            selectionMode = CalendarView.SelectionMode.SINGLE,
            selectedDates = selectedDates,
            firstDayOfWeek = firstDayOfWeek,
            showYearSelectionView = true
        )
        dialogView.cv_calendar.onDateClickListener = {
            showLog("date: ${it.calendar.timeInMillis}")
            dateCalendar.timeInMillis = it.calendar.timeInMillis
            tv_trips_date.text = getFormattedDate(dateCalendar.timeInMillis)
            tv_recyclerMessage.visibility = GONE
            bt_recycler_actionButton.visibility = GONE
            dialog.dismiss()
            showData()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun showData() {
        filteredList.clear()
        filteredList.addAll(applyFilter())
        tripAdapter.notifyDataSetChanged()
        if (filteredList.isEmpty())
            showError(getString(R.string.empty_trips))
    }

    private fun applyFilter(): ArrayList<TripResponse> {
        showLog("size ${tripList.size}")
        for (trip: TripResponse in tripList)
            showLog("${getFormattedDate(dateCalendar.timeInMillis)} : ${getFormattedDate(trip.dateOfRide)}")
        val list = tripList.filter {
            it.dateOfRide == dateCalendar.timeInMillis
        }
        return ArrayList(list)
    }
}
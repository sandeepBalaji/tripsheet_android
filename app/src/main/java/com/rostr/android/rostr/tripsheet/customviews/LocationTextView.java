package com.rostr.android.rostr.tripsheet.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.rostr.android.rostr.tripsheet.R;

import static com.rostr.android.rostr.tripsheet.utils.MessageUtilsKt.showLog;
import static com.rostr.android.rostr.tripsheet.utils.WidgetUtilsKt.isEmpty;

public class LocationTextView extends CardView {
    TextView sourceTextView, destinationTextView, sourceLabel, destinationLabel;
    ImageView sourceStartIcon, destinationStartIcon, sourceEndIcon, destinationEndIcon;
    ConstraintLayout parentView, source, destination;
    View sourceLine, verticalLine;

    public LocationTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.location_textview, this);
        initComponents();

        CharSequence srcHint = null, destHint = null, srcLabel = null, destLabel = null;
        int startSourceRes = 0, endSourceRes = 0, startDestRes = 0, endDestRes = 0;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LocationTextView);
        try {
            if (typedArray.getText(R.styleable.LocationTextView_sourceLabel) != null)
                srcLabel = typedArray.getText(R.styleable.LocationTextView_sourceLabel);
            if (typedArray.getText(R.styleable.LocationTextView_destinationLabel) != null)
                destLabel = typedArray.getText(R.styleable.LocationTextView_destinationLabel);
            if (typedArray.getText(R.styleable.LocationTextView_sourceHint) != null)
                srcHint = typedArray.getText(R.styleable.LocationTextView_sourceHint);
            if (typedArray.getText(R.styleable.LocationTextView_destinationHint) != null)
                destHint = typedArray.getText(R.styleable.LocationTextView_destinationHint);


            if (isResource(context, typedArray.getResourceId(R.styleable.LocationTextView_sourceStartDrawable, 0)))
                startSourceRes = typedArray.getResourceId(R.styleable.LocationTextView_sourceStartDrawable, 0);
            if (isResource(context, typedArray.getResourceId(R.styleable.LocationTextView_destinationStartDrawable, 0)))
                startDestRes = typedArray.getResourceId(R.styleable.LocationTextView_destinationStartDrawable, 0);
            if (isResource(context, typedArray.getResourceId(R.styleable.LocationTextView_sourceEndDrawable, 0)))
                endSourceRes = typedArray.getResourceId(R.styleable.LocationTextView_sourceEndDrawable, 0);
            if (isResource(context, typedArray.getResourceId(R.styleable.LocationTextView_destinationEndDrawable, 0)))
                endDestRes = typedArray.getResourceId(R.styleable.LocationTextView_destinationEndDrawable, 0);

            setParams(srcLabel, destLabel, srcHint, destHint, startSourceRes, startDestRes, endSourceRes, endDestRes);
        } catch (Exception ex) {
            showLog(ex);
        } finally {
            typedArray.recycle();
        }
    }

    public void setParams(CharSequence srcLabel, CharSequence destLabel, CharSequence srcHint,
                          CharSequence destHint, int startSourceRes, int startDestRes,
                          int endSourceRes, int endDestRes) {
        if (srcLabel != null)
            sourceLabel.setText(srcLabel);
        else
            sourceLabel.setVisibility(GONE);

        if (destLabel != null)
            destinationLabel.setText(destLabel);
        else
            destinationLabel.setVisibility(GONE);

        if (srcHint != null)
            sourceTextView.setHint(srcHint);

        if (destHint != null)
            destinationTextView.setHint(destHint);

        if (startSourceRes > 0)
            sourceStartIcon.setImageResource(startSourceRes);
        else
            sourceStartIcon.setVisibility(GONE);

        if (endSourceRes > 0)
            sourceEndIcon.setImageResource(endSourceRes);
        else
            sourceEndIcon.setVisibility(GONE);

        if (startDestRes > 0)
            destinationStartIcon.setImageResource(startDestRes);
        else
            destinationStartIcon.setVisibility(GONE);

        if (endDestRes > 0)
            destinationEndIcon.setImageResource(endDestRes);
        else
            destinationEndIcon.setVisibility(GONE);
    }

    private void initComponents() {
        source = findViewById(R.id.cl_location_sourceParent);
        destination = findViewById(R.id.cl_location_destinationParent);

        sourceTextView = findViewById(R.id.tv_location_sourceText);
        parentView = findViewById(R.id.cl_location_parent);
        destinationTextView = findViewById(R.id.tv_location_destinationText);
        sourceLabel = findViewById(R.id.tv_location_sourceLabel);
        destinationLabel = findViewById(R.id.tv_location_destinationLabel);
        sourceStartIcon = findViewById(R.id.iv_location_sourceStartIcon);
        destinationStartIcon = findViewById(R.id.iv_location_destinationStartIcon);
        sourceEndIcon = findViewById(R.id.iv_location_sourceEndIcon);
        destinationEndIcon = findViewById(R.id.iv_location_destinationEndIcon);
        sourceLine = findViewById(R.id.startLine);
        verticalLine = findViewById(R.id.verticalLine);
    }

    public static boolean isResource(Context context, int resId) {
        if (context != null) {
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }

    public boolean isSourceEmpty() {
        return isEmpty(sourceTextView);
    }

    public boolean isDestEmpty() {
        return isEmpty(destinationTextView);
    }

    public void setSourceEndDrawableClickListener(OnClickListener clickListener) {
        sourceEndIcon.setOnClickListener(clickListener);
    }

    public void setDestEndDrawableClickListener(OnClickListener clickListener) {
        destinationEndIcon.setOnClickListener(clickListener);
    }

    public void setSourceClickListener(OnClickListener clickListener) {
        source.setOnClickListener(clickListener);
    }

    public void setDestClickListener(OnClickListener clickListener) {
        destination.setOnClickListener(clickListener);
    }

    public String getSourceText() {
        return sourceTextView.getText().toString();
    }

    public String getDestText() {
        return destinationTextView.getText().toString();
    }

    public void setSourceText(String text) {
        sourceTextView.setText(text);
    }

    public void setSourceText(SpannableStringBuilder text) {
        sourceTextView.setText(text);
    }

    public void setDestText(String text) {
        destinationTextView.setText(text);
    }

    public void setDestText(SpannableStringBuilder text) {
        destinationTextView.setText(text);
    }

    public void setSrcLabel(String text) {
        sourceLabel.setText(text);
    }

    public void setDestLabel(String text) {
        destinationLabel.setText(text);
    }

    public void setSourceEndDrawableTag(int tag) {
        sourceEndIcon.setTag(tag);
    }

    public void setDestEndDrawableTag(int tag) {
        destinationEndIcon.setTag(tag);
    }

    public Object getSrcEndDrawableTag() {
        return sourceEndIcon.getTag();
    }

    public Object getDestEndDrawableTag() {
        return destinationEndIcon.getTag();
    }

    public void setSourceEndDrawable(int resDrawable) {
        sourceEndIcon.setImageResource(resDrawable);
    }

    public void setDestEndDrawable(int resDrawable) {
        if (resDrawable == 0)
            destinationEndIcon.setVisibility(GONE);
        else {
            destinationEndIcon.setVisibility(VISIBLE);
            destinationEndIcon.setImageResource(resDrawable);
        }
    }

    public void setSourceStartDrawable(int resDrawable) {
        if (resDrawable == 0)
            sourceStartIcon.setVisibility(GONE);
        else
            sourceStartIcon.setVisibility(VISIBLE);
        sourceStartIcon.setImageResource(resDrawable);
    }

    public void setDestStartDrawable(int resDrawable) {
        if (resDrawable == 0)
            destinationStartIcon.setVisibility(GONE);
        else
            destinationStartIcon.setVisibility(VISIBLE);
        destinationStartIcon.setImageResource(resDrawable);
    }

    public void setTextColor(int colorRes) {
        sourceTextView.setTextColor(ContextCompat.getColor(getContext(), colorRes));
        destinationTextView.setTextColor(ContextCompat.getColor(getContext(), colorRes));
    }

    public void setParentBackgroundColor(int colorRes) {
        parentView.setBackgroundColor(ContextCompat.getColor(getContext(), colorRes));
    }

    public void setVisibilitySourceLine(int visibility) {
        sourceLine.setVisibility(visibility);
    }

    public void setDestinationVisibility(int visibility) {
        if (visibility == GONE) {
            destination.setVisibility(GONE);
            destinationStartIcon.setVisibility(GONE);
            verticalLine.setVisibility(GONE);
        } else {
            destination.setVisibility(VISIBLE);
            destinationStartIcon.setVisibility(VISIBLE);
            verticalLine.setVisibility(VISIBLE);
        }
    }
}
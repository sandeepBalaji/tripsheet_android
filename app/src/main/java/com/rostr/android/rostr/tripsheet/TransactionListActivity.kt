package com.rostr.android.rostr.tripsheet

import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rostr.android.rostr.tripsheet.adapter.TransactionAdapter
import com.rostr.android.rostr.tripsheet.model.Data
import com.rostr.android.rostr.tripsheet.model.DriversTransporter
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getTransactionForTransporterOperation
import com.rostr.android.rostr.tripsheet.utils.ACCESS_TOKEN
import com.rostr.android.rostr.tripsheet.utils.DATA_KEY
import com.rostr.android.rostr.tripsheet.utils.PreferenceUtils
import com.rostr.android.rostr.tripsheet.utils.isConnectedToInternet
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.*

class TransactionListActivity : AppCompatActivity() {

    private lateinit var transporterData: DriversTransporter
    private lateinit var transactionList: ArrayList<Data>
    private lateinit var transactionAdapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.recycler_layout_toolbar)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            transporterData = intent.getParcelableExtra(DATA_KEY)
            tv_toolbar_title.text = "${transporterData.name.split(" ")[0]}'s Transactions"
            initRecycler()
        }
    }

    private fun initRecycler() {
        transactionList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@TransactionListActivity)
            transactionAdapter =
                TransactionAdapter(this@TransactionListActivity, transactionList, null)
            adapter = transactionAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(this))
            getTransactions()
        else
            showError(getString(R.string.no_internet))
    }


    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
    }

    private fun getTransactions() {
        pb_progress.visibility = VISIBLE
        tv_recyclerMessage.visibility = GONE
        getTransactionForTransporterOperation(
            ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            transporterData._id, {
                pb_progress.visibility = GONE
                transactionList.clear()
                transactionList.addAll(it.data)
                transactionAdapter.notifyDataSetChanged()
                if (transactionList.isEmpty())
                    showError(getString(R.string.empty_driver_transactions))
            }, {
                pb_progress.visibility = GONE
                showError(it.second)
            })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
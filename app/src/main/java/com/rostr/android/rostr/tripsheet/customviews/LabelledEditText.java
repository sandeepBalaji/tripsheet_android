package com.rostr.android.rostr.tripsheet.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rostr.android.rostr.tripsheet.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.rostr.android.rostr.tripsheet.utils.MessageUtilsKt.showLog;

public class LabelledEditText extends LinearLayout {
    LinearLayout parent;
    EditText editText;
    TextView labelTextView;

    public LabelledEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.labelled_edittext, this);
        initComponents();

        CharSequence hint = null, label = null, inputType = null;
        boolean isRequired, isEditable;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LabelledEditText);
        try {
            isEditable = typedArray.getBoolean(R.styleable.LabelledEditText_isEditable, true);
            isRequired = typedArray.getBoolean(R.styleable.LabelledEditText_isRequired, false);
            if (typedArray.getText(R.styleable.LabelledEditText_label) != null)
                label = typedArray.getText(R.styleable.LabelledEditText_label);
            if (typedArray.getText(R.styleable.LabelledEditText_hint) != null)
                hint = typedArray.getText(R.styleable.LabelledEditText_hint);
            if (typedArray.getText(R.styleable.LabelledEditText_inputType) != null)
                inputType = typedArray.getText(R.styleable.LabelledEditText_inputType);

            setParams(label, hint, isRequired, isEditable, inputType);
        } catch (Exception ex) {
            showLog(ex);
        } finally {
            typedArray.recycle();
        }
    }

    public EditText getEditText() {
        return editText;
    }

    public static boolean isResource(Context context, int resId) {
        if (context != null) {
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }

    private void initComponents() {
        parent = findViewById(R.id.cl_labelled_edittext_parent);
        labelTextView = findViewById(R.id.tv_labelled_edittext_label);
        editText = findViewById(R.id.et_labelled_edittext_value);
    }

    public void setParams(CharSequence label, CharSequence hint, boolean isRequired,
                          boolean isEditable, CharSequence inputType) {
        if (label != null)
            labelTextView.setText(label);
        else
            labelTextView.setVisibility(GONE);

        if (hint != null)
            editText.setHint(hint);

        if (isRequired) {
            labelTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_asterisk, 0);
        }
        editText.setEnabled(isEditable);

        if (inputType != null) {
            switch (inputType.toString()) {
                case "name":
                    editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                    break;

                case "email":
                    editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    break;

                case "phone":
                    editText.setInputType(InputType.TYPE_CLASS_PHONE);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                    break;

                case "pincode":
                    editText.setInputType(InputType.TYPE_CLASS_PHONE);
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
                    break;

                case "number":
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                    break;

                case "allCaps":
                    editText.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
                    break;

                case "gst":
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
                    break;

                case "ifsc":
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
                    break;

                case "acc":
                    editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
                    break;
            }
        }
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(editText.getText().toString().trim());
    }

    public String getValue() {
        return editText.getText().toString();
    }

    public Double getDouble() {
        if (TextUtils.isEmpty(editText.getText()))
            return 0.0;
        else
            return Double.parseDouble(editText.getText().toString());
    }

    public Integer getInt() {
        if (TextUtils.isEmpty(editText.getText()))
            return 0;
        else
            return Integer.parseInt(editText.getText().toString());
    }

    public boolean isEmailValid() {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(editText.getText().toString());
        return matcher.matches();
    }

    public void setEditable(boolean editable) {
        editText.setEnabled(editable);
        if (editable)
            editText.setFocusable(true);
    }

    public void setValue(String value) {
        editText.setText(value);
    }

    public void setOnViewClickListener(OnClickListener clickListener) {
        parent.setOnClickListener(clickListener);
    }

    public boolean isPhoneValid() {
        return !isEmpty() && getValue().length() == 10;
    }

    public void setLabelText(String text) {
        labelTextView.setVisibility(VISIBLE);
        labelTextView.setText(text);
    }
}
package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.Data
import com.rostr.android.rostr.tripsheet.utils.getFormattedDateTime
import com.rostr.android.rostr.tripsheet.utils.showLog
import kotlinx.android.synthetic.main.item_transaction.view.*
import java.util.*

class TransactionAdapter(
    private val mContext: Context,
    private val mainList: ArrayList<Data>,
    val onItemClick: ((view: View, position: Int) -> Unit)?
) :
    RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.tv_transaction_item_name
        val amount: TextView = itemView.tv_transaction_item_amount
        val remarks: TextView = itemView.tv_transaction_item_remarks
        val dateTime: TextView = itemView.tv_transaction_item_date
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.item_transaction,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        try {
            val currentItem = mainList[position]
            holder.title.text = currentItem.purpose
            if (currentItem.total == null)
                holder.amount.visibility = GONE
            else {
                holder.amount.visibility = VISIBLE
                holder.amount.text = mContext.getString(
                    R.string.rs_string,
                    currentItem.total.replace(Regex("-"), "")
                )
                if (currentItem.total.toDouble() >= 0.0)
                    holder.amount.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.colorDarkGreen
                        )
                    )
                else
                    holder.amount.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.colorAccent
                        )
                    )
            }
            val cal = Calendar.getInstance()
            cal.timeInMillis = currentItem.date
            holder.dateTime.text = getFormattedDateTime(cal)
            holder.remarks.text = currentItem.remarks
            if (onItemClick != null)
                holder.itemView.setOnClickListener {
                    onItemClick.invoke(it, position)
                }
        } catch (ex: Exception) {
            showLog(ex.localizedMessage)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
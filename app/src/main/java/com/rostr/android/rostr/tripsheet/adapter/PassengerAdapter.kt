package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.PassengerModel
import kotlinx.android.synthetic.main.item_passenger.view.*

class PassengerAdapter(
    private val context: Context,
    private val mainList: ArrayList<PassengerModel>?,
    private val onItemClick: ((view: View, position: Int) -> Unit)?
) :
    RecyclerView.Adapter<PassengerAdapter.CustomerViewHolder>() {

    class CustomerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_passenger_item_name
        val phone: TextView = itemView.tv_passenger_item_phone
        val removeIcon: ImageView = itemView.iv_passenger_item_removeIcon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        return CustomerViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_passenger,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        val currentItem = mainList!![position]
        holder.name.text = currentItem.name
        holder.phone.text = currentItem.mobileNo
        if (onItemClick != null) {
            holder.removeIcon.visibility = GONE
            holder.removeIcon.setOnClickListener {
                onItemClick.invoke(it, position)
            }
        } else
            holder.removeIcon.visibility = GONE
    }

    override fun getItemCount(): Int {
        if (mainList != null)
            return mainList.size
        else
            return 0
//        return 5
    }
}
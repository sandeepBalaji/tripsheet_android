package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.ApiException
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.rostr.android.rostr.tripsheet.model.TransporterSupport
import com.rostr.android.rostr.tripsheet.model.UpdateTransporterRequest
import com.rostr.android.rostr.tripsheet.model.UserSingleton
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.updateTransporterProfileOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.Arrays.asList

class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    private var googlePlaceId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            tv_toolbar_title.text = getString(R.string.settings)

            tv_settings_googleLink.paintFlags =
                tv_settings_googleLink.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            tv_settings_deleteLink.paintFlags =
                tv_settings_deleteLink.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            bt_settings_generateLink.setOnClickListener(this)
            tv_settings_deleteLink.setOnClickListener(this)
            bt_settings_saveButton.setOnClickListener(this)
            tv_settings_searchGoogle.setOnClickListener(this)
            iv_settings_shareIcon.setOnClickListener(this)
            setData()
        }
    }

    private fun setData() {
        val user = UserSingleton.instance?.transporter
        et_settings_supportNumber.value = user?.support?.contactNumber
        et_settings_whatsappNumber.value = user?.support?.whatsApp
        et_settings_supportEmail.value = user?.support?.email
        if (user?.placeId != null) {
            googlePlaceId = user.placeId
            tv_settings_googleLink.text =
                getString(R.string.google_review_url, googlePlaceId)
            cl_settings_googleLinkLayout.visibility = VISIBLE
            if (!Places.isInitialized()) {
                Places.initialize(this, getString(R.string.maps_api_key))
            }
            val placeFields = asList(Place.Field.NAME)
            // Construct a request object, passing the place ID and fields array.
            val request = FetchPlaceRequest.newInstance(user.placeId, placeFields)
            val client = Places.createClient(this)
            client.fetchPlace(request).addOnSuccessListener { response ->
                val place = response.place
                showLog("Place found: ${place.name!!}")
                tv_settings_searchGoogle.text = place.name
            }.addOnFailureListener { exception ->
                if (exception is ApiException) {
                    // Handle error with given status code.
                    showLog("Place not found: ${exception.localizedMessage}")
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_settings_saveButton -> {
                updateTransporterData(
                    UpdateTransporterRequest(
                        support =
                        TransporterSupport(
                            email = et_settings_supportEmail.value,
                            contactNumber = et_settings_supportNumber.value,
                            whatsApp = et_settings_whatsappNumber.value
                        )
                    ),
                    false
                )
            }

            R.id.bt_settings_generateLink -> {
                if (googlePlaceId.isNullOrEmpty()) {
                    showShortToast(this, "Please search for your business from google first")
                } else {
                    updateTransporterData(
                        UpdateTransporterRequest(placeId = googlePlaceId),
                        true
                    )
                }
            }

            R.id.tv_settings_deleteLink -> {
                updateTransporterData(
                    UpdateTransporterRequest(placeId = ""),
                    false
                )
            }

            R.id.tv_settings_searchGoogle -> {
                startLocationSearch()
            }

            R.id.iv_settings_shareIcon -> {
                val sharingIntent = Intent(Intent.ACTION_SEND)
                sharingIntent.type = "text/plain"
                sharingIntent.putExtra(Intent.EXTRA_TEXT, tv_settings_googleLink.text)
                startActivity(
                    Intent.createChooser(
                        sharingIntent,
                        resources.getString(R.string.share_using)
                    )
                )
            }
        }
    }

    private fun startLocationSearch() {
        /*
       * Initialize Places. For simplicity, the API key is hard-coded. In a production
       * environment we recommend using a secure mechanism to manage API keys.
       */
        if (!Places.isInitialized()) {
            Places.initialize(this, getString(R.string.maps_api_key))
        }
        // Set the fields to specify which types of place data to return.
        val fields = asList(Place.Field.ID, Place.Field.NAME)
//        val rectangularBounds = RectangularBounds.newInstance(
//            LatLng(12.864162, 77.438610),
//            LatLng(13.139807, 77.711895)
//        )
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        )
            .setCountry("IN")
//            .setLocationRestriction(rectangularBounds)
            .build(this)
        startActivityForResult(intent, 100)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (data != null) {
                val place = Autocomplete.getPlaceFromIntent(data)
                showLog("Place received $place")
                tv_settings_searchGoogle.text = place.name
                try {
                    googlePlaceId = place.id
                } catch (ex: Exception) {
                    showLog(ex)
                }
            }
        } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
            if (data != null) {
                val status = Autocomplete.getStatusFromIntent(data)
                showLog("$status ${status.statusMessage}")
                showAlertDialog(this, getString(R.string.oops), status.statusMessage!!)
            }
        }
    }

    private fun updateTransporterData(request: UpdateTransporterRequest, showLinkLayout: Boolean) {
        if (isConnectedToInternet(this)) {
            updateTransporterProfileOperation(ApiService.create(),
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                request, {
                    UserSingleton.instance?.transporter = it
                    showAlertDialog(this, null, "Successfully updated your data")
                    if (showLinkLayout) {
                        tv_settings_googleLink.text =
                            getString(R.string.google_review_url, googlePlaceId)
                        cl_settings_googleLinkLayout.visibility = VISIBLE
                    } else
                        if (cl_settings_googleLinkLayout.visibility == VISIBLE)
                            cl_settings_googleLinkLayout.visibility = GONE
                }, {
                    if (it.first == 401) {
                        showAlertDialog(
                            this, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            PreferenceUtils(this).clearData()
                            val intent = Intent(this, SplashActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        }
                    } else
                        showAlertDialog(this, getString(R.string.oops), it.second)
                })
        } else
            showAlertDialog(
                this,
                getString(R.string.no_internet_label),
                getString(R.string.no_internet)
            )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Vehicle(
    val id: String
) : Parcelable
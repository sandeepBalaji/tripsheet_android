package com.rostr.android.rostr.tripsheet.fragments.roleFragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.rostr.android.rostr.tripsheet.CreateProfileActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.RoleParentActivity
import com.rostr.android.rostr.tripsheet.utils.ROLE_KEY
import com.rostr.android.rostr.tripsheet.utils.TYPE_KEY
import kotlinx.android.synthetic.main.fragment_business_type.*

class SelectBusinessTypeFragment : Fragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private var parentActivity: FragmentActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_business_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cv_business_type_option1.setOnClickListener(this)
        cv_business_type_option2.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val intent = Intent(mContext, CreateProfileActivity::class.java)
        intent.putExtra(ROLE_KEY, "transporter")
        when (v?.id) {
            R.id.cv_business_type_option1 -> {
                intent.putExtra(TYPE_KEY, false)
            }

            R.id.cv_business_type_option2 -> {
                intent.putExtra(TYPE_KEY, true)
            }
        }
        (parentActivity as RoleParentActivity).startActivityFromFragment(intent)
    }
}
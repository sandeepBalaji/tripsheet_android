package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.UserDriver
import com.rostr.android.rostr.tripsheet.utils.showLog
import kotlinx.android.synthetic.main.item_driver.view.*


class DriverAdapter(
    private val context: Context,
    private val mainList: ArrayList<UserDriver>,
    private val isSearch: Boolean,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<DriverAdapter.DriverViewHolder>() {

    class DriverViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.iv_driver_item_image
        val name: TextView = itemView.tv_driver_item_name
        val vehicleType: TextView = itemView.tv_driver_item_vehicleType
        val amount: TextView = itemView.tv_driver_item_amount
        val message: TextView = itemView.tv_msg
        val vehicleNo: TextView = itemView.tv_driver_item_vehicleNo
        val callIcon: ImageView = itemView.iv_driver_item_callIcon
        val whatsapp: ImageView = itemView.iv_driver_item_whatsappIcon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverViewHolder {
        return DriverViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_driver,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DriverViewHolder, position: Int) {
        val currentItem = mainList[position]
        holder.image.setImageResource(R.drawable.ic_driver)
        holder.name.text = currentItem.driver?.name
        holder.amount.text =
            context.getString(R.string.rs_string, currentItem.due?.replace(Regex("-"), ""))
        if (currentItem.due?.contains("-")!!) {
            holder.message.visibility = VISIBLE
            holder.amount.setTextColor(ContextCompat.getColor(context, R.color.theme_red))
        } else {
            holder.message.visibility = GONE
            holder.amount.setTextColor(ContextCompat.getColor(context, R.color.colorDarkGreen))
        }
        showLog("driver ${currentItem.driver?.name}")
        holder.vehicleNo.text = currentItem.driver?.vehicle?.regNumber
        holder.vehicleType.text = currentItem.driver?.vehicle?.vehicleType

        if (isSearch) {
            holder.whatsapp.visibility = GONE
            holder.callIcon.visibility = GONE
            holder.amount.visibility = GONE
        } else {
            holder.whatsapp.visibility = VISIBLE
            holder.callIcon.visibility = VISIBLE
            holder.amount.visibility = VISIBLE
        }

        holder.itemView.setOnClickListener {
            onItemClick(it, position)
        }
        holder.callIcon.setOnClickListener {
            onItemClick(it, position)
        }
        holder.whatsapp.setOnClickListener {
            onItemClick(it, position)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
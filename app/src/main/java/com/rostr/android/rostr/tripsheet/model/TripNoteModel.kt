package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class TripNoteRequest(
    val tripNote: NoteRequest
)

data class NoteRequest(
    val notes: String? = null,
    val images: String? = null,
    val date: Long,
    val by: String
)

@Parcelize
data class TripNoteResponse(
    val notes: String? = null,
    val date: Long,
    val by: NoteBy,
    val images: ArrayList<String>? = null
): Parcelable

@Parcelize
data class NoteBy(
    val _id: String,
    val mobileNo: String,
    val name: String
): Parcelable
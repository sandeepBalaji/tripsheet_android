package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.rostr.android.rostr.tripsheet.fragments.roleFragments.SelectBusinessTypeFragment
import com.rostr.android.rostr.tripsheet.fragments.roleFragments.SelectRoleFragment
import kotlinx.android.synthetic.main.toolbar.*

class RoleParentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_role)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            replaceFragment(SelectBusinessTypeFragment())
        }
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fl_select_role_container, fragment)
            .commit()
    }

    fun startActivityFromFragment(intent: Intent) {
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackAction()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        onBackAction()
    }

    private fun onBackAction(){
        val currentFrag = supportFragmentManager.findFragmentById(R.id.fl_select_role_container)
        if (currentFrag is SelectRoleFragment)
            finish()
        else
            replaceFragment(SelectRoleFragment())

    }
}
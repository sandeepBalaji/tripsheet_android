package com.rostr.android.rostr.tripsheet

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.adapter.AutoCompleteCustomerAdapter
import com.rostr.android.rostr.tripsheet.adapter.AutoCompleteDriverAdapter
import com.rostr.android.rostr.tripsheet.adapter.ReportsAdapter
import com.rostr.android.rostr.tripsheet.adapter.SpinnerAdapter
import com.rostr.android.rostr.tripsheet.dialogs.ProgressDialog
import com.rostr.android.rostr.tripsheet.model.*
import com.rostr.android.rostr.tripsheet.network.*
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.calendar_dialog.view.*
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import ru.cleverpumpkin.calendar.CalendarDate
import ru.cleverpumpkin.calendar.CalendarView
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class ReportActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var startCalendar: Calendar
    private lateinit var endCalendar: Calendar
    private var customerId: String? = null
    private var driverId: String? = null
    private lateinit var customerList: ArrayList<Customer>
    private lateinit var driverList: ArrayList<UserDriver>
    private lateinit var transporterData: Transporter

    private lateinit var reportAdapter: ReportsAdapter

    private var selectedDates: List<CalendarDate> = ArrayList()
    private lateinit var reportList: ArrayList<ReportResponse>

    companion object {
        const val START_DATE_REQUEST = 1
        const val END_DATE_REQUEST = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            tv_toolbar_title.text = getString(R.string.reports)
            initReportTypeChangeListener()
            initRecycler()
            startCalendar = Calendar.getInstance()
            startCalendar.timeInMillis = getOnlyDateInMillis(startCalendar.timeInMillis)
            tv_report_startDate.setText(getFormattedDate(startCalendar.timeInMillis))

            endCalendar = Calendar.getInstance()
            endCalendar.timeInMillis = getOnlyDateInMillis(endCalendar.timeInMillis)
            tv_report_endDate.setText(getFormattedDate(endCalendar.timeInMillis))

            bt_report_generateReport.setOnClickListener(this)
            tv_report_startDate.setOnClickListener(this)
            tv_report_endDate.setOnClickListener(this)
            getTransporterDetail()
            getAllReports()
        }
    }

    private fun getTransporterDetail() {
        if (isConnectedToInternet(this)) {
            getTransportersDriversOperation(ApiService.create(),
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!, {
                    transporterData = it
                    setupFilterSearch()
                }, {
                    showAlertDialog(this, getString(R.string.oops), it.second)
                })
        }
    }

    private fun setupFilterSearch() {
        driverList = transporterData.drivers
        customerList = transporterData.customers
        actv_report_customer.setAdapter(
            AutoCompleteCustomerAdapter(
                this@ReportActivity,
                R.layout.item_option,
                customerList
            )
        )
        actv_report_customer.setOnItemClickListener { adapterView, _, position, _ ->
            customerId = (adapterView.getItemAtPosition(position) as UserCustomer)._id
        }

        actv_report_driver.setAdapter(
            AutoCompleteDriverAdapter(
                this@ReportActivity,
                R.layout.item_option,
                driverList
            )
        )
        actv_report_driver.setOnItemClickListener { adapterView, _, position, _ ->
            showLog("driver id: ${driverList[position].driver?._id}")
            driverId = (adapterView.getItemAtPosition(position) as UserDriver)._id
        }

        sp_report_tripType.apply {
            adapter = SpinnerAdapter(
                this@ReportActivity,
                resources.getStringArray(R.array.trip_filter_options)
            )
        }
    }

    private fun initReportTypeChangeListener() {
        rg_report_reportType.setOnCheckedChangeListener { _, checkedId ->
            if (checkedId == R.id.rb_report_trip) {
                actv_report_customer.visibility = VISIBLE
                sp_report_tripType.visibility = VISIBLE
            } else {
                actv_report_customer.visibility = GONE
                sp_report_tripType.visibility = GONE
            }
        }
    }

    private fun initRecycler() {
        reportList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ReportActivity)
            reportAdapter = ReportsAdapter(this@ReportActivity, reportList) { _, position ->
                checkStoragePermission(reportList[position].url!!)
            }
            adapter = reportAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_report_generateReport -> {
                if (isConnectedToInternet(this)) {
                    val reportType = rg_report_reportType.checkedRadioButtonId
                    if (reportType == R.id.rb_report_trip)
                        generateTripReports()
                    else
                        generateTransactionReports()
                } else
                    showAlertDialog(
                        this,
                        getString(R.string.no_internet_label),
                        getString(R.string.no_internet)
                    )
            }

            R.id.tv_report_startDate -> {
                showCalendarDialog(START_DATE_REQUEST)
            }

            R.id.tv_report_endDate -> {
                showCalendarDialog(END_DATE_REQUEST)
            }
        }
    }

    private fun generateTripReports() {
        val progress =
            ProgressDialog.progressDialog(this, "Generating reports...please wait...")
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        generateTripReportsOperation(ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            customerId,
            driverId,
            if (sp_report_tripType.selectedItemPosition == 0) null else sp_report_tripType.selectedItem.toString(),
            startCalendar.timeInMillis,
            endCalendar.timeInMillis, {
                progress.dismiss()
                getAllReports()
            }, {
                progress.dismiss()
                showAPIError(it)
            })
    }

    private fun generateTransactionReports() {
        val progress =
            ProgressDialog.progressDialog(this, "Generating reports...please wait...")
        progress.setCanceledOnTouchOutside(false)
        progress.show()
        generateTransactionReportsOperation(ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            driverId,
            startCalendar.timeInMillis,
            endCalendar.timeInMillis, {
                progress.dismiss()
                getAllReports()
            }, {
                progress.dismiss()
                showAPIError(it)
            })
    }

    private fun getAllReports() {
        if (isConnectedToInternet(this)) {
            tv_recyclerMessage.visibility = GONE
            pb_progress.visibility = VISIBLE
            getReportsOperation(ApiService.create(),
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!, { response ->
                    pb_progress.visibility = GONE
                    reportList.clear()
                    reportList.addAll(ArrayList(response.filter { it.url != null }))
                    reportAdapter.notifyDataSetChanged()
                    if (reportList.isEmpty())
                        showError(getString(R.string.empty_reports))
                }, {
                    pb_progress.visibility = GONE
                    showError(it.second)
                })
        } else
            showAlertDialog(
                this,
                getString(R.string.no_internet_label),
                getString(R.string.no_internet_label)
            )
    }

    private fun showAPIError(error: Pair<Int, String>) {
        if (error.first == 401) {
            showAlertDialog(
                this, getString(R.string.unauthorized_title),
                getString(R.string.logged_out)
            ) {
                PreferenceUtils(this).clearData()
                val intent = Intent(this, SplashActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        } else
            showAlertDialog(this, getString(R.string.oops), error.second)
    }

    private fun showCalendarDialog(requestCode: Int) {
        val dialog = BottomSheetDialog(this)
        val dialogView = layoutInflater.inflate(R.layout.calendar_dialog, null)

        val calendar = Calendar.getInstance()
// Initial date
        val initialDate = CalendarDate(calendar.time)
// Minimum available date
//        val minDate = CalendarDate(calendar.time)
// Maximum available date
        val maxDate = CalendarDate(calendar.time)
        // The first day of week
        val firstDayOfWeek = Calendar.MONDAY

        // Set up calendar with all available parameters
        dialogView.cv_calendar.setupCalendar(
            initialDate = initialDate,
            minDate = null,
            maxDate = maxDate,
            selectionMode = CalendarView.SelectionMode.SINGLE,
            selectedDates = selectedDates,
            firstDayOfWeek = firstDayOfWeek,
            showYearSelectionView = true
        )
        dialogView.cv_calendar.onDateClickListener = {
            showLog("date: ${it.calendar.timeInMillis}")
            when (requestCode) {
                START_DATE_REQUEST -> {
                    startCalendar.timeInMillis = it.calendar.timeInMillis
                    tv_report_startDate.setText(getFormattedDate(startCalendar.timeInMillis))
                    dialog.dismiss()
                }

                END_DATE_REQUEST -> {
                    endCalendar.timeInMillis = it.calendar.timeInMillis
                    tv_report_endDate.setText(getFormattedDate(endCalendar.timeInMillis))
                    dialog.dismiss()
                }
            }
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun checkStoragePermission(fileUrl: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        setupDownload(fileUrl)
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        showShortToast(
                            this@ReportActivity,
                            getString(R.string.grant_image_permission)
                        )
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest,
                        token: PermissionToken
                    ) {
                        showAlertDialog(this@ReportActivity,
                            getString(R.string.permission_required_title),
                            getString(R.string.grant_image_permission),
                            "Yes",
                            "No", {
                                token.continuePermissionRequest()
                            }, {
                                token.cancelPermissionRequest()
                            })
                    }
                }).check()
        } else {
            setupDownload(fileUrl)
        }
    }

    private fun setupDownload(fileUrl: String) {
        val name = fileUrl.split("/")
        val fileName = name[name.lastIndex]
        showLog("filename $fileName")
        val parentPath =
            "${Environment.getExternalStorageDirectory()}" +
                    "${File.separator}${getString(R.string.app_name)}"
        val parentFile = File(parentPath)
        val docFilePath = "$parentPath${File.separator}$fileName"
        if (!parentFile.exists()) {
            showLog("parent file does not exist")
            if (parentFile.mkdirs()) {
                showLog("parent file created")
                downLoadFileFromUrl(docFilePath, "${BuildConfig.SERVER_URL_IMAGE}/pdf/$fileName")
            } else {
                showAlertDialog(
                    this,
                    getString(R.string.oops),
                    "Could not create the directory"
                )
            }
        } else {
            showLog("parent file exists")
            downLoadFileFromUrl(docFilePath, "${BuildConfig.SERVER_URL_IMAGE}$fileUrl")
        }
    }

    private fun downLoadFileFromUrl(outputFilePath: String, downloadUrl: String) {
        val progress = ProgressDialog.progressDialog(
            this,
            "Downloading report..."
        )
        progress.show()
        progress.setCanceledOnTouchOutside(false)
        downloadFile(
            this, outputFilePath,
            downloadUrl, {
                progress.dismiss()
                openReportFile(this, outputFilePath)
            }, {
                progress.dismiss()
                showAlertDialog(
                    this,
                    getString(R.string.oops),
                    it
                )
            }
        )
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}

package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.User
import kotlinx.android.synthetic.main.item_passenger.view.*

class UserAdapter(
    private val context: Context,
    private val mainList: ArrayList<User>,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_passenger_item_name
        val phone: TextView = itemView.tv_passenger_item_phone
        val removeIcon: ImageView = itemView.iv_passenger_item_removeIcon
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_passenger,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val currentItem = mainList[position]
        holder.removeIcon.visibility = GONE
        holder.name.text = currentItem.name
        holder.phone.text = currentItem.mobileNo
        holder.itemView.setOnClickListener {
            onItemClick(it, position)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
    }
}
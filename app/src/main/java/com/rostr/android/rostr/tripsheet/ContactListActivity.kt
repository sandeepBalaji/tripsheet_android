package com.rostr.android.rostr.tripsheet

import android.Manifest.permission.READ_CONTACTS
import android.content.Intent
import android.net.Uri.fromParts
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.adapter.ContactAdapter
import com.rostr.android.rostr.tripsheet.dialogs.QuickAddDriverDialog
import com.rostr.android.rostr.tripsheet.model.Contacts
import com.rostr.android.rostr.tripsheet.utils.formatPhoneNumber
import com.rostr.android.rostr.tripsheet.utils.showAlertDialog
import kotlinx.android.synthetic.main.activity_contact_list.*
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ContactListActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var contactList: ArrayList<Contacts>
    private lateinit var contactAdapter: ContactAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_list)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            tv_toolbar_title.text = "Your Contacts"
            initRecycler()
            et_contact_list_searchBox.hint = "Search contacts"
            bt_contact_list_addNewButton.setOnClickListener(this)
            bt_contact_list_importContacts.setOnClickListener(this)
            checkContactPermission()
        }
    }

    private fun checkContactPermission() {
        Dexter.withActivity(this)
            .withPermission(READ_CONTACTS)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    bt_contact_list_importContacts.visibility = GONE
                    readContacts()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    // check for permanent denial of permission
                    if (response != null && response.isPermanentlyDenied) {
                        bt_contact_list_importContacts.visibility = VISIBLE
                    } else
                        showSettingsDialog()
                }
            }).check()
    }

    private fun initRecycler() {
        contactList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ContactListActivity)
            contactAdapter = ContactAdapter(this@ContactListActivity, contactList) { _, position ->
                showAlertDialog(this@ContactListActivity, null,
                    getString(R.string.add_driver_confirmation, contactList[position].contactName),
                    "Yes", "Cancel", {
                        startDriverAddDialog(contactList[position])
                    }, {

                    })
            }
            adapter = contactAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_contact_list_importContacts -> {
                checkContactPermission()
            }

            R.id.bt_contact_list_addNewButton -> {
                startDriverAddDialog(null)
            }
        }
    }

    private fun startDriverAddDialog(contacts: Contacts?){
        if (supportFragmentManager.findFragmentByTag(getString(R.string.add_driver)) != null)
            (supportFragmentManager.findFragmentByTag(getString(R.string.add_driver)) as BottomSheetDialogFragment).dismiss()
        val addDriverDialog = QuickAddDriverDialog(getString(R.string.add_driver_details), contacts)
        addDriverDialog.show(supportFragmentManager, getString(R.string.add_driver))
    }


    /*
    * Function to read all contacts from the phonebook
    * */
    private fun readContacts() {
        pb_progress.visibility = VISIBLE
        tv_recyclerMessage.visibility = GONE
        doAsync {
            val phoneBookContacts = ArrayList<Contacts>()
            val cr = contentResolver
            val cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
            if (cursor != null && cursor.count > 0) {
                while (cursor.moveToNext()) {
                    val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    val name =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))
                    if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        val pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(id), null
                        )
                        while (pCur != null && pCur.moveToNext()) {
                            val phoneNo = pCur.getString(
                                pCur.getColumnIndex(
                                    ContactsContract.CommonDataKinds.Phone.NUMBER
                                )
                            )
                            if (phoneNo.length >= 10) {
                                val contact = Contacts(name, formatPhoneNumber(phoneNo))
                                if (!phoneBookContacts.contains(contact))
                                    phoneBookContacts.add(contact)
                            }
                        }
                        pCur?.close()
                    }
                }
                cursor.close()
            }

            uiThread {
                pb_progress.visibility = GONE
                if (phoneBookContacts.isEmpty())
                    tv_recyclerMessage.visibility = VISIBLE
                else {
                    contactList.clear()
                    contactList.addAll(phoneBookContacts.sortedWith(compareBy { it.contactName }))
                }
                contactAdapter.notifyDataSetChanged()
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private fun showSettingsDialog() {
        showAlertDialog(this, "Need Permission",
            "This app needs permission to use this feature. You can grant them in app settings.",
            "Goto Settings", "Cancel", {
                openSettings()
            }, {

            })
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
package com.rostr.android.rostr.tripsheet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.rostr.android.rostr.tripsheet.R;

public class SpinnerAdapter extends BaseAdapter implements android.widget.SpinnerAdapter {

    private final Context context;
    private String[] data;

    public SpinnerAdapter(Context context, String[] data) {
        this.data = data;
        this.context = context;
    }

    public int getCount() {
        return data.length;
    }

    public Object getItem(int position) {
        return data[position];
    }

    public long getItemId(int i) {
        return (long) i;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getDropDown(position, parent);
    }

    private View getDropDown(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View mySpinner;
        mySpinner = inflater.inflate(R.layout.custom_spinner, parent, false);

        TextView main_text = mySpinner.findViewById(R.id.tv_spinner_text);
        ImageView down_arrow = mySpinner.findViewById(R.id.iv_spinner_arrowIcon);
        main_text.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

        main_text.setText(data[position]);
        down_arrow.setVisibility(View.GONE);
        return mySpinner;
    }

    public View getView(int pos, View view, ViewGroup viewgroup) {
        return getCustomView(pos, viewgroup);
    }

    private View getCustomView(int position, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View mySpinner = inflater.inflate(R.layout.custom_spinner, parent, false);

        TextView main_text = mySpinner.findViewById(R.id.tv_spinner_text);
        ImageView down_arrow = mySpinner.findViewById(R.id.iv_spinner_arrowIcon);
        main_text.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText));

        main_text.setText(data[position]);
        down_arrow.setImageResource(R.drawable.ic_down_arrow);
        return mySpinner;
    }
}

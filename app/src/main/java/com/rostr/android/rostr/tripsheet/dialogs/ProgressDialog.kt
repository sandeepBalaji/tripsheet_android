package com.rostr.android.rostr.tripsheet.dialogs

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import com.rostr.android.rostr.tripsheet.R
import kotlinx.android.synthetic.main.progress_dialog.view.*

class ProgressDialog {
companion object {
    fun progressDialog(context: Context, content:String): Dialog {
        val dialog = Dialog(context)
        val inflate = LayoutInflater.from(context).inflate(R.layout.progress_dialog, null)
        inflate.content.text = content
        dialog.setContentView(inflate)
        dialog.setCancelable(false)
        dialog.window!!.setBackgroundDrawable(
                ColorDrawable(Color.TRANSPARENT)
        )
        return dialog
    }
  }
}
package com.rostr.android.rostr.tripsheet.model

data class VersionRequest(
    val currentVersion: Int,
    val deviceType: String
)

data class VersionResponse(
    val status: Int
)
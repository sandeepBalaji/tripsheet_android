package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class SignInRequest(
    val mobileNo: String
)

@Parcelize
data class SignInRes(
    val authKey: String,
    val mobileStore: MobileStore
): Parcelable

@Parcelize
data class MobileStore(
    val id: String,
    val mobileNo: String,
    val status: String,
    val updatedAt: String
): Parcelable

data class GetInfoRes(
    val createdAt: String?,
    val deviceInfo: DeviceInformation?,
    val dlNumber: String?,
    val id: String?,
    val mobileNo: String?,
    val status: String?,
    val updatedAt: String?
)

data class DeviceInformation(
    val deviceType: String?,
    val fcmToken: String?,
    val uniqueID: String?
)
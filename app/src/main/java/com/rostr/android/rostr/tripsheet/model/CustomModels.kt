package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class AnonymousResponse(
    val message: String?=null
)

data class HomeMenu(
    val icon: Int,
    val title: String
)

@Parcelize
data class PassengerModel(
    val name: String,
    val mobileNo: String
) : Parcelable

@Parcelize
data class DocumentModel(
    val refId: String,
    val imageUrl: String
): Parcelable
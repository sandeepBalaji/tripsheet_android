package com.rostr.android.rostr.tripsheet.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.rostr.android.rostr.tripsheet.R;

import static com.rostr.android.rostr.tripsheet.utils.MessageUtilsKt.showLog;

public class LabelledTextView extends CardView {

    ImageView startIcon, endIcon;
    ConstraintLayout parent;
    TextView textView, labelTextView;

    public LabelledTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.labelled_textview, this);
        initComponents();

        CharSequence hint = null, label = null;
        int startIconResource = 0, endIconResource = 0;
        Drawable bulletIconResource = null;

//        int[] sets = {R.attr.hint, R.attr.startDrawable, R.attr.endDrawable, R.attr.bulletDrawable, R.attr.label};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LabelledTextView);
        try {
            if (typedArray.getText(R.styleable.LabelledTextView_label) != null)
                label = typedArray.getText(R.styleable.LabelledTextView_label);
            if (typedArray.getText(R.styleable.LabelledTextView_hint) != null)
                hint = typedArray.getText(R.styleable.LabelledTextView_hint);
            if (isResource(context, typedArray.getResourceId(R.styleable.LabelledTextView_startDrawable, 0)))
                startIconResource = typedArray.getResourceId(R.styleable.LabelledTextView_startDrawable, 0);
            if (isResource(context, typedArray.getResourceId(R.styleable.LabelledTextView_endDrawable, 0)))
                endIconResource = typedArray.getResourceId(R.styleable.LabelledTextView_endDrawable, 0);
            if (isResource(context, typedArray.getResourceId(R.styleable.LabelledTextView_bulletDrawable, 0)))
                bulletIconResource = AppCompatResources.getDrawable(context, typedArray.getResourceId(R.styleable.LabelledTextView_bulletDrawable, 0));

            setParams(label, hint, startIconResource, endIconResource, bulletIconResource);
        } catch (Exception ex) {
            showLog(ex);
        } finally {
            typedArray.recycle();
        }
    }

    public static boolean isResource(Context context, int resId) {
        if (context != null) {
            try {
                return context.getResources().getResourceName(resId) != null;
            } catch (Resources.NotFoundException ignore) {
            }
        }
        return false;
    }

    private void initComponents() {
        parent = findViewById(R.id.cl_location_layoutParent);
        startIcon = findViewById(R.id.iv_location_layout_startIcon);
        endIcon = findViewById(R.id.iv_location_layout_endIcon);
        textView = findViewById(R.id.tv_location_layout_text);
        labelTextView = findViewById(R.id.tv_location_layout_label);
    }

    public void setParams(CharSequence label, CharSequence hint, int startRes, int endRes, Drawable bulletRes) {
        if (label != null)
            labelTextView.setText(label);
        else
            labelTextView.setVisibility(GONE);

        if (hint != null)
            textView.setHint(hint);

        if (startRes > 0) {
            startIcon.setImageResource(startRes);
        } else
            startIcon.setVisibility(GONE);

        if (endRes > 0) {
            endIcon.setImageResource(endRes);
        } else
            endIcon.setVisibility(GONE);

        if (bulletRes != null) {
            textView.setCompoundDrawablesWithIntrinsicBounds(bulletRes, null, null, null);
        }
    }

    public boolean isEmpty() {
        return textView.getText().toString().isEmpty();
    }

    public void setOnEndDrawableClickListener(OnClickListener clickListener) {
        endIcon.setOnClickListener(clickListener);
    }

    public void setOnViewClickListener(OnClickListener clickListener) {
        parent.setOnClickListener(clickListener);
    }

    public String getStringText() {
        return textView.getText().toString();
    }

    public void setText(String text) {
        textView.setText(text);
    }

    public void setText(SpannableStringBuilder text) {
        textView.setText(text);
    }

    public void setLabelText(String text) {
        labelTextView.setVisibility(VISIBLE);
        labelTextView.setText(text);
    }

    public void setEndDrawableTag(int tag) {
        endIcon.setTag(tag);
    }

    public Object getEndDrawableTag() {
        return endIcon.getTag();
    }

    public void setEndDrawable(int resDrawable) {
        endIcon.setImageResource(resDrawable);
    }

    public void setStartDrawable(int resDrawable) {
        if (resDrawable == 0)
            startIcon.setVisibility(GONE);
        else
            startIcon.setVisibility(VISIBLE);
        startIcon.setImageResource(resDrawable);
    }

    public void setBulletDrawable(int resDrawable) {
        if (resDrawable != 0) {
            Drawable drawable = AppCompatResources.getDrawable(getContext(), resDrawable);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        } else
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }
}
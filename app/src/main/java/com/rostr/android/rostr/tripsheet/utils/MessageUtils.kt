package com.rostr.android.rostr.tripsheet.utils

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.R


fun showLog(message: String) {
    if (BuildConfig.DEBUG)
        Log.e("TRIPSHEET", message)
}

fun showLog(ex: Exception) {
    if (BuildConfig.DEBUG)
        ex.printStackTrace()
}

fun showLog(message: String, bundle: Bundle) {
    if (BuildConfig.DEBUG) {
        showLog(message)
        for (key in bundle.keySet()) {
            Log.e("Bundle Debug", key + " = \"" + bundle.get(key) + "\"")
        }
    }
}

fun showLongSnack(parent: View, message: String){
    val snack = Snackbar
            .make(parent, message, Snackbar.LENGTH_LONG)
     val sbView: View = snack.getView()
    val textView: TextView = sbView.findViewById(com.google.android.material.R.id.snackbar_text)
    textView.setTextColor(Color.WHITE)
    snack.show()
}

fun showShortToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}

fun showLongToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}

fun showAlertDialog(context: Context, title: String?, message: Int) {
    val builder = AlertDialog.Builder(context, R.style.MyMaterialDialogStyle)
    builder.setCancelable(false)
    if (title != null)
        builder.setTitle(title)
    builder.setMessage(message)
    builder.setPositiveButton("Ok") { dialog, _ ->
        dialog.dismiss()
    }
    builder.show()
}

fun showAlertDialog(context: Context, title: String?, message: String) {
    try {
        val builder = AlertDialog.Builder(context, R.style.Theme_Tasker_Dialog)
        builder.setCancelable(false)
        if (title != null)
            builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    } catch (ex: Exception) {
        showLog(ex)
    }
}

fun showAlertDialog(
    context: Context, title: String?, message: String,
    onButtonClick: () -> Unit
) {
    try {
        val builder = AlertDialog.Builder(context, R.style.Theme_Tasker_Dialog)
        builder.setCancelable(false)
        if (title != null)
            builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
            onButtonClick()
        }
        builder.show()
    } catch (ex: Exception) {
        showLog(ex)
    }
}

fun showAlertDialog(
    context: Context, title: String?, message: String,
    posText: String,
    negText: String,
    onPositiveClick: (context: Context) -> Unit,
    onNegativeClick: (context: Context) -> Unit
) {
    try {
        val builder = AlertDialog.Builder(context, R.style.Theme_Tasker_Dialog)
        builder.setCancelable(false)
        if (title != null)
            builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(posText) { dialog, _ ->
            dialog.dismiss()
            onPositiveClick(context)
        }
        builder.setNegativeButton(negText) { dialog, _ ->
            dialog.dismiss()
            onNegativeClick(context)
        }
        builder.show()
    } catch (ex: java.lang.Exception) {
        showLog(ex)
    }

}

fun showAlertDialog(
    context: Context, title: String?, message: SpannableStringBuilder,
    posText: String,
    negText: String,
    onPositiveClick: (context: Context) -> Unit,
    onNegativeClick: (context: Context) -> Unit
) {
    try {
        val builder = AlertDialog.Builder(context, R.style.Theme_Tasker_Dialog)
        builder.setCancelable(false)
        if (title != null)
            builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton(posText) { dialog, _ ->
            dialog.dismiss()
            onPositiveClick(context)
        }
        builder.setNegativeButton(negText) { dialog, _ ->
            dialog.dismiss()
            onNegativeClick(context)
        }
        builder.show()
    } catch (ex: java.lang.Exception) {
        showLog(ex)
    }

}
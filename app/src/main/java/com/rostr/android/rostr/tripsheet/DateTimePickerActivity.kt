package com.rostr.android.rostr.tripsheet

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_date_time_picker.*
import java.util.*


class DateTimePickerActivity: AppCompatActivity(), View.OnClickListener {

    private lateinit var selectedCalendar: Calendar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date_time_picker)

        selectedCalendar = Calendar.getInstance()
       /* dp_picker_date.minDate = selectedCalendar.timeInMillis
        if (intent.extras != null) {
            selectedCalendar.timeInMillis = intent.getLongExtra(DATE_KEY, 0)
            dp_picker_date.updateDate(selectedCalendar.get(Calendar.YEAR), selectedCalendar.get(Calendar.MONTH),
                selectedCalendar.get(Calendar.DAY_OF_MONTH))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tp_picker_time.hour = selectedCalendar.get(Calendar.HOUR_OF_DAY)
                tp_picker_time.minute = selectedCalendar.get(Calendar.MINUTE)
            } else {
                tp_picker_time.currentHour = selectedCalendar.get(Calendar.HOUR_OF_DAY)
                tp_picker_time.currentMinute = selectedCalendar.get(Calendar.MINUTE)
            }
        }*/
        setClickListeners()
    }

    private fun setClickListeners(){
        bt_picker_done.setOnClickListener(this)
        iv_picker_closeIcon.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.bt_picker_done ->{
                selectedCalendar.set(Calendar.DAY_OF_MONTH, dp_picker_date.dayOfMonth)
                selectedCalendar.set(Calendar.MONTH, dp_picker_date.month)
                selectedCalendar.set(Calendar.YEAR, dp_picker_date.year)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    selectedCalendar.set(Calendar.HOUR_OF_DAY, tp_picker_time.hour)
                    selectedCalendar.set(Calendar.MINUTE, tp_picker_time.minute)
                } else {
                    selectedCalendar.set(Calendar.HOUR_OF_DAY, tp_picker_time.currentHour)
                    selectedCalendar.set(Calendar.MINUTE, tp_picker_time.currentMinute)
                }
                showLog(
                    "Time selected " + getFormattedDateTime(selectedCalendar) + selectedCalendar
                )
                val intent = Intent()
                intent.putExtra(DATE_KEY, selectedCalendar.timeInMillis)
                setResult(RESULT_OK, intent)
                onBackAction()
                /*if (isDateTimeValid(selectedCalendar)) {
                    val intent = Intent()
                    intent.putExtra(DATE_KEY, selectedCalendar.timeInMillis)
                    setResult(RESULT_OK, intent)
                    onBackAction()
                } else {
                    showAlertDialog(this, null, "Please select a future date and time")
                }*/
            }
            R.id.iv_picker_closeIcon -> {
                onBackAction()
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        onBackAction()
    }

    private fun onBackAction() {
        finish()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }
}
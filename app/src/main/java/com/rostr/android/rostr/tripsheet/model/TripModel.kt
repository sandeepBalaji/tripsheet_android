package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TripCreateRequest(
    val _id: String? = null,
    val tripName: String,
    val customer: String,
    val driver: String,
    val vehicle: String?,
    val tripType: String,
    val acType: String,
    val source: Address,
    val destination: Address,
    val dateOfRide: Long,
    val endDate: Long? = null,
    val startTime: Long,
    val endTime: Long? = 0,
    val notes: String? = null,
    val passengers: ArrayList<PassengerModel>? = null,
    val amount: Double,
    val toll: Double? = 0.0,
    val driverBata: Double? = 0.0,
    val otherCost: Double? = 0.0,
    val paymentDate: Long? = null,
    val gstInfo: GstModel?
) : Parcelable

@Parcelize
data class TripResponse(
    val _id: String? = null,
    val tripId: String? = null,
    val tripName: String,
    val customer: TripCustomer,
    val driver: TripDriver?,
    val tripType: String,
    val source: Address,
    val destination: Address,
    val dateOfRide: Long,
    val startTime: Long,
    val passengers: ArrayList<PassengerModel>? = null,
    val amount: Double,
    val rideCost: Double,
    val paymentDate: Long,
    val gstInfo: GstModel?,
    val toll: Double? = 0.0,
    val notes: String? = null,
    val driverBata: Double? = 0.0,
    val otherCost: Double? = 0.0,
    val vehicle: DriverVehicle?,// change
    val tripNotes: ArrayList<TripNoteResponse>? = null
) : Parcelable

@Parcelize
data class TripDriver(
    val _id: String,
    val mobileNo: String,
    val name: String
) : Parcelable

@Parcelize
data class TripCustomer(
    val _id: String,
    val companyName: String? = null,
    val email: String? = null,
    val mobileNo: String,
    val name: String
) : Parcelable

@Parcelize
data class GstModel(
    val CGST: Double? = null,
    val SGST: Double? = null,
    val IGST: Double? = null
) : Parcelable
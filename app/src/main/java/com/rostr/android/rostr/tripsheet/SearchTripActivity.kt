package com.rostr.android.rostr.tripsheet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.rostr.android.rostr.tripsheet.adapter.TripAdapter
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getDriverTripsOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.calendar_dialog.view.*
import kotlinx.android.synthetic.main.fragment_trips.*
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.*
import ru.cleverpumpkin.calendar.CalendarDate
import ru.cleverpumpkin.calendar.CalendarView
import java.util.*
import kotlin.collections.ArrayList

class SearchTripActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var tripList: ArrayList<TripResponse>
    private lateinit var filteredList: ArrayList<TripResponse>
    private lateinit var tripAdapter: TripAdapter
    private lateinit var driverId: String

    private lateinit var dateCalendar: Calendar
    private var selectedDates: List<CalendarDate> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_trips)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            tv_toolbar_title.text = "Search Trip"

            driverId = intent.getStringExtra(DATA_KEY)
            dateCalendar = Calendar.getInstance()
            dateCalendar.timeInMillis = getOnlyDateInMillis(dateCalendar.timeInMillis)
            tv_trips_date.text = getFormattedDate(dateCalendar.timeInMillis)

            tv_trips_date.setOnClickListener(this)
            initRecycler()
        }
    }

    private fun initRecycler() {
        tripList = ArrayList()
        filteredList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SearchTripActivity)
            tripAdapter =
                TripAdapter(this@SearchTripActivity, filteredList, true) { _, position ->
                    val intent = Intent()
                    intent.putExtra(DATA_KEY, filteredList[position])
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            adapter = tripAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(this)) {
            pb_progress.visibility = View.VISIBLE
            tv_recyclerMessage.visibility = View.GONE
            getDriverTripsOperation(
                ApiService.create(),
                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                driverId, {
                    pb_progress.visibility = View.GONE
                    tripList.clear()
                    tripList.addAll(it)
                    showData()
                }, {
                    pb_progress.visibility = View.GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            this, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            PreferenceUtils(this).clearData()
                            val intent = Intent(this, SplashActivity::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                        }
                    } else
                        showError(it.second)
                })
        } else
            showError(getString(R.string.no_internet))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_trips_date -> {
                showCalendarDialog()
            }
        }
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = View.VISIBLE
    }

    private fun showCalendarDialog() {
        val dialog = BottomSheetDialog(this)
        val dialogView = layoutInflater.inflate(R.layout.calendar_dialog, null)

        val calendar = Calendar.getInstance()
// Initial date
        val initialDate = CalendarDate(calendar.time)
// Minimum available date
        val minDate = CalendarDate(calendar.time)
// Maximum available date
        val maxDate = CalendarDate(calendar.time)
        // The first day of week
        val firstDayOfWeek = Calendar.MONDAY

        // Set up calendar with all available parameters
        dialogView.cv_calendar.setupCalendar(
            initialDate = initialDate,
            minDate = null,
            maxDate = maxDate,
            selectionMode = CalendarView.SelectionMode.SINGLE,
            selectedDates = selectedDates,
            firstDayOfWeek = firstDayOfWeek,
            showYearSelectionView = true
        )
        dialogView.cv_calendar.onDateClickListener = {
            showLog("date: ${it.calendar.timeInMillis}")
            dateCalendar.timeInMillis = it.calendar.timeInMillis
            tv_trips_date.text = getFormattedDate(dateCalendar.timeInMillis)
            tv_recyclerMessage.visibility = View.GONE
            bt_recycler_actionButton.visibility = View.GONE
            dialog.dismiss()
            showData()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun showData() {
        filteredList.clear()
        filteredList.addAll(applyFilter())
        tripAdapter.notifyDataSetChanged()
        if (filteredList.isEmpty())
            showError(getString(R.string.empty_trips))
    }

    private fun applyFilter(): ArrayList<TripResponse> {
        showLog("size ${tripList.size}")
        val list = tripList.filter { it.dateOfRide == dateCalendar.timeInMillis }
        showLog("size after  ${list.size}")
        return ArrayList(list)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
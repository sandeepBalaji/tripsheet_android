package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.view.LayoutInflater
import com.rostr.android.rostr.tripsheet.R
import java.util.ArrayList


class OptionsMenuAdapter(
    val context: Context,
    private val list: Array<String>
) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val mySpinner = inflater.inflate(R.layout.item_option, parent, false)
        val optionText: TextView = mySpinner.findViewById(R.id.tv_option_item_text)
        optionText.text = getItem(position)
        return mySpinner
    }

    override fun getItem(position: Int): String {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }
}
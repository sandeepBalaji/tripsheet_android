package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.rostr.android.rostr.tripsheet.fragments.tripDetailFragments.TripNotesFragment
import com.rostr.android.rostr.tripsheet.fragments.tripDetailFragments.TripTransactionFragment
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.deleteOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_trip_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class TripDetailActivity : AppCompatActivity() {

    private lateinit var tripData: TripResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trip_detail)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            if (intent != null) {
                if (intent.hasExtra(DATA_KEY)) {
                    try {
                        tv_toolbar_title.text = getString(R.string.app_name)
                        tripData = intent.extras?.getParcelable(DATA_KEY)!!
                        tv_trip_detail_tripName.setText(tripData.tripName)
                        setTabs()
                        loadDefaultFragment()
                    } catch (ex: Exception) {
                        showLog(ex)
                    }
                }
            } else {
                showAlertDialog(this, getString(R.string.oops), "Did not receive driver details") {
                    finish()
                }
            }
        }
    }

    private fun loadDefaultFragment() {
        val fragment = TripNotesFragment()
        val bundle = Bundle()
        bundle.putParcelable(DATA_KEY, tripData)
        fragment.arguments = bundle
        replaceFragment(fragment)
    }

    private fun setTabs() {
        tl_trip_detail_tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        loadDefaultFragment()
                    }

                    else -> {
                        val fragment = TripTransactionFragment()
                        val bundle = Bundle()
                        bundle.putString(DATA_KEY, tripData._id)
                        fragment.arguments = bundle
                        replaceFragment(fragment)
                    }
                }
            }
        })
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_trip_detail_fragmentContainer, fragment).commit()
    }

    /*private fun setData() {
        tv_toolbar_title.text = tripData.tripName
        tv_trip_detail_tripType.setText(tripData.tripType)
        tv_trip_detail_amount.setText(getString(R.string.rs_value, tripData.amount))
        tv_trip_detail_customer.setText(tripData.customer.name)
        val cal = Calendar.getInstance()
        cal.timeInMillis = appendDateTime(tripData.dateOfRide, tripData.startTime)
        tv_trip_detail_dateTime.setText(getFormattedDateTime(cal))
        tv_trip_detail_driver.setText(tripData.driver?.name)
        tv_trip_detail_location.sourceText = tripData.source.addressLine
        tv_trip_detail_location.destText = tripData.destination.addressLine
        rv_trip_detail_passengers.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@TripDetailActivity)
            adapter = PassengerAdapter(this@TripDetailActivity, tripData.passengers, null)
        }

        rv_trip_detail_notes.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@TripDetailActivity)
            adapter = NoteAdapter(this@TripDetailActivity, tripData.tripNotes, null)
        }
    }*/

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (PreferenceUtils(this).getString(ROLE_KEY) == "transporter")
            menuInflater.inflate(R.menu.menu_trip, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
            }

            R.id.trip_edit -> {
                val intent = Intent(this, EditTripActivity::class.java)
                intent.putExtra(DATA_KEY, tripData)
                startActivity(intent)
            }

            R.id.trip_delete -> {
                showAlertDialog(this, null,
                    "Are you sure you want to delete trip: ${tripData.tripName}?",
                    "Yes", "No", {
                        if (isConnectedToInternet(this))
                            deleteOperation(
                                ApiService.create(),
                                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                "${BuildConfig.SERVER_URL}trips/${tripData._id}",
                                {
                                    showAlertDialog(this, null, "Successfully deleted trip!") {
                                        finish()
                                    }
                                }, {
                                    showAlertDialog(this, getString(R.string.oops), it.second)
                                }
                            )
                        else
                            showAlertDialog(
                                this,
                                getString(R.string.no_internet_label),
                                getString(R.string.no_internet)
                            )
                    }, {

                    })
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
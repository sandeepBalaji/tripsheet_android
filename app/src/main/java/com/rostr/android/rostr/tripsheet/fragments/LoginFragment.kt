package com.rostr.android.rostr.tripsheet.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.rostr.android.rostr.tripsheet.LoginActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.SignInRes
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {
    private lateinit var mContext: Context
    private lateinit var parentActivity: LoginActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity as LoginActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bt_login.setOnClickListener {
            if (!login_mobileNumber.isPhoneValid)
                showShortToast(mContext, getString(R.string.invalid_phone_number))
            else {
                if (isConnectedToInternet(mContext))
                    parentActivity.doSignIn(login_mobileNumber.value)
                else
                    showAlertDialog(
                        mContext,
                        getString(R.string.no_internet_label),
                        getString(R.string.no_internet)
                    )
            }
        }
    }

    fun doAfterLoginOperation(signInRes: SignInRes) {
        val verifyFragment = VerificationFragment()
        val bundle = Bundle()
        /*bundle.putString(ID_KEY, signInRes.mobileStore.id)
        bundle.putString(STATUS_KEY, signInRes.mobileStore.status)*/
        bundle.putParcelable(DATA_KEY, signInRes)
        bundle.putString(MOBILE_NO, login_mobileNumber.value)
        verifyFragment.arguments = bundle
        parentActivity.replaceFragment(verifyFragment)
    }
}
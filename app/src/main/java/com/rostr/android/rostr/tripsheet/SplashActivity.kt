package com.rostr.android.rostr.tripsheet

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.rostr.android.rostr.tripsheet.model.UserSingleton
import com.rostr.android.rostr.tripsheet.model.VersionRequest
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.checkVersionOperation
import com.rostr.android.rostr.tripsheet.network.getMyProfileOperation
import com.rostr.android.rostr.tripsheet.utils.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        FirebaseApp.initializeApp(this)
    }

    override fun onResume() {
        super.onResume()
        checkVersion()
    }

    private fun checkVersion() {
        if (isConnectedToInternet(this)) {
            checkVersionOperation(ApiService.create(),
                VersionRequest(deviceType = "Android", currentVersion = BuildConfig.VERSION_CODE), {
                    when (it.status) {
                        0 -> { // compulsory Update
                            showAlertDialog(this,
                                getString(R.string.compulsory_update_label),
                                getCompulsoryString(getString(R.string.compulsory_update)),
                                "UPDATE NOW",
                                "NOT NOW",
                                {
                                    try {
                                        startActivity(
                                            Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse("market://details?id= $packageName")
                                            )
                                        )
                                    } catch (ex: ActivityNotFoundException) {
                                        showLog(ex)
                                        startActivity(
                                            Intent(
                                                Intent.ACTION_VIEW,
                                                Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                            )
                                        )
                                    }
                                },
                                {
                                    finish()
                                })
                        }
                        1 -> { // optional update
                            if (PreferenceUtils(this).getInt(LAST_VERSION_CHECKED) != BuildConfig.VERSION_CODE) {
                                showAlertDialog(this,
                                    getString(R.string.optional_update_label),
                                    getOptionalString(getString(R.string.optional_update)),
                                    "UPDATE NOW",
                                    "SKIP",
                                    {
                                        PreferenceUtils(this).setData(
                                            LAST_VERSION_CHECKED,
                                            BuildConfig.VERSION_CODE
                                        )
                                        try {
                                            startActivity(
                                                Intent(
                                                    Intent.ACTION_VIEW,
                                                    Uri.parse("market://details?id= $packageName")
                                                )
                                            )
                                        } catch (ex: ActivityNotFoundException) {
                                            showLog(ex)
                                            startActivity(
                                                Intent(
                                                    Intent.ACTION_VIEW,
                                                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                                                )
                                            )
                                        }
                                    },
                                    {
                                        PreferenceUtils(this).setData(
                                            LAST_VERSION_CHECKED,
                                            BuildConfig.VERSION_CODE
                                        )
                                        navigate()
                                    })
                            } else {
                                navigate()
                            }
                        }
                        2 -> { // do nothing
                            navigate()
                        }
                    }
                }, {
                    showAlertDialog(this, getString(R.string.oops), it.second) {
                        finish()
                    }
                })
        } else {
            showAlertDialog(
                this,
                getString(R.string.no_internet_label),
                getString(R.string.no_internet)
            ) {
                finish()
            }
        }
    }

    private fun getMyProfile() {
        val url = "${BuildConfig.SERVER_URL}users/${PreferenceUtils(this).getString(ID_KEY)}"
        getMyProfileOperation(ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            url, {
                try {
                    UserSingleton.instance = it
                    if (it.driver == null && it.transporter == null) {
                        val intent = Intent(this, CreateProfileActivity::class.java)
                        intent.putExtra(
                            ROLE_KEY,
                            "transporter"
                        )
                        startActivity(intent)
//                    startActivity(Intent(this@SplashActivity, RoleParentActivity::class.java))
                    } else if (it.driver != null && it.driver.isActive) {
                        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    } else if (it.transporter != null && it.transporter!!.isActive) {
                        startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                    }
                    finish()
                } catch (ex: Exception) {
                    showLog(ex)
                    showAlertDialog(this, getString(R.string.oops), ex.localizedMessage) {
                        finish()
                    }
                }

            }, {
                if (it.first == 401) {
                    showAlertDialog(
                        this, getString(R.string.unauthorized_title),
                        getString(R.string.logged_out)
                    ) {
                        logoutUser(this)
                    }
                } else {
                    showAlertDialog(this, getString(R.string.oops), it.second) {
                        finish()
                    }
                }
            })
    }

    private fun navigate() {
        if (PreferenceUtils(this).getString(ACCESS_TOKEN) != null) {
            getMyProfile()
        } else if (PreferenceUtils(this).getString(TEMP_TOKEN) != null) {
            val intent = Intent(this, CreateProfileActivity::class.java)
            intent.putExtra(
                ROLE_KEY,
                "transporter"
            )
            startActivity(intent)
//            startActivity(Intent(this@SplashActivity, RoleParentActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this@SplashActivity, StartActivity::class.java))
            finish()
        }
    }
}

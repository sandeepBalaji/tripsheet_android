package com.rostr.android.rostr.tripsheet.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.FileProvider
import com.rostr.android.rostr.tripsheet.LoginActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.StartActivity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun getSystemTimeStamp(): String {
    return Calendar.getInstance().timeInMillis.toString()
}

fun getFormattedDate(date: Long): String {
    try {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date
        val sdf = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        return sdf.format(calendar.time)
    } catch (ex: Exception) {
        showLog(ex)
    }
    return ""
}

fun getFormattedTime(time: Long): String {
    try {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = time
        val sdf = SimpleDateFormat("hh:mm aa", Locale.ENGLISH)
        return sdf.format(calendar.time)
    } catch (ex: Exception) {
        showLog(ex)
    }
    return ""
}

fun getFormattedDateTime(calendar: Calendar): String {
    try {
        val sdf = SimpleDateFormat("dd MMM yyyy, hh:mm aa", Locale.ENGLISH)
        return sdf.format(calendar.time)
    } catch (ex: Exception) {
        showLog(ex)
    }
    return ""
}

fun isEndAfterStart(end: Calendar, start: Calendar): Boolean {
    showLog("start: ${getFormattedDateTime(end)} end: ${getFormattedDateTime(end)}")
    return end.after(start) || start === end
}

fun getOnlyDateInMillis(dateTimeInMillis: Long): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = dateTimeInMillis
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    showLog("Only date " + calendar.timeInMillis)
    return calendar.timeInMillis
}

fun getOnlyTimeInMillis(dateTimeInMillis: Long): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = dateTimeInMillis
    showLog("Only time " + getLocalTimeInMillis(calendar.timeInMillis))
    return getLocalTimeInMillis(calendar.timeInMillis)
}

fun getLocalTimeInMillis(milliSeconds: Long): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliSeconds
    val sdf = SimpleDateFormat("hh:mm aa", Locale.ENGLISH)
    val date = Date(calendar.timeInMillis)
    val timeInLocal = sdf.format(date)
    showLog("Time in local $timeInLocal")
    try {
        val localDate = sdf.parse(timeInLocal)
        showLog("Timestamp in local " + localDate.time)
        return localDate.time
    } catch (pex: ParseException) {
        showLog(pex)
    }

    return 0
}

fun logoutUser(activity: Activity) {
    PreferenceUtils(activity).clearData()
    startLogin(activity)
}

fun startLogin(context: Context) {
    val intent = Intent(context, StartActivity::class.java)
    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    context.startActivity(intent)
}

fun formatPhoneNumber(phone: String): String {
    var formattedPhone = phone
    formattedPhone = formattedPhone.replace("\\s".toRegex(), "")
    return formattedPhone.takeLast(10)
}

fun isConnectedToInternet(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val networkInfo = connectivityManager.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}

fun hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun appendDateTime(date: Long, time: Long): Long {
    val dateCal: Calendar = Calendar.getInstance()
    dateCal.timeInMillis = date
    val timeCal: Calendar = Calendar.getInstance()
    timeCal.timeInMillis = time
    dateCal.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY))
    dateCal.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE))
    dateCal.set(Calendar.SECOND, timeCal.get(Calendar.SECOND))
    dateCal.set(Calendar.MILLISECOND, timeCal.get(Calendar.MILLISECOND))
    return dateCal.timeInMillis
}

fun downloadFile(
    context: Context, filePath: String, urlString: String,
    onSuccess: () -> Unit,
    onFailure: (error: String) -> Unit
) {
    var error: String? = null
    context.doAsync {
        try {
            showLog("Downloading file from $urlString to $filePath")
            URL(urlString).openStream().use { input ->
                FileOutputStream(File(filePath)).use { output ->
                    input.copyTo(output)
                    showLog("done writing to file")
                }
            }
        } catch (ex: Exception) {
            showLog(ex)
            error = "Error occurred downloading file ${ex.localizedMessage}"
        }
        uiThread {
            if (error.isNullOrEmpty())
                onSuccess()
            else
                onFailure(error!!)
        }
    }
}

fun openInvoiceFile(context: Context, filePath: String) {
    val pdfFile = File(filePath)
    val pdfIntent = Intent(Intent.ACTION_VIEW)
    val fileUri = FileProvider.getUriForFile(
        context, "${context.packageName}.provider", pdfFile
    )
    pdfIntent.setDataAndType(fileUri, "application/pdf")
    pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    pdfIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    try {
        context.startActivity(pdfIntent)
    } catch (e: ActivityNotFoundException) {
        showAlertDialog(
            context,
            context.getString(R.string.oops),
            "No Application available to view PDF"
        )
    } catch (ex: Exception) {
        showLog(ex)
        showAlertDialog(
            context,
            context.getString(R.string.oops),
            ex.localizedMessage
        )
    }
}

fun openReportFile(context: Context, filePath: String) {
    val pdfFile = File(filePath)
    val pdfIntent = Intent(Intent.ACTION_VIEW)
    val fileUri = FileProvider.getUriForFile(
        context, "${context.packageName}.provider", pdfFile
    )
    pdfIntent.setDataAndType(fileUri, "text/csv")
    pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    try {
        context.startActivity(pdfIntent)
    } catch (e: ActivityNotFoundException) {
        showAlertDialog(
            context,
            context.getString(R.string.oops),
            "No Application available to view excel files"
        )
    } catch (ex: Exception) {
        showLog(ex)
        showAlertDialog(
            context,
            context.getString(R.string.oops),
            ex.localizedMessage
        )
    }
}
package com.rostr.android.rostr.tripsheet.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.rostr.android.rostr.tripsheet.model.UserSingleton


class PreferenceUtils(val context: Context) {
    private val CACHE_MAIN_KEY = "tripsheet_cache"
    private var sharedPref: SharedPreferences = context.getSharedPreferences(CACHE_MAIN_KEY, MODE_PRIVATE)

    fun setData(key: String, value: String?){
        sharedPref.edit().putString(key, value).apply()
    }

    fun getString(key: String): String? {
        return sharedPref.getString(key, null)
    }

    fun removeString(key:String){
        sharedPref.edit().remove(key).apply()
    }

    fun setData(key: String, value: Int) {
        sharedPref.edit().putInt(key, value).apply()
    }

    fun getInt(key: String): Int {
        return sharedPref.getInt(key, 0)
    }

    fun setData(key: String, value: Boolean) {
        sharedPref.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String): Boolean{
        return sharedPref.getBoolean(key, false)
    }

    fun clearData() {
        val defPref = PreferenceManager.getDefaultSharedPreferences(context)
        defPref.edit().clear().apply()
        UserSingleton.instance = null
        sharedPref.edit().clear().apply()
    }
}
package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.DriversTransporter
import com.rostr.android.rostr.tripsheet.model.Transporter
import kotlinx.android.synthetic.main.item_driver.view.*

class TransporterAdapter(
    private val context: Context,
    private val mainList: ArrayList<Any>,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<TransporterAdapter.CustomerViewHolder>() {

    class CustomerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_driver_item_name
        val phone: TextView = itemView.tv_driver_item_vehicleType
        val amount: TextView = itemView.tv_driver_item_amount
        val vehicleNo: TextView = itemView.tv_driver_item_vehicleNo
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        return CustomerViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_driver,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        if (mainList[position] is Transporter) {
            holder.name.text = (mainList[position] as Transporter).name
            holder.phone.text = (mainList[position] as Transporter).mobileNo

        } else {
            holder.name.text = (mainList[position] as DriversTransporter).name
            holder.phone.text = (mainList[position] as DriversTransporter).mobileNo
        }
        holder.itemView.setOnClickListener {
            onItemClick(it, position)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransactionRequest(
    val _id: String? = null,
    val driver: String? = null,
    val trip: String? = null,
    val date: Long,
    val total: String? = null,
    val paymentStatus: String? = null,
    val purpose: String,
    val remarks: String? = null,
    val images: ArrayList<String>? = null
) : Parcelable
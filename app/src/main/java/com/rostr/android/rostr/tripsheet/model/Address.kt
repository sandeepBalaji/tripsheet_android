package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(
    val addressLine: String?,
    val location: Location? = null,
    val state: String? = null,
    val city: String? = null,
    val pincode: String? = null
) : Parcelable

@Parcelize
data class Location(
    val lat: Double? = 0.0,
    val long: Double? = 0.0
) : Parcelable

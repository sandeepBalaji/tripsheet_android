package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.utils.getFormattedTime
import kotlinx.android.synthetic.main.item_trip.view.*

class TripAdapter(
    private val context: Context,
    private val mainList: ArrayList<TripResponse>,
    private val isSearch: Boolean,
    val onItemClick: ((view: View, position: Int) -> Unit)?
) :
    RecyclerView.Adapter<TripAdapter.TripViewHolder>() {

    class TripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tripName: TextView = itemView.tv_trip_item_tripName
        val dateTime: TextView = itemView.tv_trip_item_rideDateTime
        val source: TextView = itemView.tv_trip_item_source
        val destination: TextView = itemView.tv_trip_item_destination
        val amount: TextView = itemView.tv_trip_item_tripAmount
        val type: TextView = itemView.tv_trip_item_tripType
        val customerName: TextView = itemView.tv_trip_item_customerName
        val driverName: TextView = itemView.tv_trip_item_driverName
        val vehicleNumber: TextView = itemView.tv_trip_item_vehicleNo
        val invoice: TextView = itemView.tv_trip_item_invoice
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        return TripViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_trip,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        val currentItem = mainList[position]
        holder.tripName.text = currentItem.tripName
        holder.invoice.text = context.getString(R.string.trip_id, currentItem.tripId)
        holder.dateTime.text = getFormattedTime(currentItem.startTime)
        holder.source.text = currentItem.source.addressLine
        holder.destination.text = currentItem.destination.addressLine
        holder.type.text = currentItem.tripType
        holder.customerName.visibility = VISIBLE
        holder.customerName.text = currentItem.customer.name
        holder.vehicleNumber.text = currentItem.vehicle?.regNumber
        if (currentItem.amount == 0.0)
            holder.amount.visibility = GONE
        else {
            holder.amount.text = context.getString(R.string.rs_value, currentItem.amount)
            holder.amount.visibility = VISIBLE
        }
        holder.driverName.text = currentItem.driver?.name

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(it, position)
        }
        holder.invoice.setOnClickListener {
            onItemClick?.invoke(it, position)
        }
        if (isSearch)
            holder.invoice.visibility = GONE
        else {
            // don't put it in the above if else as this will reset the visibility
            if (currentItem.amount == 0.0)
                holder.invoice.visibility = GONE
            else
                holder.invoice.visibility = VISIBLE
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
package com.rostr.android.rostr.tripsheet.model

data class Contacts(
    val contactName: String,
    val contactNumber: String
)
package com.rostr.android.rostr.tripsheet.fragments.driverHomeFragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.ListPopupWindow
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.TransactionListActivity
import com.rostr.android.rostr.tripsheet.adapter.DriversTransporterAdapter
import com.rostr.android.rostr.tripsheet.adapter.OptionsMenuAdapter
import com.rostr.android.rostr.tripsheet.model.DriversTransporter
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getDriversProfileOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.recycler_layout.*

class TransporterFragment : Fragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var transporterList: ArrayList<DriversTransporter>
    private lateinit var filteredList: ArrayList<DriversTransporter>
    private lateinit var parentActivity: FragmentActivity
    private lateinit var transporterAdapter: DriversTransporterAdapter
    private var sortPosition = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        tv_title.text = getString(R.string.transporter_list)
        actv_user_searchBox.hint = "Search transporter"
        iv_sortIcon.setOnClickListener(this)
    }

    private fun initRecycler() {
        transporterList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
            transporterAdapter =
                DriversTransporterAdapter(mContext, transporterList) { _, position ->
                    val intent = Intent(mContext, TransactionListActivity::class.java)
                    intent.putExtra(DATA_KEY, transporterList[position])
                    startActivity(intent)
                }
            adapter = transporterAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_sortIcon -> {
                if (transporterList.isNullOrEmpty())
                    showLongSnack(cl_users_parent, "Cannot sort an empty list")
                else
                    showSortOptions()
            }

            R.id.iv_searchIcon -> {
                if (actv_user_searchBox.visibility == View.VISIBLE) {
                    actv_user_searchBox.visibility = View.GONE
                    if (!isEmpty(actv_user_searchBox)) {
                        actv_user_searchBox.setText("")
                        showSortedList(sortPosition)
                    }
                } else
                    actv_user_searchBox.visibility = View.VISIBLE
            }
        }
    }

    private fun showSortOptions() {
        val options = resources.getStringArray(R.array.driver_sort_options)
        val popAdapter = OptionsMenuAdapter(mContext, options)
        val mPopupWindow = ListPopupWindow(mContext)
        mPopupWindow.setAdapter(popAdapter)
        mPopupWindow.anchorView = iv_sortIcon
        mPopupWindow.width = 580
        mPopupWindow.horizontalOffset = -500 //<--this provides the margin you need
        mPopupWindow.setOnItemClickListener { _, _, position, _ ->
            //            tv_search_sortBy.text = options[position]
            showSortedList(position)
            mPopupWindow.dismiss()
        }
        mPopupWindow.show()
    }

    private fun showSortedList(sortItemPosition: Int) {
        filteredList.clear()
        when (sortItemPosition) {
            0 -> { // name
                filteredList.addAll(ArrayList(transporterList.sortedWith(compareBy { it.name })))
            }

            1 -> { // high to low
                filteredList.addAll(ArrayList(transporterList.sortedWith(compareBy { it.drivers[0].due }).reversed()))
            }

            2 -> { //low to high
                filteredList.addAll(ArrayList(transporterList.sortedWith(compareBy { it.drivers[0].due })))
            }
        }
        transporterAdapter.notifyDataSetChanged()
        sortPosition = sortItemPosition
        if (transporterList.isEmpty())
            showError(getString(R.string.empty_drivers))
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(mContext))
            getMyTransporters()
        else
            showError(getString(R.string.no_internet))
    }

    private fun getMyTransporters() {
        transporterList.clear()
        transporterAdapter.notifyDataSetChanged()
        pb_progress.visibility = View.VISIBLE
        tv_recyclerMessage.visibility = View.GONE
        getDriversProfileOperation(
            ApiService.create(),
            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!, {
                if (isAdded) {
                    pb_progress.visibility = View.GONE
                    transporterList.clear()
                    transporterList.addAll(it.transporters)
                    transporterAdapter.notifyDataSetChanged()
                    if (transporterList.isEmpty())
                        showError(getString(R.string.empty_transporter))
                }
            }, {
                if (isAdded) {
                    pb_progress.visibility = View.GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            mContext, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            logoutUser(parentActivity)
                        }
                    } else
                        showError(it.second)
                }
            }
        )
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = View.VISIBLE
    }
}
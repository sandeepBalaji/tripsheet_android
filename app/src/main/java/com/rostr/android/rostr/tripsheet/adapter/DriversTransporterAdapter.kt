package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.DriversTransporter
import kotlinx.android.synthetic.main.item_driver.view.*

class DriversTransporterAdapter(
    private val context: Context,
    private val mainList: ArrayList<DriversTransporter>,
    val onItemClick: (view: View, position: Int) -> Unit
) :
    RecyclerView.Adapter<DriversTransporterAdapter.TransporterViewHolder>() {

    class TransporterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.tv_driver_item_name
        val phone: TextView = itemView.tv_driver_item_vehicleType
        val amount: TextView = itemView.tv_driver_item_amount
        val vehicleNo: TextView = itemView.tv_driver_item_vehicleNo
        val callIcon: ImageView = itemView.iv_driver_item_callIcon
        val whatsapp: ImageView = itemView.iv_driver_item_whatsappIcon
        val image: ImageView = itemView.iv_driver_item_image
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransporterViewHolder {
        return TransporterViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_driver,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TransporterViewHolder, position: Int) {
        holder.whatsapp.visibility = GONE
        holder.callIcon.visibility = GONE
        holder.vehicleNo.visibility = GONE
        val currentItem = mainList[position]
        if (currentItem.profileImage == null) {

        } else
            Glide.with(context)
                .apply { RequestOptions().centerCrop() }
                .load("${BuildConfig.SERVER_URL_IMAGE}${currentItem.profileImage}")
                .into(holder.image)
        holder.name.text = currentItem.name
        holder.phone.text = currentItem.mobileNo
        holder.itemView.setOnClickListener {
            onItemClick(it, position)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
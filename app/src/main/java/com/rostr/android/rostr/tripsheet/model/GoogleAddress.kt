package com.rostr.android.rostr.tripsheet.model

data class GoogleAddress(
    val results: List<Result>,
    val status: String,
    val error_message: String? = null
)

data class AddressComponent(
    val long_name: String,
    val short_name: String,
    val types: List<String>
)

data class Bounds(
    val northeast: GoogleLocation,
    val southwest: GoogleLocation
)

data class Viewport(
    val northeast: GoogleLocation,
    val southwest: GoogleLocation
)

data class Geometry(
    val bounds: Bounds,
    val location: GoogleLocation,
    val location_type: String,
    val viewport: Viewport
)

data class Result(
    val address_components: List<AddressComponent>,
    val formatted_address: String,
    val geometry: Geometry,
    val place_id: String,
    val types: List<String>
)

data class GoogleLocation(
    val lat: Double,
    val lng: Double
)
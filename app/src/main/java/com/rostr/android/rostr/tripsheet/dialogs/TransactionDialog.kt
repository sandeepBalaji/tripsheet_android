package com.rostr.android.rostr.tripsheet.dialogs

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.developers.imagezipper.ImageZipper
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.DateTimePickerActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.SearchTripActivity
import com.rostr.android.rostr.tripsheet.model.Data
import com.rostr.android.rostr.tripsheet.model.TransactionRequest
import com.rostr.android.rostr.tripsheet.model.TripResponse
import com.rostr.android.rostr.tripsheet.model.UserDriver
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.createTransactionOperation
import com.rostr.android.rostr.tripsheet.network.updateTransactionOperation
import com.rostr.android.rostr.tripsheet.network.uploadImageOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.fragment_transaction.*
import kotlinx.android.synthetic.main.image_picker.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

private const val DATETIME_REQUEST = 4
const val IMAGE1_REQUEST = 1
const val IMAGE2_REQUEST = 2
const val IMAGE3_REQUEST = 3
private const val GALLERY = 400
private const val CAMERA_ACTION = 200
const val SEARCH_TRIP = 300

class TransactionDialog(val onSuccess: (transactionData: Data) -> Unit) :
    BottomSheetDialogFragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var parentActivity: FragmentActivity
    private var driverData: UserDriver? = null
    private var transactionData: Data? = null
    private lateinit var selectedCalendar: Calendar
    private var currentImageRequest: Int = 0
    private var tripData: TripResponse? = null
    private lateinit var imagesArray: ArrayList<String>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity!!
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog: BottomSheetDialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog: BottomSheetDialog = it as BottomSheetDialog
            val bottomSheet: FrameLayout =
                dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)!!
            BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
            BottomSheetBehavior.from(bottomSheet).skipCollapsed = true
            BottomSheetBehavior.from(bottomSheet).isHideable = true
        }
        return bottomSheetDialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            if (arguments!!.containsKey(USER_DATA_KEY))
                driverData = arguments!!.getParcelable(USER_DATA_KEY)!!
            selectedCalendar = Calendar.getInstance()
            setDateTime()
            imagesArray = ArrayList()
            if (arguments!!.containsKey(DATA_KEY)) {
                transactionData = arguments!!.getParcelable(DATA_KEY)
                setExistingData()
            }
            tv_transaction_dateTime.setText(getFormattedDateTime(selectedCalendar))

            bt_transaction_actionButton.setOnClickListener(this)
            tv_transaction_dateTime.setOnClickListener(this)
            iv_transaction_image1.setOnClickListener(this)
            iv_transaction_image2.setOnClickListener(this)
            iv_transaction_image3.setOnClickListener(this)
            tv_transaction_trip.setOnClickListener(this)
        }
    }

    private fun setExistingData() {
        bt_transaction_actionButton.text = getString(R.string.update)
        et_transaction_name.value = transactionData?.purpose
        et_transaction_remarks.value = transactionData?.remarks
        et_transaction_amount.value = transactionData?.total?.replace(Regex("-"), "")
        selectedCalendar.timeInMillis = transactionData?.date!!
        setDateTime()
        val checkedType: RadioButton =
            rg_transaction_paymentType.findViewWithTag(transactionData?.paymentStatus)
        rg_transaction_paymentType.check(checkedType.id)
        val transactionImages = transactionData?.images
        if (transactionImages != null && transactionImages.isNotEmpty()) {
            imagesArray.add(transactionImages[0])
            Glide.with(mContext).load(BuildConfig.SERVER_URL_IMAGE + transactionImages[0])
                .into(iv_transaction_image1)
        }
        if (transactionImages != null && transactionImages.size >= 2) {
            imagesArray.add(transactionImages[1])
            Glide.with(mContext).load(BuildConfig.SERVER_URL_IMAGE + transactionImages[1])
                .into(iv_transaction_image2)
        }
        if (transactionImages != null && transactionImages.size > 2) {
            imagesArray.add(transactionImages[2])
            Glide.with(mContext).load(BuildConfig.SERVER_URL_IMAGE + transactionImages[2])
                .into(iv_transaction_image3)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_transaction_trip -> {
                val intent = Intent(mContext, SearchTripActivity::class.java)
                intent.putExtra(DATA_KEY, driverData?.driver?._id)
                startActivityForResult(intent, SEARCH_TRIP)
            }

            R.id.bt_transaction_actionButton -> {
                if (et_transaction_name.isEmpty) {
                    showShortToast(mContext, "Please enter the title")
                    return
                }
                if (isConnectedToInternet(mContext)) {
                    val paymentType =
                        rg_transaction_paymentType.findViewById<RadioButton>(
                            rg_transaction_paymentType.checkedRadioButtonId
                        ).tag.toString()
                    val request = TransactionRequest(
                        purpose = et_transaction_name.value,
                        remarks = et_transaction_remarks.value,
                        driver = if (driverData != null) driverData?.driver?._id else null,
                        trip = if (tripData != null) tripData?._id else null,
                        paymentStatus = paymentType,
                        date = selectedCalendar.timeInMillis,
                        total = getTotal(paymentType),
                        images = imagesArray
                    )
                    if (bt_transaction_actionButton.text == getString(R.string.update)) {
                        updateTransactionOperation(ApiService.create(),
                            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                            request,
                            transactionData?._id!!, {
                                showAlertDialog(
                                    mContext,
                                    null,
                                    "Transaction updated successfully!"
                                ) {
                                    onSuccess(it)
                                    dismiss()
                                }
                            }, {
                                showAlertDialog(mContext, null, it.second)
                            })
                    } else {
                        createTransactionOperation(ApiService.create(),
                            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                            request, {
                                showAlertDialog(mContext, null, "Transaction added successfully!") {
                                    onSuccess(it)
                                    dismiss()
                                }
                            }, {
                                showAlertDialog(mContext, null, it.second)
                            })
                    }
                } else
                    showAlertDialog(
                        mContext,
                        getString(R.string.no_internet_label),
                        getString(R.string.no_internet)
                    )
            }

            R.id.tv_transaction_dateTime -> {
                val dateIntent = Intent(mContext, DateTimePickerActivity::class.java)
                dateIntent.putExtra(DATE_KEY, selectedCalendar.timeInMillis)
                startActivityForResult(dateIntent, DATETIME_REQUEST)
            }

            R.id.iv_transaction_image1 -> {
                currentImageRequest = IMAGE1_REQUEST
                checkStoragePermission()
            }
            R.id.iv_transaction_image2 -> {
                currentImageRequest = IMAGE2_REQUEST
                checkStoragePermission()
            }
            R.id.iv_transaction_image3 -> {
                currentImageRequest = IMAGE3_REQUEST
                checkStoragePermission()
            }
        }
    }

    private fun getTotal(paymentType: String): String? {
        return if (et_transaction_amount.isEmpty) {
            null
        } else {
            return if (paymentType == "due")
                "-${et_transaction_amount.double}"
            else
                et_transaction_amount.value
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK) {
            showLog("$requestCode")
            when (requestCode) {
                DATETIME_REQUEST -> {
                    if (data?.extras != null && data.hasExtra(DATE_KEY)) {
                        val calendar = Calendar.getInstance()
                        calendar.timeInMillis = data.getLongExtra(DATE_KEY, 0)
                        selectedCalendar = calendar
                        setDateTime()
                    }
                }
                GALLERY -> {
                    showLog("gallery request called")
                    if (data != null) {
                        val selectedImage: Uri? = data.data
                        showLog("$selectedImage")
                        val filePath = arrayOf(MediaStore.Images.Media.DATA)
                        if (parentActivity.contentResolver != null && selectedImage != null) {
                            val c = parentActivity.contentResolver.query(
                                selectedImage,
                                filePath,
                                null,
                                null,
                                null
                            )
                            if (c != null) {
                                c.moveToFirst()
                                val columnIndex = c.getColumnIndex(filePath[0])
                                val imagePath = c.getString(columnIndex)
                                c.close()
                                val thumbnail = BitmapFactory.decodeFile(imagePath)
                                showLog("path of image from gallery......******************.........$imagePath")
                                val compressedFile =
                                    ImageZipper(mContext).compressToFile(File(imagePath))
                                when (currentImageRequest) {
                                    IMAGE1_REQUEST -> {
                                        Glide.with(mContext).load(thumbnail).apply(
                                            RequestOptions()
                                                .centerCrop()
                                                .error(R.drawable.ic_add_photo)
                                        ).into(iv_transaction_image1)
                                        uploadImage(compressedFile, 0)
                                    }
                                    IMAGE2_REQUEST -> {
                                        Glide.with(mContext).load(thumbnail).apply(
                                            RequestOptions()
                                                .centerCrop()
                                                .error(R.drawable.ic_add_photo)
                                        ).into(iv_transaction_image2)
                                        uploadImage(compressedFile, 1)
                                    }
                                    IMAGE3_REQUEST -> {
                                        Glide.with(mContext).load(thumbnail).apply(
                                            RequestOptions()
                                                .centerCrop()
                                                .error(R.drawable.ic_add_photo)
                                        ).into(iv_transaction_image3)
                                        uploadImage(compressedFile, 2)
                                    }
                                }
                            }
                        }
                    } else
                        showLog("null data")
                }

                CAMERA_ACTION -> {
                    showLog("camera action called")
                    var f = File(Environment.getExternalStorageDirectory().toString())
                    for (temp: File in f.listFiles()) {
                        if (temp.name == "$currentImageRequest.jpg") {
                            f = temp
                            break
                        }
                    }
                    try {
                        val bitmapOptions: BitmapFactory.Options = BitmapFactory.Options()
                        val bitmap = BitmapFactory.decodeFile(
                            f.absolutePath,
                            bitmapOptions
                        )
                        when (currentImageRequest) {
                            IMAGE1_REQUEST -> {
                                Glide.with(mContext).load(bitmap).apply(
                                    RequestOptions()
                                        .centerCrop()
                                        .error(R.drawable.ic_add_photo)
                                ).into(iv_transaction_image1)
                            }
                            IMAGE2_REQUEST ->
                                Glide.with(mContext).load(bitmap).apply(
                                    RequestOptions()
                                        .centerCrop()
                                        .error(R.drawable.ic_add_photo)
                                ).into(iv_transaction_image2)
                            IMAGE3_REQUEST ->
                                Glide.with(mContext).load(bitmap).apply(
                                    RequestOptions()
                                        .centerCrop()
                                        .error(R.drawable.ic_add_photo)
                                ).into(iv_transaction_image3)
                        }

                        val parentPath =
                            "${Environment.getExternalStorageDirectory()}" +
                                    "${File.separator}${getString(R.string.app_name)}"
                        if (f.delete())
                            showLog("temp file deleted")
                        val parentFile = File(parentPath)
                        if (!parentFile.exists()) {
                            if (parentFile.mkdirs())
                                writeToNewFile(parentPath, bitmap)
                            else
                                showAlertDialog(
                                    mContext,
                                    getString(R.string.oops),
                                    "Could not create the directory"
                                )
                        } else
                            writeToNewFile(parentPath, bitmap)
                    } catch (e: Exception) {
                        showLog(e)
                    }
                }
                SEARCH_TRIP -> {
                    if (data != null) {
                        tripData = data.getParcelableExtra(DATA_KEY)
                        tv_transaction_trip.setText(tripData?.tripName)
                    }
                }
            }
        }
    }

    private fun writeToNewFile(parentPath: String, bitmap: Bitmap) {
        val file = File(parentPath, "${System.currentTimeMillis()}.jpg")
        try {
            val outFile = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile)
            outFile.flush()
            outFile.close()
            val imagePath = file.absolutePath
            showLog("Image path $imagePath")
            val compressedFile = ImageZipper(mContext).compressToFile(file)
            when (currentImageRequest) {
                IMAGE1_REQUEST -> {
                    uploadImage(compressedFile, 0)
                }
                IMAGE2_REQUEST -> {
                    uploadImage(compressedFile, 1)
                }
                IMAGE3_REQUEST -> {
                    uploadImage(compressedFile, 2)
                }
            }
        } catch (e: FileNotFoundException) {
            showLog(e)
        } catch (e: IOException) {
            showLog(e)
        } catch (e: Exception) {
            showLog(e)
        }
    }

    private fun setDateTime() {
        tv_transaction_dateTime.setText(getFormattedDateTime(selectedCalendar))
    }

    private fun checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(parentActivity)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        showImageDialog()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        showShortToast(
                            mContext,
                            getString(R.string.grant_image_permission)
                        )
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest,
                        token: PermissionToken
                    ) {
                        showAlertDialog(mContext,
                            getString(R.string.permission_required_title),
                            getString(R.string.grant_image_permission),
                            "Yes",
                            "No", {
                                token.continuePermissionRequest()
                            }, {
                                token.cancelPermissionRequest()
                            })
                    }
                }).check()
        } else {
            showImageDialog()
        }
    }

    private fun showImageDialog() {
        val dialog = BottomSheetDialog(mContext)
        val dialogView = layoutInflater.inflate(R.layout.image_picker, null)
        dialogView.tv_image_picker_camera.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Dexter.withActivity(parentActivity)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(object : PermissionListener {
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            initCamera()
                            dialog.dismiss()
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permission: PermissionRequest?,
                            token: PermissionToken?
                        ) {
                            showAlertDialog(mContext, getString(R.string.permission_required_title),
                                getString(R.string.grant_camera_permission),
                                "Ok", "Cancel", {
                                    token?.continuePermissionRequest()
                                }, {
                                    token?.cancelPermissionRequest()
                                }
                            )
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            showShortToast(mContext, getString(R.string.grant_camera_permission))
                        }
                    }).check()
            } else {
                initCamera()
                dialog.dismiss()
            }
        }

        dialogView.tv_image_picker_gallery.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(intent, GALLERY)
            dialog.dismiss()
        }

        dialogView.tv_image_picker_cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun initCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val f = File(Environment.getExternalStorageDirectory(), "$currentImageRequest.jpg")
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getFileUri(f))
        startActivityForResult(intent, CAMERA_ACTION)
    }

    private fun getFileUri(mediaFile: File): Uri {
        return FileProvider.getUriForFile(
            mContext,
            BuildConfig.APPLICATION_ID + ".provider",
            mediaFile
        )
    }


    private fun uploadImage(file: File, position: Int) {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("introImage", file.name, requestFile)
        val progress = ProgressDialog.progressDialog(mContext, "please wait...")
        progress.show()
        progress.setCanceledOnTouchOutside(false)
        uploadImageOperation(ApiService.create(),
            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
            body, {
                try {
                    progress.dismiss()
                } catch (e: Exception) {
                }
                imagesArray.add(position, it.imageUrl)
            }, {
                try {
                    progress.dismiss()
                } catch (e: Exception) {
                }
                if (it.first == 401) {
                    showAlertDialog(
                        mContext, getString(R.string.unauthorized_title),
                        getString(R.string.logged_out)
                    ) {
                        logoutUser(parentActivity)
                    }
                } else
                    showAlertDialog(mContext, getString(R.string.oops), it.second)
            })
    }
}
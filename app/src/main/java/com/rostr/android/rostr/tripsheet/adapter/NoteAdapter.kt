package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.TripNoteResponse
import com.rostr.android.rostr.tripsheet.utils.getFormattedDateTime
import com.rostr.android.rostr.tripsheet.utils.showLog
import kotlinx.android.synthetic.main.item_note.view.*
import java.util.*

class NoteAdapter(
    private val mContext: Context,
    private val mainList: ArrayList<TripNoteResponse>?,
    val onItemClick: ((view: View, position: Int) -> Unit)?
) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val author: TextView = itemView.tv_note_item_author
        val note: TextView = itemView.tv_note_item_note
        val date: TextView = itemView.tv_note_item_publishedAt
        val image: ImageView = itemView.iv_note_item_image
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.item_note,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        try {
            val currentItem = mainList!![position]
            holder.author.text = currentItem.by.name
            holder.note.text = currentItem.notes
            val cal = Calendar.getInstance()
            cal.timeInMillis = currentItem.date
            holder.date.text = getFormattedDateTime(cal)
            if (currentItem.images != null && currentItem.images.isNotEmpty())
                Glide.with(mContext)
                    .load(BuildConfig.SERVER_URL_IMAGE + currentItem.images[0])
                    .apply(RequestOptions()
                        .centerCrop()
                        .error(R.drawable.ic_add_photo))
                    .into(holder.image)
            if (onItemClick != null)
                holder.itemView.setOnClickListener {
                    onItemClick.invoke(it, position)
                }
        } catch (ex: Exception) {
            showLog(ex.localizedMessage)
        }
    }

    override fun getItemCount(): Int {
        if (mainList == null)
            return 0
        else
            return mainList.size
//        return 5
    }
}
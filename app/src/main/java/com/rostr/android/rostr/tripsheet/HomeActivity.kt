package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat.START
import androidx.core.view.get
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.dialogs.SupportDialog
import com.rostr.android.rostr.tripsheet.fragments.driverHomeFragments.DriverTripsFragment
import com.rostr.android.rostr.tripsheet.fragments.driverHomeFragments.TransporterFragment
import com.rostr.android.rostr.tripsheet.fragments.transporterHomeFragments.CustomersFragment
import com.rostr.android.rostr.tripsheet.fragments.transporterHomeFragments.DriversFragment
import com.rostr.android.rostr.tripsheet.fragments.transporterHomeFragments.TripsFragment
import com.rostr.android.rostr.tripsheet.model.User
import com.rostr.android.rostr.tripsheet.model.UserSingleton
import com.rostr.android.rostr.tripsheet.utils.*
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*


class HomeActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private lateinit var userData: User
    private var isCloseApp: Boolean = false
    private lateinit var mHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)

            tv_toolbar_title.text = getString(R.string.app_name)
            mHandler = Handler()
            initSideMenu()
            bnv_home_menu.itemIconTintList = null
            if (PreferenceUtils(this).getString(ROLE_KEY) == "driver") {
                bnv_home_menu.inflateMenu(R.menu.menu_bottom_driver)
                replaceFragment(TransporterFragment(), 0)
                fam_home_menu.visibility = GONE
            } else {
                bnv_home_menu.inflateMenu(R.menu.menu_bottom_transporter)
                replaceFragment(DriversFragment(), 0)
                fam_home_menu.visibility = VISIBLE
                fam_home_menu.setClosedOnTouchOutside(true)
            }
            initClickListeners()
        }
    }

    private fun initClickListeners() {
        bnv_home_menu.setOnNavigationItemSelectedListener(this)
        fab_add_trip.setOnClickListener(this)
        fab_add_customer.setOnClickListener(this)
        fab_add_driver.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab_add_customer -> {
                fam_home_menu.close(true)
                startActivity(Intent(this, AddCustomerActivity::class.java))
            }

            R.id.fab_add_trip -> {
                fam_home_menu.close(true)
                startActivity(Intent(this, AddTripActivity::class.java))
            }

            R.id.fab_add_driver -> {
                fam_home_menu.close(true)
                startActivity(Intent(this, ContactListActivity::class.java))
            }
        }
    }

    private fun initSideMenu() {
        if (UserSingleton.instance != null) {
            val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.drawer_open,
                R.string.drawer_close
            )

            toggle.drawerArrowDrawable.color = ContextCompat.getColor(this, R.color.white)
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()
            val navHeader = nav_view.getHeaderView(0)
            userData = UserSingleton.instance!!
            val userName: TextView = navHeader.findViewById(R.id.tv_header_userName)
            val userAvatar: CircleImageView = navHeader.findViewById(R.id.iv_header_avatar)
            userName.text = userData.name
            val role = PreferenceUtils(this).getString(ROLE_KEY)
            if (role == "transporter") {
                nav_view.inflateMenu(R.menu.menu_main_transporter)
                if (userData.transporter?.profileImage != null) {
                    Glide.with(this)
                        .load("${BuildConfig.SERVER_URL_IMAGE}${userData.transporter?.profileImage}")
                        .into(userAvatar)
                } else {
                    if (userData.gender == "Male")
                        userAvatar.setImageResource(R.drawable.ic_avatar_male)
                    else
                        userAvatar.setImageResource(R.drawable.ic_avatar_female)
                }
            } else if (role == "driver") {
                nav_view.inflateMenu(R.menu.menu_main_driver)
                if (userData.gender == "Male")
                    userAvatar.setImageResource(R.drawable.ic_avatar_male)
                else
                    userAvatar.setImageResource(R.drawable.ic_avatar_female)
            }
            navHeader.setOnClickListener {
                if (role == "driver") {
                    //TODO: driver profile open
                } else {
                    startActivity(Intent(this, TransporterProfileActivity::class.java))
                }
                toggleDrawer()
            }
            nav_view.setNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.nav_logout -> {
                        showAlertDialog(this, null,
                            "Are you sure you want to logout?",
                            "Yes", "No", {
                                logoutUser(this)
                            }, {
                                // do nothing just dismisses the dialog
                            })
                    }

                    R.id.nav_report -> {
                        startActivity(Intent(this, ReportActivity::class.java))
                    }

                    R.id.nav_settings -> {
                        startActivity(Intent(this, SettingsActivity::class.java))
                    }

                    R.id.nav_privacy -> {
                        val intent = Intent(this, WebActivity::class.java)
                        intent.putExtra(DATA_KEY, "http://www.tripsheet.in/terms")
                        startActivity(intent)
                    }

                    R.id.nav_support -> {
                        val dialogFragment = SupportDialog()
                        val bundle = Bundle()
                        bundle.putBoolean("notAlertDialog", true)
                        dialogFragment.arguments = bundle
                        val ft = supportFragmentManager.beginTransaction()
                        val prev = supportFragmentManager.findFragmentByTag("dialog")
                        if (prev != null) {
                            ft.remove(prev)
                        }
                        ft.addToBackStack(null)
                        dialogFragment.show(ft, "dialog")
                    }

                    R.id.nav_invite -> {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                Dexter.withActivity(this@HomeActivity)
                                    .withPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    .withListener(object : PermissionListener {
                                        override fun onPermissionGranted(response: PermissionGrantedResponse) {
                                            inviteUser()
                                        }

                                        override fun onPermissionDenied(response: PermissionDeniedResponse) {
                                            showShortToast(
                                                this@HomeActivity,
                                                getString(R.string.no_storage_permission)
                                            )
                                        }

                                        override fun onPermissionRationaleShouldBeShown(
                                            permission: PermissionRequest,
                                            token: PermissionToken
                                        ) {
                                            token.continuePermissionRequest()
                                        }
                                    })
                                    .check()
                            } else {
                                inviteUser()
                            }
                        } catch (ex: Exception) {
                            showLog(ex)
                        }
                    }
                }
                toggleDrawer()
                true
            }
        } else {
            // if app crashes or does not have user data then app restarts from splash screen
            val intent = Intent(this, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    private fun inviteUser() {
        Glide.with(this)
            .asBitmap()
            .load(R.drawable.invite_image)
            .into(object : CustomTarget<Bitmap>() {
                override fun onLoadCleared(placeholder: Drawable?) {
                    // this is called when imageView is cleared on lifecycle call or for
                    // some other reason.
                    // if you are referencing the bitmap somewhere else too other than this imageView
                    // clear it here as you can no longer have the bitmap
                }

                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    val imgBitmapPath = MediaStore.Images.Media.insertImage(
                        contentResolver, resource,
                        "shareRostr", null
                    )
                    val imgBitmapUri = Uri.parse(imgBitmapPath)
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "image/*"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.invite_message))
                    shareIntent.putExtra(Intent.EXTRA_STREAM, imgBitmapUri)
                    startActivity(Intent.createChooser(shareIntent, "Invite using..."))
                }
            })
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (!bnv_home_menu.menu.findItem(item.itemId).isChecked) {
            showLog("new menu")
            when (item.itemId) {
                R.id.nav_drivers -> {
                    replaceFragment(DriversFragment(), 0)
                }

                R.id.nav_trips -> {
                    replaceFragment(TripsFragment(), 1)
                }

                R.id.nav_customers -> {
                    replaceFragment(CustomersFragment(), 2)
                }

                R.id.nav_transporters -> {
                    replaceFragment(TransporterFragment(), 0)
                }

                R.id.nav_driver_trips -> {
                    replaceFragment(DriverTripsFragment(), 1)
                }
            }
        }
        return true
    }

    private fun toggleDrawer() {
        if (drawerLayout.isDrawerOpen(START)) {
            drawerLayout.closeDrawer(START)
        } else {
            drawerLayout.openDrawer(START)
        }
    }

    override fun onBackPressed() {
        if (!isCloseApp) {
            showLog("role ${PreferenceUtils(this).getString(ROLE_KEY)}")
            if (PreferenceUtils(this).getString(ROLE_KEY) == "transporter") {
                if (supportFragmentManager.findFragmentById(R.id.fl_home_container) is DriversFragment) {
                    isCloseApp = true
                    showShortToast(this, getString(R.string.press_again))
                } else {
                    replaceFragment(DriversFragment(), 0)
                    bnv_home_menu.menu[0].isChecked = true
                }
            } else {
                if (supportFragmentManager.findFragmentById(R.id.fl_home_container) is TransporterFragment) {
                    isCloseApp = true
                    showShortToast(this, getString(R.string.press_again))
                } else {
                    replaceFragment(TransporterFragment(), 0)
                    bnv_home_menu.menu[0].isChecked = true
                }
            }
        } else
            super.onBackPressed()
    }

    private fun replaceFragment(fragment: Fragment, position: Int) {
        bnv_home_menu.menu[position].isChecked = true
/*val mPendingRunnable = {
// update the main content by replacing fragments
val fragmentTransaction = supportFragmentManager
    .beginTransaction()
    .replace(R.id.fl_home_container, fragment)
fragmentTransaction.commitAllowingStateLoss()
}
// If mPendingRunnable is not null, then add to the message queue
try {
mHandler.post(mPendingRunnable as Runnable)
} catch (ex: Exception) {
ex.printStackTrace()
}*/
        val fragmentTransaction = supportFragmentManager
            .beginTransaction()
            .replace(R.id.fl_home_container, fragment)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_toolbar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.nav_notifications -> {

            }

            /*R.id.nav_pendingRequests -> {
                startActivity(Intent(this, MyProfileActivity::class.java))
            }*/
        }
        return super.onOptionsItemSelected(item)
    }
}
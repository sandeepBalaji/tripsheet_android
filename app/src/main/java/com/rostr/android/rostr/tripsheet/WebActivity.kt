package com.rostr.android.rostr.tripsheet

import android.os.Bundle
import android.view.MenuItem
import android.view.View.GONE
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.rostr.android.rostr.tripsheet.utils.DATA_KEY
import com.rostr.android.rostr.tripsheet.utils.showShortToast
import kotlinx.android.synthetic.main.activity_web.*
import kotlinx.android.synthetic.main.recycler_layout.*
import kotlinx.android.synthetic.main.toolbar.toolbar

class WebActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Privacy Policy"

        val url = intent.getStringExtra(DATA_KEY)
        wv_webView.settings.javaScriptEnabled = true // enable javascript
        wv_webView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        wv_webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                pb_web_progress.visibility = GONE
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                super.onReceivedError(view, request, error)
                pb_web_progress.visibility = GONE
                showShortToast(this@WebActivity, getString(R.string.request_failure))
            }
        }
        wv_webView.loadUrl(url)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
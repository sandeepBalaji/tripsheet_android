package com.rostr.android.rostr.tripsheet

import android.Manifest
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.developers.imagezipper.ImageZipper
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.rostr.android.rostr.tripsheet.dialogs.ProgressDialog
import com.rostr.android.rostr.tripsheet.model.AccountInfo
import com.rostr.android.rostr.tripsheet.model.Address
import com.rostr.android.rostr.tripsheet.model.UpdateTransporterRequest
import com.rostr.android.rostr.tripsheet.model.UserSingleton
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.updateTransporterProfileOperation
import com.rostr.android.rostr.tripsheet.network.uploadImageOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.dialog_add_note.*
import kotlinx.android.synthetic.main.image_picker.view.*
import kotlinx.android.synthetic.main.toolbar.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

private const val GALLERY = 100
private const val CAMERA_ACTION = 200

class TransporterProfileActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var lastSelectedProfileRadio: RadioButton
    private var userImagePath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            tv_toolbar_title.text = getString(R.string.profile)
            setData()
            bt_profile_saveButton.setOnClickListener(this)
            iv_profile_userImage.setOnClickListener(this)
            iv_profile_editImageIcon.setOnClickListener(this)
        }
    }

    private fun setData() {
        val user = UserSingleton.instance
        et_profile_name.value = user?.name
        et_profile_emailId.value = user?.email
        et_profile_phoneNo.value = user?.mobileNo
        if (user?.transporter?.profileImage.isNullOrEmpty()) {
            if (user?.gender == "Male")
                Glide.with(this).load(R.drawable.ic_avatar_male).into(iv_profile_userImage)
            else
                Glide.with(this).load(R.drawable.ic_avatar_female).into(iv_profile_userImage)
        } else
            Glide.with(this)
                .load("${BuildConfig.SERVER_URL_IMAGE}${user?.transporter?.profileImage}")
                .into(iv_profile_userImage)
        et_profile_businessName.value = user?.transporter?.companyName
        et_profile_businessAddress.value = user?.transporter?.address?.addressLine
        et_profile_pincode.value = user?.transporter?.address?.pincode
        et_profile_gstNo.value = user?.transporter?.gstIN
        et_profile_beneficiaryName.value = user?.transporter?.accountInfo?.name
        et_profile_ifsc.value = user?.transporter?.accountInfo?.IFSC
        et_profile_accountNo.value = user?.transporter?.accountInfo?.accountNumber
        if (user?.transporter?.accountInfo?.accountType == getString(R.string.savings))
            rg_profile_accountType.check(R.id.rb_savings)
        else
            rg_profile_accountType.check(R.id.rb_current)
        et_profile_confirmAccountNo.value = user?.transporter?.accountInfo?.accountNumber
    }

    private fun restartHomeActivity() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_profile_saveButton -> {
                if (et_profile_beneficiaryName.isEmpty || et_profile_ifsc.isEmpty || et_profile_accountNo.isEmpty) {
                    showShortToast(this, "Please enter all the bank account details")
                } else if (et_profile_accountNo.value != et_profile_confirmAccountNo.value)
                    showShortToast(this, "Your account number does not match")
                else {
                    if (isConnectedToInternet(this)) {
                        updateTransporterProfileOperation(ApiService.create(),
                            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                            getRequest(), {
                                UserSingleton.instance?.transporter = it
                                showAlertDialog(
                                    this,
                                    null,
                                    "Your profile has been successfully updated!"
                                )
                            }, {
                                if (it.first == 401) {
                                    showAlertDialog(
                                        this, getString(R.string.unauthorized_title),
                                        getString(R.string.logged_out)
                                    ) {
                                        PreferenceUtils(this).clearData()
                                        val intent = Intent(this, SplashActivity::class.java)
                                        intent.flags =
                                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        startActivity(intent)
                                    }
                                } else
                                    showAlertDialog(this, getString(R.string.oops), it.second)
                            })
                    } else {
                        showAlertDialog(
                            this,
                            getString(R.string.no_internet_label),
                            getString(R.string.no_internet)
                        )
                    }
                }
            }

            R.id.iv_profile_editImageIcon, R.id.iv_profile_userImage -> {
                checkStoragePermission()
            }
        }
    }

    private fun checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        showImageDialog()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        showShortToast(
                            this@TransporterProfileActivity,
                            getString(R.string.grant_image_permission)
                        )
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest,
                        token: PermissionToken
                    ) {
                        showAlertDialog(this@TransporterProfileActivity,
                            getString(R.string.permission_required_title),
                            getString(R.string.grant_image_permission),
                            "Yes",
                            "No", {
                                token.continuePermissionRequest()
                            }, {
                                token.cancelPermissionRequest()
                            })
                    }
                }).check()
        } else {
            showImageDialog()
        }
    }

    private fun showImageDialog() {
        val dialog = BottomSheetDialog(this)
        val dialogView = layoutInflater.inflate(R.layout.image_picker, null)
        dialogView.tv_image_picker_camera.setOnClickListener {
            showLog("Camera clicked")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Dexter.withActivity(this)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(object : PermissionListener {
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            showLog("Permission granted")
                            initCamera()
                            dialog.dismiss()
                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permission: PermissionRequest?,
                            token: PermissionToken?
                        ) {
                            showAlertDialog(this@TransporterProfileActivity,
                                getString(R.string.permission_required_title),
                                getString(R.string.grant_camera_permission),
                                "Ok",
                                "Cancel",
                                {
                                    token?.continuePermissionRequest()
                                },
                                {
                                    token?.cancelPermissionRequest()
                                }
                            )
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            showShortToast(
                                this@TransporterProfileActivity,
                                getString(R.string.grant_camera_permission)
                            )
                        }
                    }).check()
            } else {
                initCamera()
                dialog.dismiss()
            }
        }

        dialogView.tv_image_picker_gallery.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            startActivityForResult(intent, GALLERY)
            dialog.dismiss()
        }
        dialogView.tv_image_picker_cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    private fun initCamera() {
        showLog("Camera init")
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val f = File(Environment.getExternalStorageDirectory(), "temp.jpg")
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getFileUri(f))
        startActivityForResult(intent, CAMERA_ACTION)
    }

    private fun getFileUri(mediaFile: File): Uri {
        return FileProvider.getUriForFile(
            this,
            BuildConfig.APPLICATION_ID + ".provider",
            mediaFile
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                CAMERA_ACTION -> {
                    var f = File(Environment.getExternalStorageDirectory().toString())
                    for (temp: File in f.listFiles()) {
                        if (temp.name == "temp.jpg") {
                            f = temp
                            break
                        }
                    }
                    try {
                        val bitmapOptions: BitmapFactory.Options = BitmapFactory.Options()
                        val bitmap = BitmapFactory.decodeFile(
                            f.absolutePath,
                            bitmapOptions
                        )
                        Glide.with(this).load(bitmap).apply(
                            RequestOptions()
                                .centerCrop()
                                .error(R.drawable.ic_add_photo)
                        ).into(iv_add_note_image)

                        val parentPath =
                            "${Environment.getExternalStorageDirectory()}" +
                                    "${File.separator}${getString(R.string.app_name)}"
                        if (f.delete())
                            showLog("temp file deleted")
                        val parentFile = File(parentPath)
                        if (!parentFile.exists()) {
                            if (parentFile.mkdirs())
                                writeToNewFile(parentPath, bitmap)
                            else
                                showAlertDialog(
                                    this,
                                    getString(R.string.oops),
                                    "Could not create the directory"
                                )
                        } else
                            writeToNewFile(parentPath, bitmap)
                    } catch (e: Exception) {
                        showLog(e)
                    }
                }

                GALLERY -> {
                    if (data != null) {
                        try {
                            val selectedImage: Uri? = data.data
                            val filePath = arrayOf(MediaStore.Images.Media.DATA)
                            if (contentResolver != null && selectedImage != null) {
                                val c = contentResolver.query(
                                    selectedImage,
                                    filePath,
                                    null,
                                    null,
                                    null
                                )
                                if (c != null) {
                                    c.moveToFirst()
                                    val columnIndex = c.getColumnIndex(filePath[0])
                                    val imagePath = c.getString(columnIndex)
                                    showLog("filepath $imagePath")
                                    c.close()
                                    val thumbnail = BitmapFactory.decodeFile(imagePath)
                                    Glide.with(this).load(thumbnail).apply(
                                        RequestOptions()
                                            .centerCrop()
                                            .error(R.drawable.ic_add_photo)
                                    ).into(iv_profile_userImage)
                                    showLog("path of image from gallery......******************.........$imagePath")
                                    val compressedFile =
                                        ImageZipper(this).compressToFile(File(imagePath))
                                    uploadImage(compressedFile)
                                }
                            }
                        } catch (ex: Exception) {
                            showLog(ex)
                            showShortToast(this, ex.localizedMessage)
                        }
                    }
                }
            }
        }
    }

    private fun writeToNewFile(parentPath: String, bitmap: Bitmap) {
        val file = File(parentPath, "${System.currentTimeMillis()}.jpg")
        try {
            val outFile = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile)
            outFile.flush()
            outFile.close()
            val imagePath = file.absolutePath
            showLog("Image path $imagePath")
            val compressedFile = ImageZipper(this).compressToFile(file)
            uploadImage(compressedFile)
        } catch (e: FileNotFoundException) {
            showLog(e)
        } catch (e: IOException) {
            showLog(e)
        } catch (e: Exception) {
            showLog(e)
        }
    }

    private fun uploadImage(file: File) {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("introImage", file.name, requestFile)
        val progress = ProgressDialog.progressDialog(this, "please wait...")
        progress.show()
        progress.setCanceledOnTouchOutside(false)
        uploadImageOperation(ApiService.create(),
            PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
            body, {
                showLog("success")
                progress.dismiss()
                userImagePath = it.imageUrl
                UserSingleton.instance?.transporter?.profileImage = it.imageUrl
            }, {
                showLog("error uploading image: ${it.second}")
                progress.dismiss()
                if (it.first == 401) {
                    showAlertDialog(
                        this, getString(R.string.unauthorized_title),
                        getString(R.string.logged_out)
                    ) {
                        logoutUser(this)
                    }
                } else
                    showAlertDialog(this, getString(R.string.oops), it.second)
            })
    }

    private fun getRequest(): UpdateTransporterRequest {
        val accountType =
            rg_profile_accountType.findViewById<RadioButton>(rg_profile_accountType.checkedRadioButtonId)
        return UpdateTransporterRequest(
            name = et_profile_name.value,
            email = et_profile_emailId.value,
            profileImage = userImagePath,
            companyName = et_profile_businessName.value,
            address = Address(
                pincode = et_profile_pincode.value,
                addressLine = et_profile_businessAddress.value
            ),
            gstIN = et_profile_gstNo.value,
            accountInfo = AccountInfo(
                name = et_profile_beneficiaryName.value,
                IFSC = et_profile_ifsc.value,
                accountNumber = et_profile_accountNo.value,
                accountType = if (et_profile_beneficiaryName.isEmpty) null else accountType.tag.toString()
            )
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
/*val currentRole = rg_profileType.findViewWithTag<RadioButton>(
    PreferenceUtils(this).getString(
        ROLE_KEY
    )
)
rg_profileType.check(currentRole.id)*/
//            lastSelectedProfileRadio = rg_profileType.findViewById(currentRole.id)
/*rg_profileType.setOnCheckedChangeListener { radioGroup, checkedId ->
    val radioButton: RadioButton = radioGroup.findViewById(checkedId)
    if (radioButton.tag.toString() != PreferenceUtils(this).getString(ROLE_KEY)) {
        showAlertDialog(this, null,
            "Are you sure you want to switch your profile to ${radioButton.text}?",
            "Yes", "No", {
                val userData = UserSingleton.instance
                if (isConnectedToInternet(this)) {
                    if (radioButton.tag.toString() == "driver") { // changing to driver
                        if (userData?.driver != null) {
                            switchProfileOperation(ApiService.create(),
                                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                SwitchProfileRequest(userData.driver._id, null), {
                                    PreferenceUtils(this).setData(
                                        ROLE_KEY,
                                        radioButton.tag.toString()
                                    )
                                    showAlertDialog(
                                        this, null,
                                        "You have successfully switched your profile to ${radioButton.text}"
                                    ) {
                                        restartHomeActivity()
                                    }
                                }, {
                                    lastSelectedProfileRadio.isChecked = true
                                    showAlertDialog(
                                        this,
                                        getString(R.string.oops),
                                        it.second
                                    )
                                })
                        } else {
                            alternateSignInOperation(ApiService.create(),
                                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                getRequest("driver"),
                                userData!!._id, {
                                    PreferenceUtils(this).setData(
                                        ROLE_KEY,
                                        radioButton.tag.toString()
                                    )
                                    showAlertDialog(
                                        this, null,
                                        "You have successfully created your ${radioButton.text} profile"
                                    ) {
                                        UserSingleton.instance = it
                                        restartHomeActivity()
                                    }
                                }, {
                                    lastSelectedProfileRadio.isChecked = true
                                    showAlertDialog(
                                        this,
                                        getString(R.string.oops),
                                        it.second
                                    )
                                })
                        }
                    } else if (radioButton.tag.toString() == "transporter") { // changing to transporter
                        if (userData?.transporter != null) {
                            switchProfileOperation(ApiService.create(),
                                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                SwitchProfileRequest(null, userData.transporter?._id), {
                                    PreferenceUtils(this).setData(
                                        ROLE_KEY,
                                        radioButton.tag.toString()
                                    )
                                    showAlertDialog(
                                        this, null,
                                        "You have successfully switched your profile to ${radioButton.text}"
                                    ) {
                                        restartHomeActivity()
                                    }
                                }, {
                                    lastSelectedProfileRadio.isChecked = true
                                    showAlertDialog(
                                        this,
                                        getString(R.string.oops),
                                        it.second
                                    )
                                })
                        } else {
                            alternateSignInOperation(ApiService.create(),
                                PreferenceUtils(this).getString(ACCESS_TOKEN)!!,
                                getRequest("transporter"),
                                userData!!._id, {
                                    PreferenceUtils(this).setData(
                                        ROLE_KEY,
                                        radioButton.tag.toString()
                                    )
                                    showAlertDialog(
                                        this, null,
                                        "You have successfully created your ${radioButton.text} profile"
                                    ) {
                                        UserSingleton.instance = it
                                        restartHomeActivity()
                                    }
                                }, {
                                    lastSelectedProfileRadio.isChecked = true
                                    showAlertDialog(
                                        this,
                                        getString(R.string.oops),
                                        it.second
                                    )
                                })
                        }
                    }
                } else {
                    lastSelectedProfileRadio.isChecked = true
                    showAlertDialog(
                        this,
                        getString(R.string.no_internet_label),
                        getString(R.string.no_internet)
                    )
                }
            }, {
                lastSelectedProfileRadio.isChecked = true
            })
    }
}*/


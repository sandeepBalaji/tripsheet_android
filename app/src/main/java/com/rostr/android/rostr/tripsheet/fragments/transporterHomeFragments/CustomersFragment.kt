package com.rostr.android.rostr.tripsheet.fragments.transporterHomeFragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.ListPopupWindow
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rostr.android.rostr.tripsheet.AddCustomerActivity
import com.rostr.android.rostr.tripsheet.CustomerDetailActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.adapter.AutoCompleteCustomerAdapter
import com.rostr.android.rostr.tripsheet.adapter.CustomersAdapter
import com.rostr.android.rostr.tripsheet.adapter.OptionsMenuAdapter
import com.rostr.android.rostr.tripsheet.model.Customer
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getTransportersDriversOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.recycler_layout.*


class CustomersFragment : Fragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var customerAdapter: CustomersAdapter
    private lateinit var customerList: ArrayList<Customer>
    private lateinit var filteredList: ArrayList<Customer>
    private lateinit var parentActivity: FragmentActivity
    private var sortPosition: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        actv_user_searchBox.hint = "Search customers"
        tv_title.text = getString(R.string.customer_list)
        iv_searchIcon.setOnClickListener(this)
        iv_sortIcon.setOnClickListener(this)
        bt_recycler_actionButton.setOnClickListener(this)
        actv_user_searchBox.setAdapter(
            AutoCompleteCustomerAdapter(
                mContext,
                R.layout.item_option,
                customerList
            )
        )
        actv_user_searchBox.setOnItemClickListener { adapterView, _, position, _ ->
            filteredList.clear()
            filteredList.add(adapterView.getItemAtPosition(position) as Customer)
            customerAdapter.notifyDataSetChanged()
        }
    }

    private fun initRecycler() {
        customerList = ArrayList()
        filteredList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
            customerAdapter = CustomersAdapter(mContext, filteredList) { _, position ->
                val intent = Intent(mContext, CustomerDetailActivity::class.java)
                intent.putExtra(DATA_KEY, filteredList[position])
                startActivity(intent)
            }
            adapter = customerAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_trips_optionsIcon -> {

            }

            R.id.iv_searchIcon -> {
                if (actv_user_searchBox.visibility == VISIBLE) {
                    actv_user_searchBox.visibility = GONE
                    if (!isEmpty(actv_user_searchBox)) {
                        actv_user_searchBox.setText("")
                        showSortedList(sortPosition)
                    }
                } else
                    actv_user_searchBox.visibility = VISIBLE
            }

            R.id.iv_sortIcon -> {
                if (customerList.isNullOrEmpty())
                    showLongSnack(cl_users_parent, "Cannot sort an empty list")
                else
                    showFilterOptions()
            }

            R.id.bt_recycler_actionButton -> {
                startActivity(Intent(mContext, AddCustomerActivity::class.java))
            }
        }
    }

    private fun showFilterOptions() {
        val options = resources.getStringArray(R.array.customer_filter_options)
        val popAdapter = OptionsMenuAdapter(
            mContext,
            options
        )
        val mPopupWindow = ListPopupWindow(mContext)
        mPopupWindow.setAdapter(popAdapter)
        mPopupWindow.anchorView = iv_sortIcon
        mPopupWindow.width = 300
        mPopupWindow.horizontalOffset = -250 //<--this provides the margin you need
        mPopupWindow.setOnItemClickListener { _, _, position, _ ->
            //            tv_search_sortBy.text = options[position]
            tv_recyclerMessage.visibility = GONE
            bt_recycler_actionButton.visibility = GONE
            showSortedList(position)
            mPopupWindow.dismiss()
        }
        mPopupWindow.show()
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(mContext))
            getMyCustomers()
        else
            showError(getString(R.string.no_internet))
    }

    private fun getMyCustomers() {
        customerList.clear()
        customerAdapter.notifyDataSetChanged()
        pb_progress.visibility = VISIBLE
        tv_recyclerMessage.visibility = GONE
        bt_recycler_actionButton.visibility = GONE
        getTransportersDriversOperation(
            ApiService.create(),
            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!, {
                if (isAdded) {
                    pb_progress.visibility = GONE
                    customerList.clear()
                    customerList.addAll(it.customers)
                    showSortedList(sortPosition)
                }
            }, {
                if (isAdded) {
                    pb_progress.visibility = GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            mContext, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            logoutUser(parentActivity)
                        }
                    } else
                        showError(it.second)
                }
            }
        )
    }

    private fun showSortedList(sort: Int) {
        filteredList.clear()
        when (sort) {
            0 -> { // name
                filteredList.addAll(customerList)
            }

            1 -> { // individual
                filteredList.addAll(ArrayList(customerList.filter { it.type == "individual" }))
            }

            2 -> { // business
                filteredList.addAll(ArrayList(customerList.filter { it.type == "corporate" }))
            }
        }
        customerAdapter.notifyDataSetChanged()
        sortPosition = sort
        if (filteredList.isEmpty())
            showError(getString(R.string.empty_customers))
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
        bt_recycler_actionButton.visibility = VISIBLE
        bt_recycler_actionButton.text = getString(R.string.add_customer)
    }
}
package com.rostr.android.rostr.tripsheet.dialogs

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.TransactionRequest
import com.rostr.android.rostr.tripsheet.model.UserDriver
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.createTransactionOperation
import com.rostr.android.rostr.tripsheet.utils.ACCESS_TOKEN
import com.rostr.android.rostr.tripsheet.utils.PreferenceUtils
import com.rostr.android.rostr.tripsheet.utils.showAlertDialog
import com.rostr.android.rostr.tripsheet.utils.showShortToast
import kotlinx.android.synthetic.main.dialog_pay_now.*
import java.util.*


class PayNowDialog(private val driverInfo: UserDriver) : BottomSheetDialogFragment() {

    private lateinit var mContext: Context
    private var isPaymentStarted: Boolean = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_pay_now, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (driverInfo.upi != null) {
            et_pay_now_upi.setEditable(false)
            et_pay_now_upi.value = driverInfo.upi
        } else
            et_pay_now_upi.setEditable(true)

        bt_pay_now_button.setOnClickListener {
            if (et_pay_now_amount.isEmpty || et_pay_now_amount.double == 0.0) {
                showShortToast(mContext, "Please enter a valid amount")
            } else {
                val pa = et_pay_now_upi.value
                val pn = driverInfo.driver?.name
                val tn = et_pay_now_note.value
                val am = et_pay_now_amount.value
                val url = "upi://pay?pa=${pa}&pn=${pn}&tn=${tn}&am=${am}"
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
                isPaymentStarted = true
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (isPaymentStarted) {
            val request = TransactionRequest(
                purpose = "Paid ${et_pay_now_amount.value} to ${driverInfo.driver?.name}",
                remarks = et_pay_now_note.value,
                driver = driverInfo.driver?._id,
                paymentStatus = "paid",
                date = Calendar.getInstance().timeInMillis,
                total = et_pay_now_amount.value
            )
            createTransactionOperation(
                ApiService.create(),
                PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!,
                request, {
                    showAlertDialog(mContext, null, "Transaction added successfully!") {
                        dismiss()
                    }
                }, {
                    showAlertDialog(mContext, null, it.second)
                })
        }
    }
}
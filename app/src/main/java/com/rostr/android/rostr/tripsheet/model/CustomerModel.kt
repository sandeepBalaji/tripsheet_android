package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class CustomerCreateRequest(
    val name: String,
    val mobileNo: String,
    val gender: String,
    val type: String,
    val email: String? = null,
    val companyName: String? = null,
    val address: Address? = null,
    val gstInfo: CustomerGst?
)

data class UserCustomer(
    val _id: String,
    val name: String,
    val mobileNo: String,
    val type: String
)

@Parcelize
data class Customer(
    val _id: String,
    val name: String,
    val mobileNo: String,
    val gender: String,
    val type: String,
    val email: String? = null,
    val companyName: String? = null,
    val address: Address? = null,
    val gstInfo: CustomerGst?
) : Parcelable

@Parcelize
data class CustomerGst(
    val gstIN: String?
) : Parcelable
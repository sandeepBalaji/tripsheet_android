package com.rostr.android.rostr.tripsheet

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.rostr.android.rostr.tripsheet.fragments.LoginFragment
import com.rostr.android.rostr.tripsheet.fragments.VerificationFragment
import com.rostr.android.rostr.tripsheet.model.SignInRequest
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.signInOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_login_master.*
import kotlin.properties.Delegates

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_master)

        replaceFragment(LoginFragment())
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fl_container, fragment).commit()
    }

    var showProgress: Boolean by Delegates.observable(false) { _, _, _ ->

        if (showProgress) {
            progress.visibility = View.VISIBLE
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
        } else {
            progress.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    fun doSignIn(phoneNumber: String) {
        showProgress = true
        val requestObject = SignInRequest(phoneNumber)
        signInOperation(ApiService.create(), requestObject, {
            showProgress = false
            PreferenceUtils(this).setData(MOBILE_NO, phoneNumber)
            try {
                val currentFragment = supportFragmentManager.findFragmentById(R.id.fl_container)
                if(currentFragment is LoginFragment)
                    currentFragment.doAfterLoginOperation(it)
                else if(currentFragment is VerificationFragment)
                    currentFragment.doAfterResendOperation()
            } catch (e: Exception) {
                showAlertDialog(this, null, e.localizedMessage)
            }
        }, {
            showProgress = false
            showAlertDialog(this, null, it.second)
        })
    }

    override fun onBackPressed() {
        if (supportFragmentManager.findFragmentById(R.id.fl_container) is LoginFragment)
            super.onBackPressed()
        else
            replaceFragment(LoginFragment())
    }
}
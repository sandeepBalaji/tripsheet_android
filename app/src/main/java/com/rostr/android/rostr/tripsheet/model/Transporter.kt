package com.rostr.android.rostr.tripsheet.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Transporter(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val customers: ArrayList<Customer>,
    val drivers: ArrayList<UserDriver>,
    val gender: String,
    val isActive: Boolean,
    val mobileNo: String,
    val name: String,
    val updatedAt: String,
    val vehicles: ArrayList<Vehicle>
) : Parcelable

@Parcelize
data class DriversTransporter(
    val __v: Int,
    val _id: String,
    val createdAt: String,
    val profileImage: String?,
    val customers: ArrayList<String>,
    val drivers: ArrayList<TransporterDriver>,
    val gender: String,
    val isActive: Boolean,
    val mobileNo: String,
    val name: String,
    val updatedAt: String,
    val vehicles: ArrayList<Vehicle>
) : Parcelable

data class UserTransPorter(
    val _id: String,
    val customers: ArrayList<Any>,
    val drivers: ArrayList<Any>,
    val gender: String,
    val isActive: Boolean,
    val mobileNo: String,
    val name: String,
    val vehicles: ArrayList<String>,
    var profileImage: String? = null,
    val companyName: String,
    val isLogistic: Boolean? = null,
    val accountInfo: AccountInfo? = null,
    val address: Address? = null,
    val gstIN: String? = null,
    val placeId: String? = null,
    val support: TransporterSupport? = null
//edit
)

data class UpdateTransporterRequest(
    val name: String? = null,
    val email: String? = null,
    val gender: String? = null,
    val profileImage: String? = null,
    val companyName: String? = null,
    val isLogistic: Boolean? = null,
    val accountInfo: AccountInfo? = null,
    val address: Address? = null,
    val gstIN: String? = null,
    val placeId: String? = null,
    val support: TransporterSupport? = null
)

data class AccountInfo(
    val name: String? = null,
    val accountNumber: String? = null,
    val IFSC: String? = null,
    val accountType: String? = null
)

data class TransporterSupport(
    val email: String?= null,
    val whatsApp: String? = null,
    val contactNumber: String? = null
)
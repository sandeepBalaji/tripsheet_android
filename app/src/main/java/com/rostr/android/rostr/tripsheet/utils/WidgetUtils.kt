package com.rostr.android.rostr.tripsheet.utils

import android.text.TextUtils
import android.widget.EditText
import android.widget.TextView
import android.text.Spannable
import android.text.style.UnderlineSpan
import android.graphics.Typeface
import android.text.style.StyleSpan
import android.text.SpannableStringBuilder



fun isEmpty(editText: EditText) : Boolean{
    return TextUtils.isEmpty(editText.text.toString().trim())
}

fun isEmpty(editText: TextView) : Boolean{
    return TextUtils.isEmpty(editText.text.toString().trim())
}

fun getStringData(editText: EditText): String{
    return editText.text.toString()
}

fun getCompulsoryString(text: String): SpannableStringBuilder {
    val compulsoryString = SpannableStringBuilder(text)
    compulsoryString.setSpan(
        StyleSpan(Typeface.BOLD),
        0, 32,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    compulsoryString.setSpan(UnderlineSpan(), 39, 49, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return compulsoryString
}

fun getOptionalString(text: String): SpannableStringBuilder {
    val optionalString = SpannableStringBuilder(text)
    optionalString.setSpan(
        StyleSpan(Typeface.BOLD),
        0, 32,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return optionalString
}
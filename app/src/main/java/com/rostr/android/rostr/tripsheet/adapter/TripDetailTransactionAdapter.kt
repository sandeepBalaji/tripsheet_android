package com.rostr.android.rostr.tripsheet.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rostr.android.rostr.tripsheet.BuildConfig
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.model.Data
import com.rostr.android.rostr.tripsheet.utils.getFormattedDateTime
import com.rostr.android.rostr.tripsheet.utils.showLog
import kotlinx.android.synthetic.main.item_trip_detail_transaction.view.*
import java.util.*

class TripDetailTransactionAdapter(
    private val mContext: Context,
    private val mainList: ArrayList<Data>,
    val onItemClick: ((view: View, position: Int) -> Unit)?
) :
    RecyclerView.Adapter<TripDetailTransactionAdapter.TransactionViewHolder>() {

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.tv_transaction_detail_item_name
        val amount: TextView = itemView.tv_transaction_detail_item_amount
        val remarks: TextView = itemView.tv_transaction_detail_item_remarks
        val dateTime: TextView = itemView.tv_transaction_detail_item_dateTime
        val image1: ImageView = itemView.iv_transaction_detail_item_image1
        val image2: ImageView = itemView.iv_transaction_detail_item_image2
        val image3: ImageView = itemView.iv_transaction_detail_item_image3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.item_trip_detail_transaction,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        try {
            val currentItem = mainList[position]
            holder.title.text = currentItem.purpose
            if (currentItem.total == null)
                holder.amount.visibility = GONE
            else {
                holder.amount.visibility = VISIBLE
                holder.amount.text = mContext.getString(
                    R.string.rs_string,
                    currentItem.total.replace(Regex("-"), "")
                )
                if (currentItem.total.toDouble() >= 0.0)
                    holder.amount.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.colorDarkGreen
                        )
                    )
                else
                    holder.amount.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.colorAccent
                        )
                    )
            }
            val cal = Calendar.getInstance()
            cal.timeInMillis = currentItem.date
            holder.dateTime.text = getFormattedDateTime(cal)
            holder.remarks.text = currentItem.remarks
            if (currentItem.images.isNotEmpty())
                Glide.with(mContext)
                    .apply { RequestOptions().centerCrop() }
                    .load("${BuildConfig.SERVER_URL_IMAGE}${currentItem.images[0]}")
                    .into(holder.image1)

            if (currentItem.images.size > 1)
                Glide.with(mContext)
                    .apply { RequestOptions().centerCrop() }
                    .load("${BuildConfig.SERVER_URL_IMAGE}${currentItem.images[1]}")
                    .into(holder.image2)

            if (currentItem.images.size > 2)
                Glide.with(mContext)
                    .apply { RequestOptions().centerCrop() }
                    .load("${BuildConfig.SERVER_URL_IMAGE}${currentItem.images[2]}")
                    .into(holder.image3)

            if (onItemClick != null)
                holder.itemView.setOnClickListener {
                    onItemClick.invoke(it, position)
                }
        } catch (ex: Exception) {
            showLog(ex.localizedMessage)
        }
    }

    override fun getItemCount(): Int {
        return mainList.size
//        return 5
    }
}
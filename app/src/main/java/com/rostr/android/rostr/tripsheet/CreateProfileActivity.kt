package com.rostr.android.rostr.tripsheet

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.RadioButton
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.rostr.android.rostr.tripsheet.model.CreateProfileRequest
import com.rostr.android.rostr.tripsheet.model.UserSingleton
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.createProfileOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.activity_create_profile.*
import kotlinx.android.synthetic.main.toolbar.*
import kotlin.properties.Delegates

class CreateProfileActivity : AppCompatActivity(), View.OnClickListener {

    private var businessType: Boolean = false
    private var role: String by Delegates.observable("Select Role") { _, _, newValue ->

        if (newValue == getString(R.string.driver)) {
            et_create_profile_businessName.visibility = GONE
            et_create_profile_businessName.value = ""
        } else {
            et_create_profile_businessName.visibility = VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_profile)
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).setDisplayShowTitleEnabled(false)
            (supportActionBar as ActionBar).setDisplayHomeAsUpEnabled(true)
            if (intent.hasExtra(ROLE_KEY)) {
                role = intent.getStringExtra(ROLE_KEY)
                if (role == "transporter")
                    businessType = intent.getBooleanExtra(TYPE_KEY, false)
            }
            tv_toolbar_title.text = getString(R.string.create_your_profile)
            et_create_profile_mobileNo.value = PreferenceUtils(this).getString(MOBILE_NO)
            bt_create_profile_actionButton.setOnClickListener(this)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_create_profile_actionButton -> {
                if (et_create_profile_name.isEmpty) {
                    showShortToast(this, "Please enter your name")
                    return
                }
                if (role == "Transporter") {
                    if (et_create_profile_businessName.isEmpty) {
                        showShortToast(this, "Please enter your business name")
                        return
                    }
                }
                if (!et_create_profile_email.isEmpty) {
                    if (!et_create_profile_email.isEmailValid) {
                        showShortToast(this, getString(R.string.invalid_email))
                        return
                    }
                }
                doCreate()
            }
        }
    }

    private fun doCreate() {
        createProfileOperation(ApiService.create(),
            PreferenceUtils(this).getString(TEMP_TOKEN)!!,
            getRequest(), {
                PreferenceUtils(this).setData(ACCESS_TOKEN, "Bearer ${it.token}")
                PreferenceUtils(this).setData(ID_KEY, it.user._id)
                PreferenceUtils(this).setData(ROLE_KEY, role)
                UserSingleton.instance = it.user
                val intent = Intent(this, HomeActivity::class.java)
                intent.flags =
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }, {
                if (it.first == 401) {
                    showAlertDialog(
                        this, getString(R.string.unauthorized_title),
                        getString(R.string.logged_out)
                    ) {
                        logoutUser(this)
                    }
                } else
                    showAlertDialog(this, getString(R.string.oops), it.second)
            })
    }

    private fun getRequest(): CreateProfileRequest {
        val genderRadio =
            rg_create_profile_gender.findViewById<RadioButton>(rg_create_profile_gender.checkedRadioButtonId)
        return CreateProfileRequest(
            name = et_create_profile_name.value,
            email = if (et_create_profile_email.isEmpty) null else et_create_profile_email.value,
            gender = genderRadio.text.toString(),
            mobileNo = et_create_profile_mobileNo.value,
            type = role,
            companyName = if (role == "transporter") et_create_profile_businessName.value else null,
            isLogistic = if (role == "transporter") businessType else null
        )
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }
}
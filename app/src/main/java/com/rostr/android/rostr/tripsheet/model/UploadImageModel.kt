package com.rostr.android.rostr.tripsheet.model

data class UploadImageResponse(
    val __v: Int,
    val createdAt: String,
    val id: String,
    val imageName: String,
    val imageUrl: String,
    val updatedAt: String
)
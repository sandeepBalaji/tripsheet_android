package com.rostr.android.rostr.tripsheet.model

data class VerifyRequest(
    val authKey: String,
    val mobileStore: MobileStoreInfo,
    val otp: String
)

data class MobileStoreInfo(
    val deviceInfo: DeviceInfo,
    val id: String,
    val mobileNo: String,
    val status: String
)

data class DeviceInfo(
    val deviceType: String,
    val fcmToken: String,
    val uniqueID: String
)

data class VerifyResponse(
    val token: String,
    val user: User? = null
)
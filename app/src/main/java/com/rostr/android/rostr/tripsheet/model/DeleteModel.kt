package com.rostr.android.rostr.tripsheet.model

data class DeleteRequest(
    val customer: String? = null,
    val driver: String? = null
)
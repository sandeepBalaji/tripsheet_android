package com.rostr.android.rostr.tripsheet.fragments.transporterHomeFragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.widget.ListPopupWindow
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.rostr.android.rostr.tripsheet.ContactListActivity
import com.rostr.android.rostr.tripsheet.DriverDetailActivity
import com.rostr.android.rostr.tripsheet.R
import com.rostr.android.rostr.tripsheet.adapter.AutoCompleteDriverAdapter
import com.rostr.android.rostr.tripsheet.adapter.DriverAdapter
import com.rostr.android.rostr.tripsheet.adapter.OptionsMenuAdapter
import com.rostr.android.rostr.tripsheet.model.UserDriver
import com.rostr.android.rostr.tripsheet.network.ApiService
import com.rostr.android.rostr.tripsheet.network.getTransportersDriversOperation
import com.rostr.android.rostr.tripsheet.utils.*
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.recycler_layout.*

class DriversFragment : Fragment(), View.OnClickListener {

    private lateinit var mContext: Context
    private lateinit var driverAdapter: DriverAdapter
    private lateinit var driverList: ArrayList<UserDriver>
    private lateinit var filteredList: ArrayList<UserDriver>
    private lateinit var parentActivity: FragmentActivity
    private var sortPosition = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
        parentActivity = activity!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecycler()
        tv_title.text = getString(R.string.drivers_list)
        actv_user_searchBox.hint = "Search Driver"
        iv_sortIcon.setOnClickListener(this)
        iv_searchIcon.setOnClickListener(this)
        bt_recycler_actionButton.setOnClickListener(this)
        actv_user_searchBox.setAdapter(
            AutoCompleteDriverAdapter(
                mContext,
                R.layout.item_option,
                driverList
            )
        )
        actv_user_searchBox.setOnItemClickListener { adapterView, _, position, _ ->
            filteredList.clear()
            filteredList.add(adapterView.getItemAtPosition(position) as UserDriver)
            driverAdapter.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isConnectedToInternet(mContext))
            getMyDrivers()
        else
            showError(getString(R.string.no_internet))
    }

    private fun initRecycler() {
        driverList = ArrayList()
        filteredList = ArrayList()
        rv_recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(mContext)
            driverAdapter = DriverAdapter(mContext, filteredList, false) { view, position ->
                when (view.id) {
                    R.id.iv_driver_item_callIcon -> {
                        val intent = Intent(
                            Intent.ACTION_DIAL,
                            Uri.parse("tel:" + filteredList[position].driver?.mobileNo)
                        )
                        activity!!.startActivity(intent)
                    }
                    R.id.iv_driver_item_whatsappIcon -> {
                        try {
                            val whatsappIntent = Intent(Intent.ACTION_VIEW)
                            val url =
                                "https://api.whatsapp.com/send?phone=+91${filteredList[position].driver?.mobileNo}"
                            whatsappIntent.setPackage("com.whatsapp")
                            whatsappIntent.data = Uri.parse(url)
                            if (whatsappIntent.resolveActivity(activity!!.packageManager) != null) {
                                activity!!.startActivity(whatsappIntent)
                            } else {
                                showLongToast(
                                    context!!,
                                    getString(R.string.no_whatsapp)
                                )
                            }
                        } catch (e: Exception) {
                            showLog(e)
                            showLongToast(
                                context!!,
                                getString(R.string.request_failure)
                            )
                        }
                    }
                    else -> {
                        val intent = Intent(mContext, DriverDetailActivity::class.java)
                        intent.putExtra(DATA_KEY, filteredList[position])
                        startActivity(intent)
                    }
                }

            }
            adapter = driverAdapter
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_sortIcon -> {
                if (driverList.isNullOrEmpty()) {
                    showLongSnack(cl_users_parent, "Cannot sort an empty list")
                } else
                    showSortOptions()
            }

            R.id.iv_searchIcon -> {
                if (actv_user_searchBox.visibility == VISIBLE) {
                    actv_user_searchBox.visibility = GONE
                    if (!isEmpty(actv_user_searchBox)) {
                        actv_user_searchBox.setText("")
                        showSortedList(sortPosition)
                    }
                } else
                    actv_user_searchBox.visibility = VISIBLE
            }

            R.id.bt_recycler_actionButton -> {
                startActivity(Intent(mContext, ContactListActivity::class.java))
            }
        }
    }

    private fun showSortOptions() {
        val options = resources.getStringArray(R.array.driver_sort_options)
        val popAdapter = OptionsMenuAdapter(mContext, options)
        val mPopupWindow = ListPopupWindow(mContext)
        mPopupWindow.setAdapter(popAdapter)
        mPopupWindow.anchorView = iv_sortIcon
        mPopupWindow.width = 580
        mPopupWindow.horizontalOffset = -500 //<--this provides the margin you need
        mPopupWindow.setOnItemClickListener { _, _, position, _ ->
            //            tv_search_sortBy.text = options[position]
            showSortedList(position)
            mPopupWindow.dismiss()
        }
        mPopupWindow.show()
    }

    private fun showSortedList(sortItemPosition: Int) {
        filteredList.clear()
        when (sortItemPosition) {
            0 -> { // name
                filteredList.addAll(ArrayList(driverList.sortedWith(compareBy { it.driver?.name })))
            }

            1 -> { // high to low
                filteredList.addAll(ArrayList(driverList.sortedWith(compareBy { it.due }).reversed()))
            }

            2 -> { //low to high
                filteredList.addAll(ArrayList(driverList.sortedWith(compareBy { it.due })))
            }
        }
        driverAdapter.notifyDataSetChanged()
        sortPosition = sortItemPosition
        if (driverList.isEmpty())
            showError(getString(R.string.empty_drivers))
    }

    private fun getMyDrivers() {
        driverList.clear()
        driverAdapter.notifyDataSetChanged()
        pb_progress.visibility = VISIBLE
        tv_recyclerMessage.visibility = GONE
        bt_recycler_actionButton.visibility = GONE
        getTransportersDriversOperation(ApiService.create(),
            PreferenceUtils(mContext).getString(ACCESS_TOKEN)!!, {
                if (isAdded) {
                    pb_progress.visibility = GONE
                    driverList.clear()
                    filteredList.clear()
                    driverList.addAll(it.drivers)
                    showSortedList(sortPosition)
                }
            }, {
                if (isAdded) {
                    pb_progress.visibility = GONE
                    if (it.first == 401) {
                        showAlertDialog(
                            mContext, getString(R.string.unauthorized_title),
                            getString(R.string.logged_out)
                        ) {
                            logoutUser(parentActivity)
                        }
                    } else
                        showError(it.second)
                }
            })
    }

    private fun showError(message: String) {
        tv_recyclerMessage.text = message
        tv_recyclerMessage.visibility = VISIBLE
        bt_recycler_actionButton.visibility = VISIBLE
        bt_recycler_actionButton.text = getString(R.string.add_driver)
    }
}